//////// Tree structure simulation  ////////////////////////////////////
#include <typeinfo>
#include <iostream>
#include "A.hpp"
#include "B.hpp"

using namespace std;


int main()
{
 
// 1 -QUESTION: how to make working for all version of f calls below?
    void f(const int& x);
    int a = 10;
    const int b = 10;

    f(a++);
    f(++a);
    f(a+b);
    f(10);
    f(b);
    f(b+a);
    f(a += b);

    cout << "\na = " << a << "\n";
    ++++a;
    cout << "\"++++a\": a = " << a << "\n";

// 2 -QUESTION: how to non-dublicate the code in postfix increment for A?
    A aStruct;
    cout << "\n";
    cout << "   aStruct = " << aStruct << "\n";
    ++aStruct;
    cout << "   ++aStruct = " << aStruct << "\n";
    cout << "   aStruct++ = " << aStruct++ << "\n";
    cout << "   aStruct = " << aStruct << "\n";

// 3 -QUESTION: which of the following will work?
    int a1 = 10;
    int* b1 = &a;
    int& c = a;
    int** d = &b1;
    int* e = b1;
//    int&* f1 = &c;  error: cannot declare pointer to ‘int&’
    int*& g = b1;

// 4 -QUESTION: which of the following will work?
    case1(); // B.hpp
    case2(); // B.hpp






    return 0; /// program ended successfully
}/// end function main


void f(const int& x) { cout << "x = " << x << "\n"; }


