#ifndef A_HPP
#define A_HPP

struct A {

    A(int n1 = 0, int n2 = 0, 
      int n3 = 0, int n4 = 0,
      int n5 = 0, int n6 = 0,
      int n7 = 0, int n8 = 0)
        :n1_(n1), 
         n2_(n2),
         n3_(n3),
         n4_(n4),
         n5_(n5),
         n6_(n6),
         n7_(n7),
         n8_(n8)
    { }

    A& operator++()
    {
        ++n1_; ++n2_; ++n3_; ++n4_;
        ++n5_; ++n6_; ++n7_; ++n8_;
        return *this;
    }

    A operator++(int dummy)
    {
        A tempA = *this;
        ++(*this);

        return tempA;
    }

    friend std::ostream& operator<<(std::ostream& oS, const A& sourceA)
    {
        oS << sourceA.n1_ << sourceA.n2_ << sourceA.n2_ 
           << sourceA.n3_ << sourceA.n4_ << sourceA.n5_ 
           << sourceA.n6_ << sourceA.n7_ << sourceA.n8_ << "\n";

        return oS;
    }

    int n1_, n2_, n3_, n4_, n5_, n6_, n7_, n8_;
};

#endif 

