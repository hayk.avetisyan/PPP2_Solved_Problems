#ifndef B_HPP
#define B_HPP
#include <iostream>
using namespace std;

struct B {
    B()  { cout << "B::B()\n"; }
    ~B() { cout << "B::~B()\n"; }

    virtual void f()                { cout << "void B::f()\n"; }
    virtual int  f(int i = 0) const { cout << "int  B::f(" << i << ") const\n"; return 0; }
};

template <class T>
struct D : private T
{
    D()  { cout << "D::D()\n"; }
    ~D() { cout << "D::~D()\n"; }

    virtual void f()                { cout << "void D::f()\n"; }
    virtual int  f(int i = 0)       { cout << "int  D::f(" << i << ")\n"; return 0; }
    virtual int  f(int i = 0) const { cout << "int  D::f(" << i << ") const\n"; return 0; }
};

void case1()
{
    D<B> d;
    d.f();
}

void case2()
{
    const B* b = new D<B>();
    b->f();
    delete b;
}

#endif 

