#ifndef RATIONAL_HPP
#define RATIONAL_HPP
// Rational class's definition. 
#include <iostream>
#include <exception>
#include <stdexcept>
#include <typeinfo>

using namespace std;

class Rational {
public:
    Rational(const string& name, int numerator, int denominator) 
        :name_( name ), 
        numerator_( numerator ), 
        denominator_( denominator )
    { }

    Rational operator*(const Rational& sourceRational)
    {
        int numerator = numerator_ * sourceRational.numerator();
        int denominator = denominator_ * sourceRational.denominator();
        Rational tempRational("", numerator, denominator);
        return tempRational;
    }

    int numerator() const
    {
        return numerator_;
    }

    int denominator() const
    {
        return denominator_;
    }

    string name() const
    {
        return name_;
    }

    void print_Rational()
    {
        cout << "\n" << name() << " is equal to  " 
             << numerator_ << " / " << denominator_ << "\n";
    }

    void setName(const string& name) 
    {
        name_ = name;
    }
private:
    int numerator_;
    int denominator_;
    string name_;
};
#endif 

