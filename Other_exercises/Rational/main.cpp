//////// Tree structure simulation  ////////////////////////////////////
#include <iterator>
#include "Rational.hpp"

int main()
{
    try {
        cout << "hello\n";



        Rational Rat1("Rat1", 5, 6);
        Rat1.print_Rational();

        Rational Rat2("Rat2", 10, 11);
        Rat2.print_Rational();

        Rational Rat3 = Rat1 * Rat2;
        Rat3.setName("Rat3");
        Rat3.print_Rational();

        Rat1 * Rat2 = Rat1;

        ( Rat1 * Rat2 = Rat1 ).print_Rational();

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


