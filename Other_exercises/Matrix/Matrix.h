#ifndef MATRIX_H
#define MATRIX_H

#include "../../std_lib_facilities_haykav.h"

enum slice { row, column };

template <class T> 
class Matrix;

template <class T> 
class VectorForMatrix {
public:
    VectorForMatrix(int IndexOFrowORcolumn, slice sliceName, Matrix<T>& matrixArg);
    ~VectorForMatrix();

    // copy constructor MUSN'T WORK
    VectorForMatrix(const VectorForMatrix& vectorArg) = delete; 

    int size() const { return size_; }

          T& operator[](int index);       // return ref without range check
    const T& operator[](int index) const; // return const-ref without range check
    
          T& at(int index);               // return ref with range check
    const T& at(int index) const;         // return const-ref with range check

    bool operator==(const VectorForMatrix& vectorArg) const;
    bool operator!=(const VectorForMatrix& vectorArg) const;

    void SetToZero(); // setting zero/NULL all members

    VectorForMatrix(VectorForMatrix&& vectorArg);                    // move constructor
    VectorForMatrix& operator=(VectorForMatrix&& vectorArg);         // move assignment
    VectorForMatrix<T>& operator=(const VectorForMatrix& vectorArg); // copy assignment

    void print() const; // print VectorForMatrix's elements

private:
    vector<T>* data;  // pointer to matrix's vector
    int size_;        // size of vector
    int step_;        // depends on row or column is the vector
    int offset_;      // depends on row or column is the vector
};


template <class T> 
class Matrix {
    template <class T_VEC> friend class VectorForMatrix;

public:
     Matrix();
     Matrix(int row, int column);
    ~Matrix();

          T& operator()(int row, int column);
    const T& operator()(int row, int column) const;

    int getRowsNumber()    const;
    int getColumnsNumber() const;

    VectorForMatrix<T>       getRow(int rowIndex);             //        m[rowIndex] row access 
    VectorForMatrix<const T> getRow(int rowIndex) const;       // const  m[rowIndex] row access

    VectorForMatrix<T>       getColumn(int columnIndex);       //       m[][columnIndex] column access
    VectorForMatrix<const T> getColumn(int columnIndex) const; // const m[][columnIndex] column access

    void print() const; // print matrix's elements (only if T has operator <<)

private:
    vector<T> flatElements_; // the elements of matrix is placed at the one vector
    int rowSize_;            // size of the row
};

#include "Matrix_Member_Functions.h"

#endif
