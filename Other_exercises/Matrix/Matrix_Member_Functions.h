#include <iomanip> 
/****************************************************
********                                      *******
********      Matrix's VectorForMatrix's      *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <class T>
VectorForMatrix<T>::VectorForMatrix(int IndexOFrowORcolumn, slice sliceName, Matrix<T>& matrixArg)
{
   if (row == sliceName) {
       int columnSize =  matrixArg.flatElements_.size() / matrixArg.rowSize_;
       step_ = 1;
       offset_ = IndexOFrowORcolumn * columnSize;
       size_ = matrixArg.flatElements_.size() / matrixArg.rowSize_;
   } else {
       step_ = matrixArg.flatElements_.size() / matrixArg.rowSize_;
       offset_ = IndexOFrowORcolumn;
       size_ = matrixArg.rowSize_;
   }
       
   data = &(matrixArg.flatElements_);
}

template <class T>
VectorForMatrix<T>::~VectorForMatrix()
{
    data = NULL;
}

template <class T> 
T & 
VectorForMatrix<T>::operator[](int index)
{
    return ((*data)[offset_ + index * step_]); 
}

template <class T> 
const T & 
VectorForMatrix<T>::operator[](int index) const
{
    return ((*data)[offset_ + index * step_]); 
}

template <class T> 
T & 
VectorForMatrix<T>::at(int index)
{
    if ( index < 0 || index >= size_ ) {
        throw out_of_range("Bad index of VectorForMatrix\n");
    }

    return ((*data)[offset_ + index * step_]); 
}

template <class T> 
const T & 
VectorForMatrix<T>::at(int index) const
{
    if ( index < 0 || index >= size_ ) {
        throw out_of_range("Bad index of VectorForMatrix\n");
    }

    return ((*data)[offset_ + index * step_]); 
}

template <class T> 
bool
VectorForMatrix<T>::operator==(const VectorForMatrix& vectorArg) const
{
    // may be the vectorArg is self object
    if (data == vectorArg.data && 
        size_ == vectorArg.size_ && 
        step_ == vectorArg.step_ && 
        offset_ == vectorArg.offset_) {
        return true;
    }

    // vectors' sizes and members must be equal to consider them equal
    if (size_ == vectorArg.size_) {
        for (int i = 0; i < size_; ++i) {
            if ( (*this)[i] != vectorArg[i] ) {
                return false;
            }
        }

        return true;
    }

    return false;
}

template <class T> 
bool 
VectorForMatrix<T>::operator!=(const VectorForMatrix& vectorArg) const
{
    return !(*this == vectorArg);
}


template <class T> // setting zero/NULL all members
void VectorForMatrix<T>::SetToZero()
{
    data    = NULL;
    size_   = 0;
    step_   = 0;
    offset_ = 0;
}

template <class T>
VectorForMatrix<T>::VectorForMatrix(VectorForMatrix&& vectorArg)
    : data(vectorArg.data),
    size_(vectorArg.size_),
    step_(vectorArg.step_),
    offset_(vectorArg.offset_)
{
    vectorArg.SetToZero();
}

template <class T>
VectorForMatrix<T> & 
VectorForMatrix<T>::operator=(VectorForMatrix&& vectorArg)
{
    if (size_ != vectorArg.size_) {
        throw invalid_argument("Bad assignment, source and destination vectors,\
                                must have the same sizes\n");
    }

    for (int i = 0; i < size_; ++i) {
        (*this)[i] = vectorArg[i];
    }

    vectorArg.SetToZero();
    return (*this);
}

template <class T> 
VectorForMatrix<T> & 
VectorForMatrix<T>::operator=(const VectorForMatrix& vectorArg)
{
    if (*this == vectorArg) {
        return *this;
    }

    if (size_ != vectorArg.size_) {
        throw invalid_argument("Bad assignment, source and destination vectors,\
                                must have the same sizes\n");
    }

    for (int i = 0; i < size_; ++i) {
        (*this)[i] = vectorArg[i];
    }

    return (*this);
}


////////////////////////////////////////////////////////////////////////////////
// ATTENTION - If "T" type doesn't have "<<" operator DON'T USE THIS FUNCTION //
////////////////////////////////////////////////////////////////////////////////
template <class T> // print VectorForMatrix's elements 
void  
VectorForMatrix<T>::print() const
{
    for (int i = 0; i < size_; ++i) {
        cout << (*this)[i] << " ";
    }
}


/****************************************************
********                                      *******
********               Matrix's               *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <class T>
Matrix<T>::Matrix()
    : rowSize_(0) 
{ } 

template <class T>
Matrix<T>::Matrix(int rowSize, int columnSize)
    : rowSize_( rowSize )
{
    int sizeOfElements = rowSize * columnSize;
    flatElements_.reserve( sizeOfElements );

    for (int i = 0; i < sizeOfElements; ++i) {
        flatElements_.push_back( T() );
    }
}

template <class T>
Matrix<T>::~Matrix()
{ }

template <class T>  // return ref
T &
Matrix<T>::operator()(int row, int column)
{
    int columnSize = flatElements_.size() / rowSize_;
    if (row >= rowSize_ || column >= columnSize) {
        throw out_of_range("Invalid index for matrix elements\n");
    }

    return flatElements_[column + row * columnSize];
}

template <class T>
const T &
Matrix<T>::operator()(int row, int column) const
{
    int columnSize = flatElements_.size() / rowSize_;
    if (row >= rowSize_ || column >= columnSize) {
        throw out_of_range("Invalid index for matrix elements\n");
    }

    return flatElements_[column + row * columnSize];
}

template <class T>
int
Matrix<T>::getRowsNumber() const
{
    return rowSize_;
}

template <class T> // return number of rows
int
Matrix<T>::getColumnsNumber() const
{
    return flatElements_.size() / rowSize_;
}

template <class T> // m[rowIndex] row access
VectorForMatrix<T>
Matrix<T>::getRow(int rowIndex)
{
    VectorForMatrix<T> rowVector(rowIndex, row, *this);
    return rowVector;
}

template <class T> // const  m[rowIndex] row access
VectorForMatrix<const T>
Matrix<T>::getRow(int rowIndex) const
{
    VectorForMatrix<const T> rowVector(rowIndex, row, *this);
    return rowVector;
}

template <class T> // m[columnIndex] column access 
VectorForMatrix<T>
Matrix<T>::getColumn(int columnIndex)
{
    VectorForMatrix<T> columnVector(columnIndex, column, *this);
    return columnVector;
}

template <class T> // const m[columnIndex] column access 
VectorForMatrix<const T>
Matrix<T>::getColumn(int columnIndex) const
{
    VectorForMatrix<const T> columnVector(columnIndex, column, *this);
    return columnVector;
}

////////////////////////////////////////////////////////////////////////////////
// ATTENTION - If "T" type doesn't have "<<" operator DON'T USE THIS FUNCTION //
////////////////////////////////////////////////////////////////////////////////
template <class T> // print matrix's elements 
void
Matrix<T>::print() const
{
    int columnSize = flatElements_.size() / rowSize_;

    for (int i = 0; i < rowSize_; ++i) {
        cout << " ";
        for (int j = 0; j < columnSize; ++j) {
            cout << setw(5) << (*this)(i, j) << " ";
        }

        cout << endl;
    }
}

