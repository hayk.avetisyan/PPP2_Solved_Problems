//////// Tree structure simulation  ////////////////////////////////////
#include "Matrix.h"

int main()
{
    try {
        const size_t ROW_SIZE = 5;
        const size_t COLUMN_SIZE = 4;

        //////////////////////////////////////////////////////////////////////
        //////////////////// Matrix of integers //////////////////////////////
        //////////////////////////////////////////////////////////////////////
        Matrix<int> matrix(ROW_SIZE, COLUMN_SIZE);
        cout << "matrix of integers with default values:\n";
        matrix.print();
        cout << endl;

        // assigning matrix' elements
        for (int i = 0; i < ROW_SIZE; ++i) {
            for (int j = 0; j < COLUMN_SIZE; ++j) {
                matrix(i,j) = i * COLUMN_SIZE + j;
            }
        }

        cout << "matrix of integers after assigning elements:\n";
        matrix.print();

        //////////////////// printing of rows //
        cout << "\nprinting of rows\n";
        int numberOfRows = matrix.getRowsNumber();
        for (int i = 0; i < numberOfRows; ++i) {
           VectorForMatrix<int> rowVector = matrix.getRow( i );
           cout << " " << i << " row = ";
           rowVector.print();
           cout << endl;
        }

        //////////////////// printing of columns //
        cout << "\nprinting of columns\n";
        int numberOfColumns = matrix.getColumnsNumber();
        for (int i = 0; i < numberOfColumns; ++i) {
           VectorForMatrix<int> columnVector = matrix.getColumn( i );
           cout << " " << i << " column = ";
           columnVector.print();
           cout << endl;
        }

        VectorForMatrix<int> rowVector1 = matrix.getRow(1); 
        VectorForMatrix<int> rowVector2(2, row, matrix);
        rowVector2 = rowVector1;
        cout << "\nCopying the 2th row into 3th row:\n";
        matrix.print();


        //////////////////////////////////////////////////////////////////////
        //////////////////// Matrix of strings //////////////////////////////
        //////////////////////////////////////////////////////////////////////
        cout << endl << endl;
        Matrix<string> matrixOfStrings(ROW_SIZE, COLUMN_SIZE);
        cout << "///////////////  String matrix //////////////\n";
        cout << "matrix of Strings with default values:\n";
        matrixOfStrings.print();
        cout << endl;

        for (int i = 0; i < ROW_SIZE; ++i) {
            for (int j = 0; j < COLUMN_SIZE; ++j) {
                if (i > 0 && 0 != j && COLUMN_SIZE - 1 != j) {
                    matrixOfStrings(i,j) = matrixOfStrings(i - 1, j - 1) + matrixOfStrings(i - 1, j + 1);
                } else {
                    matrixOfStrings(i,j) = 65 + i * COLUMN_SIZE + j;
                }
            }
        }

        cout << "matrix of Strings after assigning elements:\n";
        matrixOfStrings.print();

        //////////////////// printing of rows //
        cout << "\nprinting of rows\n";
        int numberOfRowsString = matrixOfStrings.getRowsNumber();
        for (int i = 0; i < numberOfRowsString; ++i) {
           VectorForMatrix<string> rowVector = matrixOfStrings.getRow( i );
           cout << " " << i << " row = ";
           rowVector.print();
           cout << endl;
        }

        //////////////////// printing of columns //
        cout << "\nprinting of columns\n";
        int numberOfColumnsString = matrixOfStrings.getColumnsNumber();
        for (int i = 0; i < numberOfColumnsString; ++i) {
           VectorForMatrix<string> columnVector = matrixOfStrings.getColumn( i );
           cout << " " << i << " column = ";
           columnVector.print();
           cout << endl;
        }

        VectorForMatrix<string> columnVector1 = matrixOfStrings.getColumn(1);
        VectorForMatrix<string> columnVector2(2, column, matrixOfStrings);
        columnVector2 = columnVector1;
        cout << "\nCopying the 2th column into 3th column:\n";
        matrixOfStrings.print();

    } catch (out_of_range &e) {
        cerr << "\nException: " << e.what() << endl; 
        return 1;
    } catch (invalid_argument &e) {
        cerr << "\nException: " << e.what() << endl; 
        return 1;
    } catch (...) {
        cerr << "\nException\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


