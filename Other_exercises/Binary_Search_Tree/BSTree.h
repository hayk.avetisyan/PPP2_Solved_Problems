#ifndef BSTREE_H
#define BSTREE_H

#include<cstdlib>
#include<iostream>

template <typename T>
class BST_Node {

    template <typename U> friend class BSTree;  // BST is Binary Search Tree class

public:
             BST_Node();                  
    explicit BST_Node(const T& sourceData_);

    bool operator==(const BST_Node<T>& sourceNode) const;
    bool operator!=(const BST_Node<T>& sourceNode) const;

                  BST_Node(const BST_Node<T>& sourceNode);
    BST_Node<T>& operator=(const BST_Node<T>& sourceNode);

    ~BST_Node();

    inline bool isLeaf() const { return (NULL == leftChild_ && NULL == rightChild_); }

    void PrintData() const { std::cout << data_ << " "; }

    inline BST_Node<T> *  left() const { return leftChild_;  }
    inline BST_Node<T> * right() const { return rightChild_; }

    inline BST_Node<T> *  Set_left(BST_Node<T> * pNode) {  leftChild_ = pNode; }
    inline BST_Node<T> * Set_right(BST_Node<T> * pNode) { rightChild_ = pNode; }
private:
    T data_;
    BST_Node<T> * leftChild_;
    BST_Node<T> * rightChild_;
};

#include "BST_Node_Functions.h"

template <typename T>
class BSTree {

public:
             BSTree();                  
    explicit BSTree(const T& sourceData_);
             BSTree(BST_Node<T> * pNode);

                  BSTree(const BSTree<T>& sourceNode);
    BSTree<T>& operator=(const BSTree<T>& sourceNode);

    ~BSTree();

    inline BST_Node<T> * root() const { return root_; }
private:
    BST_Node<T> * root_;
};

#include "BSTree_Functions.h"

#endif
