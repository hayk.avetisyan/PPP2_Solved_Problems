/****************************************************
********                                      *******
********              BST_Node's              *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <typename T>
BST_Node<T>::BST_Node() 
    : data_( T() ), 
      leftChild_( NULL ), 
      rightChild_( NULL )
{ }

template <typename T>
BST_Node<T>::BST_Node(const T& sourceData) 
    : data_( sourceData ), 
      leftChild_( NULL ), 
      rightChild_( NULL )
{ }

template <typename T>
bool BST_Node<T>::operator==(const BST_Node<T>& sourceNode) const
{
    if (      data_ == sourceNode.data_ &&
         leftChild_ == sourceNode.leftChild_ &&
        rightChild_ == sourceNode.rightChild_ ) {

        return true;
    }

    return false;
}

template <typename T>
bool BST_Node<T>::operator!=(const BST_Node<T>& sourceNode) const
{
    if (*this == sourceNode) {
        return false;
    }

    return true;
}

template <typename T> 
BST_Node<T>::BST_Node(const BST_Node<T>& sourceNode)  
    : data_( sourceNode.data_ ) 
{
   if (NULL == sourceNode.leftChild_) {
       leftChild_ = NULL;
   } else {
       leftChild_ = new BST_Node<T>( *(sourceNode.leftChild_) );
   }

   if (NULL == sourceNode.rightChild_) {
       rightChild_ = NULL;
   } else {
       rightChild_ = new BST_Node<T>( *(sourceNode.rightChild_) );
   }
}

template <typename T> 
BST_Node<T>&
BST_Node<T>::operator=(const BST_Node<T>& sourceNode)  
{
    if (*this == sourceNode) {
        return *this;
    }

    data_ = sourceNode.data_;

    if (NULL != leftChild_) {
        if (NULL != sourceNode.leftChild_) {
            leftChild_->data_ =  (sourceNode.leftChild_)->data_;
        } else {
            leftChild_ = NULL;
        }
    } else if (NULL != sourceNode.leftChild_) {
            leftChild_ = new BST_Node<T>( *(sourceNode.leftChild_) );
    } 

    if (NULL != rightChild_) {
        if (NULL != sourceNode.rightChild_) {
            rightChild_->data_ =  (sourceNode.rightChild_)->data_;
        } else {
            rightChild_ = NULL;
        }
    } else if (NULL != sourceNode.rightChild_) {
            rightChild_ = new BST_Node<T>( *(sourceNode.rightChild_) );
    } 

    return *this;
}

template <typename T>
BST_Node<T>::~BST_Node() // destructor
{
    if (NULL != leftChild_) {
        delete leftChild_;
    }

    if (NULL != rightChild_) {
        delete rightChild_;
    }
}

