//////// BSTree structure simulation  ////////////////////////////////////
#include "BSTree.h"
#include <stack>


template <typename T>
void Print_Tree_InOrder(const BSTree<T>& sourceTree) // inorder printing of tree
{
    BST_Node<T> * curr = sourceTree.root();
    std::stack< BST_Node<T>* > S;

    while ( !S.empty() || NULL != curr ) {
        if (NULL != curr) {
            S.push( curr );
            curr = curr -> left();
        } else {
            curr = S.top(); 
            S.pop();
            curr -> PrintData();
            curr = curr -> right();
        }
    }
}

int main()
{
    // construction of first subtree with root = 2
    BST_Node<int> * pNode2 = new BST_Node<int>(2);      //      2           //
    pNode2 ->  Set_left( new BST_Node<int>(1) );        //     / \          //
    pNode2 -> Set_right( new BST_Node<int>(3) );        //    1   3         //

    // construction of second subtree with root = 20
    BST_Node<int> * pNode20 = new BST_Node<int>(20);    //      20          //
    pNode20 ->  Set_left( new BST_Node<int>(17) );      //     /  \         //
    pNode20 -> Set_right( new BST_Node<int>(25) );      //   17    25       //

    // put first and second subtrees under root = 15
    BST_Node<int> * pNode15 = new BST_Node<int>(15);    //      15          //        
    pNode15 ->  Set_left( pNode2 );                     //     /  \         //
    pNode15 -> Set_right( pNode20 );                    //    2    20       //
                                                        //   / \   / \      //
                                                        //  1   3 17  25    //
    
    // initialize tree's root = 15 
    // by existed "pNode15" node
    BSTree<int> tree( pNode15 );

    std::cout << "\nHere is inorder-printed tree: ";
    Print_Tree_InOrder( tree ); 

    return 0; 
}


