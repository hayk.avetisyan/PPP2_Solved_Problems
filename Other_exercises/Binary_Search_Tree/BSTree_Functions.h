/****************************************************
********                                      *******
********               BSTree's               *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <typename T>
BSTree<T>::BSTree() 
    : root_( NULL )
{ } 

template <typename T>
BSTree<T>::BSTree(const T& sourceData_) 
    :root_( new BST_Node<T>(sourceData_) )
{ }

template <typename T>
BSTree<T>::BSTree(BST_Node<T> * pNode) 
    : root_( pNode )
{ }

template <typename T>
BSTree<T>::BSTree(const BSTree<T> & sourceTree)
    : root_( new BST_Node<T>( &(sourceTree -> root_) ) )
{ }

template <typename T>
BSTree<T>& 
BSTree<T>::operator=(const BSTree<T> & sourceTree)
{
    if (NULL != root_) {
      delete root_;
    }

    root_ = new BST_Node<T>( &(sourceTree -> root_) );
}

template <typename T>
BSTree<T>::~BSTree()
{
    if (NULL != root_) {
        delete root_;
    }
}

