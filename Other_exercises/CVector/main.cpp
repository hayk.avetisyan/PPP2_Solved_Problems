//////// Tree structure simulation  ////////////////////////////////////
#include "CVector.hpp"
#include <vector>
#include <iterator>
using namespace std;


int main()
{
    try {

        CVector<int> vecOfInts;
//        std::cout << vecOfInts << "\n";

        vecOfInts.push_back(10);
        vecOfInts.push_back(15);
        vecOfInts.push_back(20);
        vecOfInts.push_back(25);
        cout << "vecOfInts's size is: " << vecOfInts.size() << "\n";
        cout << "vecOfInts's elements:\n";

        CVector<int>::Iterator it = vecOfInts.begin();

        for (CVector<int>::Iterator it = vecOfInts.begin(); it != vecOfInts.end(); ++it) {
            cout << *it << "\n";
        }

     


    } catch (exception& e) {
        cerr << "exception: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


