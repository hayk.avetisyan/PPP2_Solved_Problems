#ifndef CVector_HPP
#define CVector_HPP
// CVector class's definition. 
#include <iostream>
#include <exception>
#include <stdexcept>
#include <cmath>

#include "CIterator.hpp"


template<typename T>
class Allocator {
public : 
    //    typedefs
    typedef T value_type;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef int size_type;
    typedef std::ptrdiff_t difference_type;

public : 
    //    convert an allocator<T> to allocator<U>
    template<typename U>
    struct rebind {
        typedef Allocator<U> other;
    };

public : 
    inline explicit Allocator() {}
    inline ~Allocator() {}
    inline explicit Allocator(Allocator const&) {}
    template<typename U>
    inline explicit Allocator(Allocator<U> const&) {}

    //    address
    inline pointer address(reference r) { return &r; }
    inline const_pointer address(const_reference r) { return &r; }

    //    memory allocation
    inline pointer allocate(size_type cnt, 
       typename std::allocator<void>::const_pointer = 0) { 
      return reinterpret_cast<pointer>(::operator new(cnt * sizeof (T))); 
    }
    inline void deallocate(pointer p, size_type) { 
        ::operator delete(p); 
    }

    //    construction/destruction
    inline void construct(pointer p, const T& t) { new(p) T(t); }
    inline void destroy(pointer p) { p->~T(); }

    inline bool operator==(Allocator const&) { return true; }
    inline bool operator!=(Allocator const& a) { return !operator==(a); }
};    //    end of class Allocator 


template <class T>
class CVector
{
public:
    CVector(); // default constructor
    explicit CVector(int sizeOfVec); // constructor
    ~CVector();

               CVector(const CVector& sourceVec); // copy constructor
    CVector& operator=(const CVector& sourceVec); // assignment

    // Capacity
    int size() const { return size_; }
    unsigned long max_size() const;
    void resize(int sizeNew, T sourceT);
    int capacity() const { return space_; }
    bool empty() const;
    void reserve(int spaceNew);

    // Element access
    const T& operator[](int index) const;
          T& operator[](int index);

    const T& at(int index) const;
          T& at(int index); 

    const T& front() const;
          T& front(); 

    const T& back() const;
          T& back(); 

    // Modifiers
    void assign(int elemCount, const T& sourceT);          // fill       

    template <class InputIterator> 
    void assign(InputIterator fisrt, InputIterator last);  // range

    void push_back(const T& sourceT);
    void pop_back();

    class Iterator;
    Iterator insert(Iterator position, const T& sourceT);                        // single element
        void insert(Iterator position, int elemCount, const T& sourceT);         // fill

    template <class InputIterator> 
        void insert(Iterator position, InputIterator fisrt, InputIterator last); // range

    Iterator erase(Iterator position);
    Iterator erase(Iterator fisrt, Iterator last);

    void swap(const T& sourceT);
    void clear(const T& sourceT);

    CVector& operator+=(const CVector& sourceVec);

    // Relational operators
    bool operator==(const CVector& sourceVec) const;
    bool operator!=(const CVector& sourceVec) const;
    bool operator<(const CVector& sourceVec) const;
    bool operator<=(const CVector& sourceVec) const;
    bool operator>(const CVector& sourceVec) const;
    bool operator>=(const CVector& sourceVec) const;

    // Non-member functions
    template <class U> friend void swap(CVector<U>& vec1, CVector<U>& vec2);

    template <class U> 
    friend std::ostream& operator<<(std::ostream& os, const CVector<U>& vec);

    template <class U> 
    friend std::istream& operator>>(std::istream& is, CVector<U>& vec);

    template <class U> 
    friend CVector merge(const CVector<U>& sourceVec1, const CVector<U>& sourceVec2);

    template <class U> 
    friend CVector operator+(const CVector<U>& sourceVec1, const CVector<U>& sourceVec2);

    // Iterators
public:

    class const_Iterator : public forward_iterator<T> {
        public:
            typedef const_Iterator self_type;

            const_Iterator()
                :pCurr_(NULL)
            { }

            const_Iterator(T* srcPointer)
                :pCurr_(srcPointer)
            { }

            const_Iterator(const CIterator<T>& srcIter)
                :forward_iterator<T>(srcIter)
            { }

            const self_type& operator=(const self_type& srcIter)
            {
                pCurr_ = srcIter.pCurr_;
                return *this;
            }

            ~const_Iterator()
            { }

            T& operator*()
            {
                return static_cast<const T&>(*pCurr_);
            }

            T* operator->()
            {
                return pCurr_;
            }
  
            self_type& operator++() //prefix increment
            {
                ++pCurr_;
                return *this;
            }

            self_type operator++(int) //prefix increment
            {
                self_type tempIter =*this;
                ++(*this);
                return tempIter;
            }

            bool operator==(const self_type& srcIter) const
            {
                return (pCurr_ == srcIter.pCurr_);
            }

            bool operator!=(const self_type& srcIter) const
            {
                return (pCurr_ != srcIter.pCurr_);
            }

        protected:
            T* pCurr_;
    };

    class Iterator : public randomAccess_iterator<T> {
        public:
            typedef CVector<T>::Iterator self_type;

            Iterator()
                :pCurr_(NULL)
            { }

            Iterator(const CIterator<T>& srcIter)
                :pCurr_(srcIter.pCurr_)
            { }

            Iterator(T* src_pCurr)
                :pCurr_(src_pCurr) 
            { }

             ~Iterator()
            { }

            bool operator==(const self_type& srcIter) const
            {
                return (pCurr_ == srcIter.pCurr_);
            }

            bool operator!=(const self_type& srcIter) const
            {
                return (pCurr_ != srcIter.pCurr_);
            }

            T& operator*()
            {
                return *pCurr_;
            }

            T* operator->()
            {
                return pCurr_;
            }

            self_type& operator++() //prefix increment
            {
                ++pCurr_;
                return *this;
            }

            self_type operator++(int junk) //postfix increment
            {
                self_type tempIter = *this;
                ++(*this);
                return tempIter;
            }

            self_type& operator--() //prefix increment
            {
                --pCurr_;
                return *this;
            }

            self_type operator--(int junk) //postfix increment
            {
                self_type tempIter = *this;
                --(*this);
                return tempIter;
            }

        protected:
            T* pCurr_;
    };

          Iterator end();
    const_Iterator end() const;

          Iterator begin();
    const_Iterator begin() const;

          Iterator rend();
    const_Iterator rend() const;

          Iterator rbegin();
    const_Iterator rbegin() const;
protected:
    template <class U> friend void copy_CVector(typename CVector<U>::const_Iterator it_begin, typename CVector<U>::const_Iterator it_end, T* p);

private:
    T * pData_; 
    int size_; 
    int space_; 
}; // end of the CVector class declaration 

#include "CVector_Member_Functions.hpp"
#endif 

