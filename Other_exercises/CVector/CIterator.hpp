
template <class T>
class CIterator {
public:    
    typedef CIterator self_type;

    CIterator()
        :pCurr_(NULL)
    { }

    CIterator(const CIterator<T>& srcCIter)
        :pCurr_(srcCIter.pCurr_)
    { }

    CIterator& operator=(const CIterator<T>& srcCIter)
    {
        pCurr_ = srcCIter.pCurr_;
        return *this;
    }

    CIterator(T* src_pCurr_)
        :pCurr_(src_pCurr_)
    { }
    
    virtual ~CIterator()
    { }

    virtual CIterator<T>& operator++() = 0; //prefix increment
protected:
    T* pCurr_;
};

template <class T>
class input_iterator : virtual public CIterator<T> {
public:    
    typedef input_iterator<T> self_type;

    input_iterator(const CIterator<T>& srcCIter)
        :CIterator<T>::pCurr_(srcCIter.pCurr)
    { }

    virtual ~input_iterator()
    { }

    virtual bool operator==(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ == srcIter.pCurr_);
    }

    virtual bool operator!=(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ != srcIter.pCurr_);
    }

    virtual const T& operator*() const
    {
        return *CIterator<T>::pCurr_;
    }

    virtual T* operator->() = 0;

protected:
    input_iterator(T* src_pCurr)
        :CIterator<T>::pCurr_(src_pCurr)
    { }

    input_iterator()
        :CIterator<T>()
    { }
};

template <class T>
class output_iterator : virtual public CIterator<T> {
public:    
    typedef output_iterator<T> self_type;

    output_iterator(const CIterator<T>& srcCIter)
        :CIterator<T>(srcCIter)
    { }

    virtual ~output_iterator()
    { }

    virtual T& operator*()
    {
        return *CIterator<T>::pCurr_;
    }

protected:
    output_iterator(T* src_pCurr)
        :CIterator<T>::pCurr_(src_pCurr)
    { }

    output_iterator()
        :CIterator<T>()
    { }
};

template <class T>
class forward_iterator : public output_iterator<T>, public input_iterator<T> {
public:    
    typedef forward_iterator<T> self_type;

    forward_iterator(const CIterator<T>& srcCIter)
        :CIterator<T>(srcCIter)
    { }

    forward_iterator()
    { }

    virtual ~forward_iterator()
    { }

    virtual bool operator==(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ == srcIter.pCurr_);
    }

    virtual bool operator!=(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ != srcIter.pCurr_);
    }

protected:
    forward_iterator(T* src_pCurr)
        :CIterator<T>::pCurr_(src_pCurr)
    { }
};


template <class T>
class bidirectional_iterator : public forward_iterator<T> {
public:    
    typedef bidirectional_iterator<T> self_type;

    bidirectional_iterator(const CIterator<T>& srcCIter)
        :CIterator<T>(srcCIter)
    { }

    bidirectional_iterator()
    { }

    virtual ~bidirectional_iterator()
    { }

    virtual bool operator==(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ == srcIter.pCurr_);
    }

    virtual bool operator!=(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ != srcIter.pCurr_);
    }

    virtual CIterator<T>& operator--() = 0; //prefix increment

protected:
    bidirectional_iterator(T* src_pCurr)
        :CIterator<T>::pCurr_(src_pCurr)
    { }
};


template <class T>
class randomAccess_iterator : public bidirectional_iterator<T> {
public:    
    typedef randomAccess_iterator<T> self_type;

    randomAccess_iterator(const CIterator<T>& srcCIter)
        :CIterator<T>(srcCIter)
    { }

    randomAccess_iterator()
    { }

    virtual ~randomAccess_iterator()
    { }

    virtual bool operator==(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ == srcIter.pCurr_);
    }

    virtual bool operator!=(const self_type& srcIter) const
    {
        return (CIterator<T>::pCurr_ != srcIter.pCurr_);
    }

    template <class U> 
    friend randomAccess_iterator<U> operator+( const randomAccess_iterator<U>& ra_iter, int intNumber);

    template <class U> 
    friend randomAccess_iterator<U> operator+( int intNumber, const randomAccess_iterator<U>& ra_iter);

    template <class U> 
    friend randomAccess_iterator<U> operator-( const randomAccess_iterator<U>& ra_iter, int intNumber);
    template <class U> 
    friend randomAccess_iterator<U> operator-( int intNumber, const randomAccess_iterator<U>& ra_iter);

    template <class U> 
    friend bool operator>( const randomAccess_iterator<U>& ra_iter1, const randomAccess_iterator<U>& ra_iter2);
    template <class U> 
    friend bool operator<( const randomAccess_iterator<U>& ra_iter1, const randomAccess_iterator<U>& ra_iter2);

    template <class U> 
    friend bool operator>=( const randomAccess_iterator<U>& ra_iter1, const randomAccess_iterator<U>& ra_iter2);
    template <class U> 
    friend bool operator<=( const randomAccess_iterator<U>& ra_iter1, const randomAccess_iterator<U>& ra_iter2);

    randomAccess_iterator<T>& operator+=(int intNumber);
    randomAccess_iterator<T>& operator-=(int intNumber);

protected:
    randomAccess_iterator(T* src_pCurr)
        :CIterator<T>::pCurr_(src_pCurr)
    { }
};


