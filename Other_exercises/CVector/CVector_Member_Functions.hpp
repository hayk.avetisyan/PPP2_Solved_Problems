/****************************************************
********                                      *******
********               CVector's              *******
********           member-functions           *******
********                                      *******
*****************************************************/

template <class T>
CVector<T>::CVector()
    :pData_( NULL ), size_(0), space_(0)
{
}

template <class T>
CVector<T>::CVector(int sizeOfVec)
    :pData_(new T[sizeOfVec]), size_( sizeOfVec )
{
    space_ = 1.5 * size_;
    for (int i = 0; i < size_; ++i) {
        pData_[i] = T();
    }
}

template <class T>
CVector<T>::~CVector()
{
    delete[] pData_;
}

template <class T>
CVector<T>::CVector(const CVector& sourceVec)
    :pData_(new T[sourceVec.size()]), size_( sourceVec.size() ), space_( sourceVec.space_ )
{
    copy(sourceVec.begin(), sourceVec.end(), pData_);
}

template <class T>
CVector<T>&
CVector<T>::operator=(const CVector& sourceVec)
{
    if (*this == sourceVec) return *this;

    int sizeOfsource = sourceVec.size();
    if (sizeOfsource <= space_) {
        for (int i = 0; i < sizeOfsource; ++i) {
            pData_[ i ] = sourceVec[ i ];
        }

        size_ = sizeOfsource;
        return *this;
    }

    delete[] pData_;
    size_ = sizeOfsource;
    space_ = sourceVec.space_;
    T* p = new T[ size_ ];
    copy(sourceVec.begin(), sourceVec.end(), p);
    pData_ = p;

    return *this;
}

template <class T>
std::ostream&
operator<<(std::ostream& outStream, const CVector<T>& vec)
{
    if ( vec.empty() ) {
        throw std::runtime_error ("The vector is empty\n");
    }

    typename CVector<T>::const_Iterator It = vec.begin();
    while ( It != vec.end() ) {
        if (outStream << *It) {
            outStream << " ";
            ++It;
        }
    }

    return outStream;
}

template <class T>
std::istream& 
operator>>(std::istream& inputStream, CVector<T>& vec)
{
    T inputElelement;
    while ( inputStream >> inputElelement ) {
        vec.push_back( inputElelement );
    }

    return inputStream;
}

template <class T>
void
copy_CVector(typename CVector<T>::const_Iterator it_begin, typename CVector<T>::const_Iterator it_end, T* p)
{
    while (it_begin != it_end) {
        *p = *it_begin;
        ++p;
        ++it_begin;
    }
}

template <class T>
CVector<T>
merge(const CVector<T>& sourceVec1, const CVector<T>& sourceVec2)
{
    int sizeNew = sourceVec1.size() + sourceVec2.size();
    CVector<T> vecNew( sizeNew );
    typename CVector<T>::Iterator IterNew = vecNew.begin();

    typename CVector<T>::const_Iterator Iter1 = sourceVec1.begin();
    while ( Iter1 != sourceVec1.end() ) {
        *IterNew = *Iter1;
        ++Iter1;
        ++IterNew;
    }

    typename CVector<T>::const_Iterator Iter2 = sourceVec2.begin();
    while ( Iter2 != sourceVec2.end() ) {
        *IterNew = *Iter2;
        ++Iter2;
        ++IterNew;
    }

    return vecNew;
}

template <class T>
CVector<T>
operator+(const CVector<T>& sourceVec1, const CVector<T>& sourceVec2)
{
    return merge(sourceVec1, sourceVec2);
}
                                        // Capacity
template <class T>
unsigned long
CVector<T>::max_size() const // 
{
    int power = ceil(log2(sizeof(T)));
    int remainBytes = 32 - power;
    return pow(2,remainBytes) - 1;
}
                                       
template <class T>
void 
CVector<T>::resize(int sizeNew, T sourceT) // 
{
    if (sizeNew < size_) {
        for (int i = sizeNew; i < size_; ++i) {
            delete &(pData_[ i ]);
        }
    } else if (space_ > sizeNew) {
        for (int i = sizeNew; i < size_; ++i) {
            pData_[ i ] = sourceT;
        }
    } else {
        reserve(2 * sizeNew);
        for (int i = size_; i < sizeNew; ++i) {
            pData_[ i ] = sourceT;
        }
    }

    size_ = sizeNew;
}

template <class T>
bool 
CVector<T>::empty() const // 
{
    if (0 == size_) {
        return true;
    }

    return false;
}

template <class T>
void 
CVector<T>::reserve(int spaceNew)
{
    if (spaceNew <= size_) {
        return;
    }

    T* p = new T[ spaceNew ];
    for (int i = 0; i < size_; ++i) {
        p[ i ] = pData_[ i ];
    }

    delete [] pData_;
    pData_ = p;
    space_ = spaceNew;
}

template <class T>
const T& 
CVector<T>::operator[](int index) const
{
    return pData_[index];
}

template <class T>
T& 
CVector<T>::operator[](int index)
{
    return ( const_cast<T&>( 
                static_cast<const T&>(*this)[index]
            ) );
}

template <class T>
const T& 
CVector<T>::at(int index) const
{
    if (index >= size_) {
        throw std::out_of_range("The index must be smaller than vector's size\n");
    }

    return pData_[ index ];
}

    // Element access
template <class T>
T& 
CVector<T>::at(int index)  
{
    return ( const_cast<T&>( 
                static_cast<const T&>(*this).at(index) 
            ) );
}

template <class T>
const T& 
CVector<T>::front() const
{
    if (empty()) {
        throw std::out_of_range("The vector must be non-empty to return a first element\n");
    }

    return pData_[ 0 ];
}

template <class T>
T& 
CVector<T>::front() 
{
    return ( const_cast<T&>( 
                static_cast<const T&>(*this).front() 
            ) );
}

template <class T>
const T& 
CVector<T>::back() const
{
    if (empty()) {
        throw std::out_of_range("The vector must be non-empty to return a last element\n");
    }

    return pData_[ size_ - 1 ];
}

template <class T>
T& 
CVector<T>::back()
{
    return ( const_cast<T&>( 
                static_cast<const T&>(*this).back() 
            ) );
}

// Modifiers

template <class T>
void 
CVector<T>::assign(int elemCount, const T& sourceT) // fill
{
   if (elemCount > space_) {
       reserve(2 * elemCount);
   }

   if (elemCount > size_) {
       size_ = elemCount;
   }

   for (int i = 0; i < elemCount; ++i) {
       delete &(pData_[ i ]);
       pData_[ i ] = sourceT;
   }
}


/*
template <class InputIterator> 
void assign(InputIterator fisrt, InputIterator last)  // range
{
}
*/


template <class T>
void 
CVector<T>::push_back(const T& sourceT) // add sourceT in CVector
{
   if (0 == space_) {
      reserve(8);
   } else if (size_ == space_) {
      reserve(2 * space_);
   }

   pData_[size_] = sourceT;
   ++size_;
}

template <class T>
bool 
CVector<T>::operator==(const CVector& sourceVec) const  
{
    if ( size_ == sourceVec.size() ) {
        typename CVector<T>::const_Iterator Iter = begin();
        typename CVector<T>::const_Iterator Iter_src = sourceVec.begin();
        while ( Iter != end() ) {
            if (*Iter != *Iter_src) {
                return false;
            }
            ++Iter;
            ++Iter_src;
        }

       return true;
   }

   return false;
}

template <class T>
bool 
CVector<T>::operator!=(const CVector& sourceVec) const  
{
   if (*this == sourceVec) {
       return false;
   }

   return true;
}

template <class T>
CVector<T>& 
CVector<T>::operator+=(const CVector& sourceVec)  
{
    int sizeOfsource = sourceVec.size();
    if ( space_ - size_ < sizeOfsource ) {
        reserve( sizeOfsource - (space_ - size_) );
    } 

    for (int i = size_; i < space_; ++i) {
        pData_[ i ] = sourceVec[ i - size_ ];
    }

    return *this;
}

template <class T>
typename CVector<T>::Iterator 
CVector<T>::end() 
{
    typename CVector<T>::Iterator beyondIterator(pData_ + size_);
    return beyondIterator;
}

template <class T>
typename CVector<T>::const_Iterator 
CVector<T>::end() const
{
    typename CVector<T>::const_Iterator beyondIterator(pData_ + size_);
    return beyondIterator;
}

template <class T>
typename CVector<T>::const_Iterator 
CVector<T>::begin() const
{
    if ( empty() ) {
        return end();
    }

    typename CVector<T>::const_Iterator iterOnFirst(pData_);
    return iterOnFirst;
}

template <class T>
typename CVector<T>::Iterator 
CVector<T>::begin() 
{
    if ( empty() ) {
        return end();
    }

    typename CVector<T>::Iterator iterOnFirst(pData_);
    return iterOnFirst;
}

