//////// Tests of C++ Primer's 4 chapter ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        int *pToInt;
        pToInt = (int *) 0xbffe9f90; 
        cout << "pToInt = " << pToInt << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

