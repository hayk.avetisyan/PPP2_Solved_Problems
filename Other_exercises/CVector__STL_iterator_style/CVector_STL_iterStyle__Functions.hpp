/****************************************************
********                                      *******
********               CVector's              *******
********           member-functions           *******
********                                      *******
*****************************************************/

template <class T, class A>
CVector<T,A>::CVector()
    :size_(0), space_(0), alloc(), elem( NULL )
{
}

template <class T, class A>
CVector<T,A>::CVector(int sizeOfVec)
    :size_(sizeOfVec), 
     space_(1.5 * size_), 
     alloc(),
     elem( alloc.allocate(space_) )
{
    for (int i = 0; i < size_; ++i) {
        alloc.construct( &elem[ i ], T() );
    }
}

template <class T, class A>
CVector<T,A>::~CVector()
{
    delete[] elem;
}

template <class T, class A>
CVector<T,A>::CVector(const CVector& sourceVec)
    :size_( sourceVec.size() ), 
    space_( sourceVec.space_ ), 
    alloc(),
    elem( alloc.allocate(space_) )
{
    for (int i = 0; i < size_; ++i) {
        alloc.construct( &elem[i], sourceVec[i] );
    }
}

template <class T, class A>
CVector<T,A>&
CVector<T,A>::operator=(const CVector<T,A>& sourceVec)
{
    if (*this == sourceVec) return *this;

    int sizeOfsource = sourceVec.size();
    if (sizeOfsource <= space_) {
        size_ = sizeOfsource;
        for (int i = 0; i < size_; ++i) {
            elem[i] = sourceVec[i];
        }

        return *this;
    }

    space_ = 1.5 * sizeOfsource;
    T* p = alloc.allocate( space_ );
    for (int i = 0; i < sizeOfsource; ++i) {
        alloc.construct( &p[i], sourceVec[i] );
    }

    for (int i = 0; i < size_; ++i) {
        alloc.destroy( &elem[i] );
    }

    elem = p;
    size_ = sizeOfsource;
    p = NULL;
    return *this;
}

template <class T, class A>
std::ostream&
operator<<(std::ostream& outStream, const CVector<T,A>& vec)
{
    if ( vec.empty() ) {
        throw std::runtime_error ("The vector is empty\n");
    }

    outStream << "  ";
    typename CVector<T,A>::const_Iterator It = vec.begin();
    while ( It != vec.end() ) {
        if (outStream << *It) {
            outStream << " ";
            ++It;
        }
    }

    return outStream;
}

template <class T, class A>
std::istream& 
operator>>(std::istream& inputStream, CVector<T,A>& vec)
{
    T inputElelement;
    while ( inputStream >> inputElelement ) {
        vec.push_back( inputElelement );
    }

    return inputStream;
}

template <class T, class A>
void
copy_CVector(typename CVector<T,A>::const_Iterator first, typename CVector<T,A>::const_Iterator last, typename CVector<T,A>::Iterator result)
{
    typename CVector<T,A>::Iterator temp = result;
    while (first != last) {
        *temp++ = *first++;
    }

    return result;
}

template <class T, class A>
CVector<T,A>
merge(const CVector<T,A>& sourceVec1, const CVector<T,A>& sourceVec2)
{
    int sizeNew = sourceVec1.size() + sourceVec2.size();
    CVector<T,A> vecNew( sizeNew );
    typename CVector<T,A>::Iterator IterNew = vecNew.begin();

    typename CVector<T,A>::const_Iterator Iter1 = sourceVec1.begin();
    while ( Iter1 != sourceVec1.end() ) {
        *IterNew = *Iter1;
        ++Iter1;
        ++IterNew;
    }

    typename CVector<T,A>::const_Iterator Iter2 = sourceVec2.begin();
    while ( Iter2 != sourceVec2.end() ) {
        *IterNew = *Iter2;
        ++Iter2;
        ++IterNew;
    }

    return vecNew;
}

template <class T, class A>
CVector<T,A>
operator+(const CVector<T,A>& sourceVec1, const CVector<T,A>& sourceVec2)
{
    return merge(sourceVec1, sourceVec2);
}
                                        // Capacity
template <class T, class A>
unsigned long
CVector<T,A>::max_size() const // 
{
    int power = ceil(log2(sizeof(T)));
    int remainBytes = 32 - power;
    return pow(2,remainBytes) - 1;
}
                                       
template <class T, class A>
void 
CVector<T,A>::resize(int sizeNew, T sourceT) // 
{
    if (sizeNew < size_) {
        for (int i = sizeNew; i < size_; ++i) {
            alloc.destroy( &(elem[i]) );
        }
    } else if (space_ > sizeNew) {
        for (int i = sizeNew; i < size_; ++i) {
            elem[ i ] = sourceT;
        }
    } else {
        reserve(1.5 * sizeNew);
        for (int i = size_; i < sizeNew; ++i) {
            elem[ i ] = sourceT;
        }
    }

    size_ = sizeNew;
}

template <class T, class A>
bool 
CVector<T,A>::empty() const // 
{
    if (0 == size_) {
        return true;
    }

    return false;
}

template <class T, class A>
void 
CVector<T,A>::reserve(int spaceNew)
{
    if (spaceNew <= size_) {
        return;
    }

    T* p = alloc.allocate( spaceNew );
    for (int i = 0; i < size_; ++i) {
        alloc.construct(&p[ i ], elem[ i ]);
    }

    for (int i = 0; i < size_; ++i) {
        alloc.destroy(&elem[ i ]);
    }

    alloc.deallocate(elem, space_);

    elem = p;
    space_ = spaceNew;
    p = NULL;
}

template <class T, class A>
inline const T& 
CVector<T,A>::operator[](int index) const
{
    return elem[index];
}

template <class T, class A>
inline T& 
CVector<T,A>::operator[](int index)
{
    return const_cast<T&>( static_cast<const CVector<T,A>& >(*this)[index] );
}

template <class T, class A>
const T& 
CVector<T,A>::at(int index) const
{
    if (index >= size_) {
        throw std::out_of_range("The index must be smaller than vector's size\n");
    }

    return elem[ index ];
}

    // Element access
template <class T, class A>
T& 
CVector<T,A>::at(int index)  
{
    return const_cast<T&>( static_cast<const CVector<T,A>& >(*this).at(index) );
}

template <class T, class A>
const T& 
CVector<T,A>::front() const
{
    if (empty()) {
        throw std::out_of_range("The vector must be non-empty to return a first element\n");
    }

    return elem[ 0 ];
}

template <class T, class A>
T& 
CVector<T,A>::front() 
{
    return const_cast<T&>( static_cast<const CVector<T,A>& >(*this).front() );
}

template <class T, class A>
const T& 
CVector<T,A>::back() const
{
    if ( empty() ) {
        throw std::out_of_range("The vector must be non-empty to return a last element\n");
    }

    return elem[ size_ - 1 ];
}

template <class T, class A>
T& 
CVector<T,A>::back()
{
    return const_cast<T&>( static_cast<const CVector<T,A>& >(*this).back() );
}

// Modifiers

template <class T, class A>
void 
CVector<T,A>::assign(int elemCount, const T& sourceT) // fill
{
   if (elemCount > space_) {
       reserve(2 * elemCount);
   }

   size_ = elemCount;
   for (int i = 0; i < elemCount; ++i) {
       elem[ i ] = sourceT;
   }
}

template<class T, class A> template<class InputIterator> 
void 
CVector<T,A>::assign(InputIterator first, InputIterator  last)  // range
{

    typedef typename iterator_traits<InputIterator>::iterator_category iterator_category;
    if ( typeid(iterator_category) != typeid(input_iterator_tag) ) {
        throw std::invalid_argument("there must be input iterator as an argument of the assign function\n");
    }
}

template <class T, class A>
void 
CVector<T,A>::push_back(const T& sourceT) // add sourceT in CVector
{
   if (0 == space_) {
      reserve(8);
   } else if (size_ == space_) {
      reserve(2 * space_);
   }

   elem[size_] = sourceT;
   ++size_;
}

template <class T, class A>
void 
CVector<T,A>::pop_back() // remove last element in CVector
{
    if ( empty() ) {
        throw std::out_of_range("The vector must be non-empty to for pop_back() function\n");
    }

    --size_;
}


template <class T, class A>
bool 
CVector<T,A>::operator==(const CVector& sourceVec) const  
{
    if ( size_ == sourceVec.size() ) {
        typename CVector<T,A>::const_Iterator Iter = begin();
        typename CVector<T,A>::const_Iterator Iter_src = sourceVec.begin();
        while ( Iter != end() ) {
            if (*Iter != *Iter_src) {
                return false;
            }
            ++Iter;
            ++Iter_src;
        }

       return true;
   }

   return false;
}

template <class T, class A>
bool 
CVector<T,A>::operator!=(const CVector& sourceVec) const  
{
   if (*this == sourceVec) {
       return false;
   }

   return true;
}

template <class T, class A>
CVector<T,A>& 
CVector<T,A>::operator+=(const CVector& sourceVec)  
{
    int sizeOfsource = sourceVec.size();
    if ( space_ < size_ + sizeOfsource ) {
        space_ = 1.5 * (size_ + sizeOfsource);
        reserve(space_);
    } 

    int sizeNew = size_ + sourceVec.size();
    for (int i = size_; i < sizeNew; ++i) {
        elem[ i ] = sourceVec[ i - size_ ];
    }

    size_ = sizeNew;
    return *this;
}

template <class T, class A>
typename CVector<T,A>::Iterator 
CVector<T,A>::end() 
{
    typename CVector<T,A>::Iterator beyondIterator(elem + size_);
    return beyondIterator;
}

template <class T, class A>
typename CVector<T,A>::const_Iterator 
CVector<T,A>::end() const
{
    typename CVector<T,A>::const_Iterator beyondIterator(elem + size_);
    return beyondIterator;
}

template <class T, class A>
typename CVector<T,A>::const_Iterator 
CVector<T,A>::begin() const
{
    if ( empty() ) {
        return end();
    }

    typename CVector<T,A>::const_Iterator iterOnFirst(elem);
    return iterOnFirst;
}

template <class T, class A>
typename CVector<T,A>::Iterator 
CVector<T,A>::begin() 
{
    if ( empty() ) {
        return end();
    }

    typename CVector<T,A>::Iterator iterOnFirst(elem);
    return iterOnFirst;
}

