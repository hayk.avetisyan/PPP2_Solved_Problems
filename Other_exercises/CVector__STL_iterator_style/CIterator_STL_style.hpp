class input_iterator_tag {};

class output_iterator_tag {};

class forward_iterator_tag : public input_iterator_tag { };

class bidirectional_iterator_tag : public forward_iterator_tag {};

class random_access_iterator_tag : public bidirectional_iterator_tag {};




template <class _Tp, class _Distance> struct input_iterator {
  typedef input_iterator_tag iterator_category;
  typedef _Tp                value_type;
  typedef _Distance          difference_type;
  typedef _Tp*               pointer;
  typedef _Tp&               reference;
};

struct output_iterator {
  typedef output_iterator_tag iterator_category;
  typedef void                value_type;
  typedef void                difference_type;
  typedef void                pointer;
  typedef void                reference;
};

template <class _Tp, class _Distance> struct forward_iterator {
  typedef forward_iterator_tag iterator_category;
  typedef _Tp                  value_type;
  typedef _Distance            difference_type;
  typedef _Tp*                 pointer;
  typedef _Tp&                 reference;
};


template <class _Tp, class _Distance> struct bidirectional_iterator {
  typedef bidirectional_iterator_tag iterator_category;
  typedef _Tp                        value_type;
  typedef _Distance                  difference_type;
  typedef _Tp*                       pointer;
  typedef _Tp&                       reference;
};

template <class _Tp, class _Distance> struct random_access_iterator {
  typedef random_access_iterator_tag iterator_category;
  typedef _Tp                        value_type;
  typedef _Distance                  difference_type;
  typedef _Tp*                       pointer;
  typedef _Tp&                       reference;
};

template <class _Category, class _Tp, class _Distance = std::ptrdiff_t,
          class _Pointer = _Tp*, class _Reference = _Tp&>
struct iterator {
  typedef _Category  iterator_category;
  typedef _Tp        value_type;
  typedef _Distance  difference_type;
  typedef _Pointer   pointer;
  typedef _Reference reference;
};

template<typename Iter>
class iterator_traits {
public:
    typedef  typename Iter::value_type value_type;
    typedef  typename Iter::pointer pointer; // pointer type
    typedef  typename Iter::difference_type difference_type;
    typedef  typename Iter::reference reference; // reference type
    typedef  typename Iter::iterator_category iterator_category; // (tag)
};

template<typename T>
class iterator_traits<T*> {
public:
    typedef  T value_type;
    typedef  T* pointer; // pointer type
    typedef  std::ptrdiff_t difference_type;
    typedef  T& reference; // reference type
    typedef  random_access_iterator_tag iterator_category; // (tag)
};

template<typename T>
class iterator_traits<const T&> {
public:
    typedef  T value_type;
    typedef  const T* pointer; // pointer type
    typedef  std::ptrdiff_t difference_type;
    typedef  const T& reference; // reference type
    typedef  random_access_iterator_tag iterator_category; // (tag)
};

template <class _Iter>
inline typename iterator_traits<_Iter>::iterator_category
__iterator_category(const _Iter&) 
{
    typedef typename iterator_traits<_Iter>::iterator_category _Category;
    return _Category();
}

template <class _Iter>
inline typename iterator_traits<_Iter>::difference_type*
__distance_type(const _Iter&)
{
    return static_cast<typename iterator_traits<_Iter>::difference_type*>(0);
}

template <class _Iter>
inline typename iterator_traits<_Iter>::value_type*
__value_type(const _Iter&)
{
    return static_cast<typename iterator_traits<_Iter>::value_type*>(0);
}

template <class _Iter>
inline typename iterator_traits<_Iter>::iterator_category
iterator_category(const _Iter& i) { return __iterator_category(i); }

template <class _Iter>
inline typename iterator_traits<_Iter>::difference_type*
distance_type(const _Iter& i) { return __distance_type(i); }

template <class _Iter>
inline typename iterator_traits<_Iter>::value_type*
value_type(const _Iter& i) { return __value_type(i); }



// iterator_category
template <class _Tp, class _Distance>
inline input_iterator_tag
iterator_category(const input_iterator<_Tp, _Distance>&)
{
    return input_iterator_tag();
}

inline output_iterator_tag
iterator_category(const output_iterator&) { return output_iterator_tag(); }

template <class _Tp, class _Distance>
inline forward_iterator_tag
iterator_category(const forward_iterator<_Tp, _Distance>&) { return forward_iterator_tag(); }

template <class _Tp, class _Distance>
inline bidirectional_iterator_tag
iterator_category(const bidirectional_iterator<_Tp, _Distance>&) { return bidirectional_iterator_tag(); }

template <class _Tp, class _Distance>
inline random_access_iterator_tag
iterator_category(const random_access_iterator<_Tp, _Distance>&) { return random_access_iterator_tag(); }

// value_type
template <class _Tp, class _Distance>
inline _Tp* value_type(const input_iterator<_Tp, _Distance>&)
{
    return (_Tp*)(0);
}

template <class _Tp, class _Distance>
inline _Tp* 
value_type(const forward_iterator<_Tp, _Distance>&) { return (_Tp*)(0); }

template <class _Tp, class _Distance>
inline _Tp* 
value_type(const bidirectional_iterator<_Tp, _Distance>&) { return (_Tp*)(0); }

template <class _Tp, class _Distance>
inline _Tp* 
value_type(const random_access_iterator<_Tp, _Distance>&) { return (_Tp*)(0); }

// distance_type
template <class _Tp, class _Distance>
inline _Distance* 
distance_type(const input_iterator<_Tp, _Distance>&) { return (_Distance*)(0); }

template <class _Tp, class _Distance>
inline _Distance* 
distance_type(const forward_iterator<_Tp, _Distance>&) { return (_Distance*)(0); }

template <class _Tp, class _Distance>
inline _Distance* 
distance_type(const bidirectional_iterator<_Tp, _Distance>&) { return (_Distance*)(0); }

template <class _Tp, class _Distance>
inline _Distance* 
distance_type(const random_access_iterator<_Tp, _Distance>&) { return (_Distance*)(0); }

template <class _Tp*>
inline std::ptrdiff_t* 
distance_type(const _Tp*) { return (std::ptrdiff_t*)(0); }

// __distance
template <class _InputIterator, class _Distance>
inline void 
__distance(_InputIterator __first, _InputIterator __last, _Distance& __n, input_iterator_tag)
{ 
    while (__first != __last) { 
        ++__first; 
        ++__n; 
    } 
}

template <class _RandomAccessIterator, class _Distance>
inline void 
__distance(_RandomAccessIterator __first, _RandomAccessIterator __last,
           _Distance& __n, random_access_iterator_tag)
{ 
    __n += __last - __first; 
}

template <class _InputIterator, class _Distance>
inline void 
distance(_InputIterator __first, _InputIterator __last, _Distance& __n)
{
    //__STL_REQUIRES(_InputIterator, _InputIterator);
    __distance(__first, __last, __n, iterator_category(__first));
}

// __distance with return value
template <class _InputIterator>
inline typename iterator_traits<_InputIterator>::difference_type
__distance(_InputIterator __first, _InputIterator __last, input_iterator_tag)
{
    typename iterator_traits<_InputIterator>::difference_type __n = 0;
    while (__first != __last) {
        ++__first;
        ++__n;
    }

    return __n;
}

template <class _RandomAccessIterator>
inline typename iterator_traits<_RandomAccessIterator>::difference_type
__distance(_RandomAccessIterator __first, 
           _RandomAccessIterator __last, 
           random_access_iterator_tag    )
{
    //__STL_REQUIRES(_RandomAccessIterator, _RandomAccessIterator);
    return __last - __first;
}

template <class _InputIterator>
inline typename iterator_traits<_InputIterator>::difference_type
distance(_InputIterator __first, _InputIterator __last)
{
    typedef typename iterator_traits<_InputIterator>::iterator_category Category;

    //__STL_REQUIRES(_InputIterator, _InputIterator);
    return __distance( __first, __last, iterator_category(__first) );
}

// advance
template <class _InputIterator, class _Distance>
inline void 
__advance(_InputIterator& __i, _Distance __n, 
                      input_iterator_tag)
{
    while(--__n) { ++__i; }
}

template <class _BidirectionalIterator, class _Distance>
inline void 
__advance(_BidirectionalIterator& __i, _Distance __n, 
                      bidirectional_iterator_tag)
{
    //__STL_REQUIRES(_BidirectionalIterator, _BidirectionalIterator);
    if (__n >= 0) {
        while (--__n) { ++__i; }
    } else {
        while (++__n) { ++__i; }
    }
}

template <class _RandomAccessIterator, class _Distance>
inline void 
__advance(_RandomAccessIterator& __i, _Distance __n, 
                      random_access_iterator_tag)
{
    //__STL_REQUIRES(_RandomAccessIterator, _RandomAccessIterator);
    __i += __n;
}


template <class InputIterator, class Distance>
inline void
advance(InputIterator& i, Distance n)
{
    __advance(i, n, iterator_category( i ) );
}
