//////// Tree structure simulation  ////////////////////////////////////
#include "CVector_STL_iterStyle.hpp"
#include <typeinfo>

template <class T, class A = Allocator<T> > 
struct testClass
{
    testClass()
        :intNumber(0)
    { }

    testClass(int intArg)
        :intNumber( intArg )
    { }

    testClass(const CVector<T,A>& sourceVec)
        :intNumber( sourceVec.size() )
    { }

    testClass(const testClass& sourceObj)
        :intNumber( sourceObj.intNumber )
    { }

    ~testClass()
    { }

    friend std::ostream& operator<<(std::ostream& sourceOS, const testClass& sourceObj)
    {
        sourceOS << sourceObj.intNumber;
        return sourceOS;
    }

    int intNumber;
};

template <class T, class A>
void printConst(const testClass<T,A>& sourceObj)
{
    std::cout << "sourceObj = " << sourceObj << "\n";
}

template <class T, class A>
void print(testClass<T,A>& sourceObj)
{
    std::cout << "sourceObj = " << sourceObj << "\n";
}

int main()
{
    try {
        std::cout << "#########################################################################\n"
                  << "################ TESTING OF \"CVector's member functions ################\n"
                  << "                                                                         \n";

        std::cout << "\"CVector() default-ctor\" by creating vecOfInts:\n";
        CVector<int> vecOfInts;

        /* gives an "exception: The vector is empty"
        std::cout << "  vecOfInts's elements\n";
        std::cout << vecOfInts << "\n";
        */ 
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n\n";

        std::cout << "\"push_back(const T& sourceT)\" by adding 10, 15, 20, 25 at vecOfInts:\n";
        vecOfInts.push_back(10);
        vecOfInts.push_back(15);
        vecOfInts.push_back(20);
        vecOfInts.push_back(25);
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n\n";

        std::cout << "\"operator<<\":\n";
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n\n";

        std::cout << "\"max_size()\":\n";
        std::cout << "  vecOfInts.max_size(): " << vecOfInts.max_size() << "\n\n";

        std::cout << "\"resize(1)\": vecOfInts.resize(1):\n";
        vecOfInts.resize(1);
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts's capacity: " << vecOfInts.capacity() << "\n";
        std::cout << "  vecOfInts[3]: " << vecOfInts[3] << "\n\n";

        std::cout << "\"resize(6)\": vecOfInts.resize(6):\n";
        vecOfInts.resize(6);
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts's capacity: " << vecOfInts.capacity() << "\n\n";

        std::cout << "\"resize(15)\": vecOfInts.resize(15):\n";
        vecOfInts.resize(15);
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts's capacity: " << vecOfInts.capacity() << "\n\n";

        std::cout << "\"empty()\": vecOfInts.empty():\n";
        if ( vecOfInts.empty() ) {
            std::cout << "  The vecOfInts is empty\n\n";
        } else {
            std::cout << "  The vecOfInts is not empty\n\n";
        }

        std::cout << "\"reserve(5)\": vecOfInts.reserve(15):\n";
        vecOfInts.reserve(5);
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts's capacity: " << vecOfInts.capacity() << "\n\n";

        std::cout << "\"reserve(20)\": vecOfInts.reserve(20):\n";
        vecOfInts.reserve(20);
        std::cout << "  vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts's capacity: " << vecOfInts.capacity() << "\n\n";

        std::cout << "\"at(10)\": vecOfInts.at(10):\n";
        std::cout << "  vecOfInts.at(10): " << vecOfInts.at(10) << "\n\n";

        /* gives an "exception: The index must be smaller than vector's size"
        std::cout << "\"at(27)\": vecOfInts.at(27):\n";
        std::cout << "  vecOfInts.at(27): " << vecOfInts.at(27) << "\n";
        */ 

        std::cout << "\"front()\":\n";
        std::cout << "  vecOfInts.front(): " << vecOfInts.front() << "\n\n";

        std::cout << "\"back()\":\n";
        std::cout << "  vecOfInts.back(): " << vecOfInts.back() << "\n\n";

        std::cout << "\"assign(10, 123)\":\n";
        vecOfInts.assign(10, 123);
        std::cout << "  vecOfInts.assign(10, 123): " << vecOfInts << "\n\n";


        std::cout << "\"CVector(int sizeOfVec) ctor\" by creating vecOfInts2(7):\n";
        CVector<int> vecOfInts2(7);
        std::cout << "  vecOfInts2's elements: " << vecOfInts2 << "\n\n";

        std::cout << "\"CVector(const CVector& sourceVec) copy-ctor by creating vecOfInts_copy\":\n\n";
        CVector<int> vecOfInts_copy(vecOfInts);

        std::cout << "       vecOfInts's size: " << vecOfInts.size() << "\n";
        std::cout << "  vecOfInts_copy's size: " << vecOfInts_copy.size() << "\n";
        std::cout << "       vecOfInts's elements: " << vecOfInts << "\n";
        std::cout << "  vecOfInts_copy's elements: " << vecOfInts_copy << "\n";

        vecOfInts_copy.push_back(333);
        vecOfInts_copy.push_back(444);

        std::cout << "\n";
        std::cout << "\"push_back\": Adding 333 and 444 at the vecOfInts_copy: \n";
        std::cout << "       vecOfInts.back() = " << vecOfInts.back() << "\n";
        std::cout << "  vecOfInts_copy.back() = " << vecOfInts_copy.back() << "\n";

        vecOfInts_copy.pop_back();
        vecOfInts_copy.pop_back();

        std::cout << "\n";
        std::cout << "\"pop_back()\": Removing last 2 elements (333, 444) at the vecOfInts_copy: \n";
        std::cout << "       vecOfInts.back() = " << vecOfInts.back() << "\n";
        std::cout << "  vecOfInts_copy.back() = " << vecOfInts_copy.back() << "\n\n";

        std::cout << "\"operator==\":\n";
        if (vecOfInts_copy == vecOfInts) {
            std::cout << "  vecOfInts_copy == vecOfInts\n";
        } else {
            std::cout << "  vecOfInts_copy != vecOfInts\n";
        }

        if (vecOfInts_copy == vecOfInts2) {
            std::cout << "  vecOfInts_copy == vecOfInts2\n";
        } else {
            std::cout << "  vecOfInts_copy != vecOfInts2\n";
        }

        std::cout << "\"operator+=\":\n";
        vecOfInts2 += vecOfInts;
        std::cout << "  vecOfInts2 += vecOfInts: \n";
        std::cout << "  vecOfInts2's elements: " << vecOfInts2 << "\n\n";

        // Iterators
        std::cout << "#########################################################################\n"
                  << "# TESTING OF \"CVector's Iterators and member functions with Iterators ##\n"
                  << "                                                                         \n";

        std::cout << "\"const_Iterator\"\n"
                  << "  vecOfInts's elements with const_Iterator:   ";
        
        for (CVector<int>::const_Iterator iterConst  = vecOfInts.begin(); 
                                          iterConst !=   vecOfInts.end(); ++iterConst) {
            std::cout << *iterConst << " ";
        }

        std::cout << "\n\n";
        std::cout << "\"Iterator\"\n"
                  << "  vecOfInts2's elements with Iterator:   ";
        for (CVector<int>::Iterator  iter  = vecOfInts2.begin(); 
                                     iter !=   vecOfInts2.end(); ++iter) {
            std::cout << *iter << " ";
        }
        std::cout << "\n\n";

        std::cout << "distance(vecOfInts.begin(), vecOfInts.end() ) = " 
                  << distance(vecOfInts.begin(), vecOfInts.end() ) << "\n";

        std::cout << "distance(vecOfInts2.begin(), vecOfInts2.end() ) = " 
                  << distance(vecOfInts2.begin(), vecOfInts2.end() ) << "\n";

        CVector<int>::Iterator  iter  = vecOfInts2.begin();
        std::cout << "*iter = " << *iter << "\n\n";
        advance(iter, 6);
        std::cout << "\"advance(iter, 6);\" =>  " << "*iter = " << *iter << "\n";
        advance(iter, 1);
        std::cout << "\"advance(iter, 1);\" =>  " << "*iter = " << *iter << "\n\n";

        testClass<int> testObj1 = vecOfInts2;
        std::cout << "testObj1 = " << testObj1 << "\n";

        std::cout << "static_cast<int>(vecOfInts2) = " << static_cast<int>(vecOfInts2) << "\n";
        std::cout << "static_cast<bool>(vecOfInts2) = " << static_cast<bool>(vecOfInts2) << "\n";
        std::cout << "static_cast<testClass<int>>(vecOfInts2) = " 
                  << static_cast< testClass<int> >(vecOfInts2) << "\n";
    
        CVector< testClass<int> > testVector; 
        testVector.push_back(0);
        testVector.push_back(1);
        testVector.push_back(2);
        testVector.push_back(3);
        CVector< testClass<int> >::const_Iterator iterOFtest = testVector.begin();
        std::cout << "*iterOFtest++ = " << *iterOFtest++ << "\n";
        std::cout << "iterOFtest -> intNumber = " << iterOFtest -> intNumber << "\n";

    } catch (std::exception& e) {
        std::cerr << "exception: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


