#ifndef CVector_HPP
#define CVector_HPP
// CVector class's definition. 
#include <iostream>
#include <exception>
#include <stdexcept>
#include <cmath>
#include <new>

#include <typeinfo>
#include "CIterator_STL_style.hpp"


template<typename T>
class Allocator {
public : 
    //    typedefs
    typedef T value_type;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef int size_type;
    typedef std::ptrdiff_t difference_type;

public : 
    //    convert an allocator<T> to allocator<U>
    template<typename U>
    struct rebind {
        typedef Allocator<U> other;
    };

public : 
    inline explicit Allocator() {}
    inline ~Allocator() {}
    inline explicit Allocator(Allocator const&) {}
    template<typename U>
    inline explicit Allocator(Allocator<U> const&) {}

    //    address
    inline pointer address(reference r) { return &r; }
    inline const_pointer address(const_reference r) { return &r; }

    //    memory allocation
    inline pointer allocate(size_type cnt, 
       typename std::allocator<void>::const_pointer = 0) { 
      return reinterpret_cast<pointer>(::operator new(cnt * sizeof (T))); 
    }
    inline void deallocate(pointer p, size_type) { 
        ::operator delete(p); 
    }

    //    construction/destruction
    inline void construct(pointer p, const T& t) { new(p) T(t); }
    inline void destroy(pointer p) { p->~T(); }

    inline bool operator==(Allocator const&) { return true; }
    inline bool operator!=(Allocator const& a) { return !operator==(a); }
};    //    end of class Allocator 


template <class T, class A = Allocator<T> >
class CVector
{
public:
    CVector(); // default constructor
    explicit CVector(int sizeOfVec); // constructor
    ~CVector();

               CVector(const CVector& sourceVec); // copy constructor
    CVector& operator=(const CVector& sourceVec); // assignment

    // Capacity
    inline int size() const { return size_; }
    unsigned long max_size() const;
    inline int capacity() const { return space_; }
    void resize(int sizeNew, T sourceT = T() );
    bool empty() const;
    void reserve(int spaceNew);

    // Element access
    inline const T& operator[](int index) const;
    inline       T& operator[](int index);

    const T& at(int index) const;
          T& at(int index); 

    const T& front() const;
          T& front(); 

    const T& back() const;
          T& back(); 

    // Modifiers
    void assign(int elemCount, const T& sourceT);          // fill       

    template <class InputIterator> 
    void assign(InputIterator fisrt, InputIterator last);  // range

    void push_back(const T& sourceT);
    void pop_back();

    class Iterator;

    Iterator insert(Iterator position, const T& sourceT);                        // single element
        void insert(Iterator position, int elemCount, const T& sourceT);         // fill

    template <class InputIterator> 
        void insert(Iterator position, InputIterator fisrt, InputIterator last); // range

    Iterator erase(Iterator position);
    Iterator erase(Iterator fisrt, Iterator last);

    void swap(const T& sourceT);
    void clear(const T& sourceT);

    CVector& operator+=(const CVector& sourceVec);

    // Relational operators
    bool operator==(const CVector& sourceVec) const;
    bool operator!=(const CVector& sourceVec) const;

    // Conversion operators
    inline operator int() const { return size_; }
    inline operator bool() const { return !empty(); }

    // Non-member functions
     friend void swap(CVector<T,A>& vec1, CVector<T,A>& vec2);
     
    template <class U, class ALLOC> 
        friend std::ostream& operator<<(std::ostream& os, const CVector<U,ALLOC>& vec);

    friend std::istream& operator>>(std::istream& is, CVector<T,A>& vec);
    friend CVector merge(const CVector<T,A>& sourceVec1, const CVector<T,A>& sourceVec2);
    friend CVector operator+(const CVector<T,A>& sourceVec1, const CVector<T,A>& sourceVec2);

    // Iterators
public:

    class const_Iterator : public random_access_iterator<T, std::ptrdiff_t> {
        public:
            typedef typename CVector<T,A>::const_Iterator self_type;

            inline const_Iterator()
                :pCurr_(NULL)
            { }

            inline const_Iterator(T* srcPointer)
                :pCurr_(srcPointer)
            { }

            inline const_Iterator(const self_type& srcIter)
                :pCurr_(srcIter.pCurr_)
            { }

            self_type& operator=(const self_type& srcIter)
            {
                pCurr_ = srcIter.pCurr_;
                return *this;
            }

            inline ~const_Iterator() { }

            inline const T& operator*() { return *pCurr_; }

            inline T* operator->() { return pCurr_; }
  
            self_type& operator++() //prefix increment
            {
                ++pCurr_;
                return *this;
            }

            self_type operator++(int junk) //postfix increment
            {
                self_type tempIter = *this;
                ++(*this);
                return tempIter;
            }

            self_type& operator--() //prefix increment
            {
                --pCurr_;
                return *this;
            }

            self_type operator--(int junk) //postfix increment
            {
                self_type tempIter = *this;
                --(*this);
                return tempIter;
            }

            inline T operator[](int index) const { return pCurr_[ index ]; }

            inline bool operator==(const self_type& srcIter) const
            {
                return (pCurr_ == srcIter.pCurr_);
            }

            inline bool operator!=(const self_type& srcIter) const
            {
                return (pCurr_ != srcIter.pCurr_);
            }

            friend self_type operator+(const self_type& srcIter, int intNumber)
            {
                self_type iterNew( srcIter.pCurr_ + intNumber );
                return iterNew;
            }

            friend self_type operator+(int intNumber, const self_type& srcIter)
            {
                return (srcIter + intNumber);
            }

            friend self_type operator-(const self_type& srcIter, int intNumber)
            {
                self_type iterNew( srcIter.pCurr_ - intNumber );
                return iterNew;
            }

            friend int operator-(const self_type& srcIter1, const self_type& srcIter2)
            {
                return srcIter1.pCurr_ - srcIter2.pCurr_;
            }

            friend bool operator<(const self_type& srcIter1, const self_type& srcIter2)
            {
                return (srcIter1.pCurr_ < srcIter2.pCurr_) ? true : false;
            }

            friend bool operator<=(const self_type& srcIter1, const self_type& srcIter2)
            {
                return (srcIter1.pCurr_ <= srcIter2.pCurr_) ? true : false;
            }

            friend bool operator>(const self_type& srcIter1, const self_type& srcIter2)
            {
                return !(srcIter1 <= srcIter2);
            }

            friend bool operator>=(const self_type& srcIter1, const self_type& srcIter2)
            {
                return !(srcIter1 < srcIter2);
            }

            self_type& operator+=(int intNumber)
            {
                *this = *this + intNumber;
                return *this;
            }

            self_type& operator-=(int intNumber)
            {
                *this = *this - intNumber;
                return *this;
            }

        protected:
            T* pCurr_;
    };

    class Iterator : public const_Iterator {
        public:
            typedef typename CVector<T,A>::Iterator self_type;

            inline Iterator()
                :const_Iterator()
            { }

            inline Iterator(T* srcPointer)
                :const_Iterator(srcPointer)
            { }

            inline Iterator(const self_type& srcIter)
                :const_Iterator(srcIter)
            { }

            self_type& operator=(const self_type& srcIter)
            {
                const_Iterator::pCurr_ = srcIter.const_Iterator::pCurr_;
                return *this;
            }

            inline ~Iterator() { }

            inline T& operator*()
            {
                return *const_Iterator::pCurr_;
            }
    };

          Iterator end();
    const_Iterator end() const;

          Iterator begin();
    const_Iterator begin() const;

          Iterator rend();
    const_Iterator rend() const;

          Iterator rbegin();
    const_Iterator rbegin() const;
protected:
    typename CVector<T,A>::Iterator 
    copy_CVector(typename CVector<T,A>::const_Iterator first, typename CVector<T,A>::const_Iterator last, typename CVector<T,A>::Iterator result);

private:
    int size_; 
    int space_; 
    A alloc;
    T * elem; 
}; // end of the CVector class declaration 

#include "CVector_STL_iterStyle__Functions.hpp"
#endif 

