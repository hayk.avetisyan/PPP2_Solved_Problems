//////// Tests of Deitel 8 chapter  ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        int* zPtr;
        void* sPtr = nullptr;
        int number;
        int z[5] = {1, 2, 3, 4, 5};

        cout << "/// Exercise 8.6 ///////////////////////////////////////////\n";
        /// a)
        cout << "a)\n";
        cout << "\tzPtr = " << zPtr << endl;
        ++zPtr;
        cout << "\t(++zPtr)  zPtr = " << zPtr << endl;

        cout << "\n\tright version:\n";
        zPtr = z;
        cout << "\t(zPtr = z)  zPtr = " << zPtr << endl;
        ++zPtr;
        cout << "\t(++zPtr)  zPtr = " << zPtr << endl;

        /// b)
        cout << "b)\n";
        zPtr = z;
        number = *zPtr;
        cout << "\t(number = *zPtr)  number = " << number << endl;

        /// c)
        number = zPtr[2];
        cout << "c)\n";
        cout << "\t(number = zPtr[2])  number = " << number << endl;

        /// d)
        cout << "d)\n";
        for (size_t index = 0; index <= 4; ++index) {
            cout << "\tz[" << index << "] = " << z[index] << endl;
        }

        /// e)
        cout << "e)\n";
        /// number = *static_cast<int*>(sPtr);

        /// f)
        cout << "f)\n";
        ++zPtr;
        cout << "\t(++zPtr)  *zPtr = " << *zPtr << endl;


    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

