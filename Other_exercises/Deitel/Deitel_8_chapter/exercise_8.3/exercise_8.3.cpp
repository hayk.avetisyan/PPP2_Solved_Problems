//////// Tests of Deitel 8 chapter  ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool); 
//////////////////////////////////////////////////////////////////////////////////////

/// Drill 4
void print_array10(ostream& os, int* pToInts)
{
    for (int index = 0; index < 10; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

/// Drill 7
void print_array(ostream& os, const int* pToInts, const int n)
{
    for (int index = 0; index < n; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

int main()
{
    try {
        /// TRY THIS page: 591
        char ch; 
        int i; 
        double d; 
        bool b; 
        char* pChar; 
        int* pInt; 
        double* pDouble;
        bool* pBool;
//        sizes(ch, i, d, b, pChar, pInt, pDouble, pBool);

        const int constInt = 5;
        const int* pToConstInt = &constInt;
        const void* pToConstVoid = &constInt;
        cout << "pToConstVoid = " << pToConstVoid << endl;
        cout << "pToConstInt = " << pToConstInt << endl;

        int someInt = -999;
        int* pToSomeInt = &someInt;
        void* pToVoid = &someInt;
        pToVoid = pToSomeInt;
        cout << "pToVoid = " << pToVoid << endl;

        cout << "\n/// Exercise 8.3 ///////////////////////////////////////////";
        const size_t size = 10;
        /// a)
        double numbers[size] = {0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
        /// b)
        double* nPtr = nullptr;

        /// c)
        cout << endl << "c)\n";
        for (int index = 0; index < size; ++index) {
            cout << "\tnumber[" << index << "] = " << setprecision(1)
                 << fixed << numbers[index] << endl;
        }

        /// d)
        nPtr = numbers;
        cout << endl << "d)\n";
        cout << "\tnPtr = numbers:     " << "nPtr = " << nPtr << endl;
        nPtr = &numbers[0];
        cout << "\tnPtr = &numbers[0]: " << "nPtr = " << nPtr << endl;

        /// e)
        cout << endl << "e)\n";
        for (int index = 0; index < size; ++index) {
            cout << "\t   *(nPtr + " << index << ") = " << setprecision(1) 
                 << fixed << *(nPtr + index) << endl;
        }

        /// f)
        cout << endl << "f)\n";
        for (int index = 0; index < size; ++index) {
            cout << "\t*(numbers + " << index << ") = " << setprecision(1) 
                 << fixed << *(numbers + index) << endl;
        }

        /// g)
        cout << endl << "g)\n";
        for (int index = 0; index < size; ++index) {
            cout << "\t       nPtr[" << index << "] = " << setprecision(1)
                 << fixed << nPtr[index] << endl;
        }

        /// h)
        cout << endl << "h)\n";
        cout << "\tnumbers[3]     = " << numbers[3] << endl;
        cout << "\t*(numbers + 3) = " << *(numbers + 3) << endl;
        cout << "\tnPtr[3]        = " << nPtr[3] << endl;
        cout << "\t*(nPtr + 3)    = " << *(nPtr + 3) << endl;

        /// i)
        cout << endl << "h)\n";
        cout << "\t*(nPtr + 8)     = " << *(nPtr + 8) << endl;

        /// j)
        cout << endl << "h)\n";
        nPtr = &numbers[5];
        cout << "\t*(nPtr -= 4)     = " << *(nPtr -= 4) << endl;


    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool) 
{
    cout << "the size of char is " << sizeof(char) << ' ' << sizeof (ch) << '\n';
    cout << "the size of int is " << sizeof(int) << ' ' << sizeof (i) << '\n';
    cout << "the size of double is " << sizeof(double) << ' ' << sizeof (d) << '\n';
    cout << "the size of bool is " << sizeof(bool) << ' ' << sizeof (b) << '\n';
    cout << "the size of char* is " << sizeof(char*) << ' ' << sizeof (pChar) << '\n';
    cout << "the size of int* is " << sizeof(int*) << ' ' << sizeof (pInt) << '\n';
    cout << "the size of double* is " << sizeof(double*) << ' ' << sizeof (pDouble) << '\n';
    cout << "the size of bool* is " << sizeof(bool*) << ' ' << sizeof (pBool) << '\n';
}    

