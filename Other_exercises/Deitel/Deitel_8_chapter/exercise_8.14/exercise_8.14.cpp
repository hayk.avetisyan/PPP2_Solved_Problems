//////// SOLUTION of the exercise_8.14  ////////////////////////////////////
/// mystery function returns the strings length 
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
int mystery2(const char *); // prototype
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char string1[ 80 ];
        cout << "Enter a string:\n";
        cin >> string1;
        cout << mystery2( string1 ) << endl;

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
// What does this function do?
int mystery2(const char *s)
{
    unsigned int x;

    for (x = 0; *s != '\0'; ++s) {
        ++x;
    }

    return x;
} // end function mystery2
