//////// SOLUTION of the exercise_8.9  ////////////////////////////////////
// Assume that unsigned integers are stored in two bytes and that the  
// starting address of the built-in array is at location 1002500 in memory
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        long value1 = 200000;
        long value2;

        /// a)
        long *longPtr;

        /// b)
        longPtr = &value1;

        /// c)
        cout << "c)\n";
        cout << "   *longPtr = " << *longPtr << endl;

        /// d)
        value2 = *longPtr;

        /// e)
        cout << "\ne)\n";
        cout << "   value2 = " << value2 << endl;

        /// f)
        cout << "\nf)\n";
        cout << "   &value1 = " << &value1 <<  endl;

        /// g)
        cout << "\ng)\n";
        cout << "   longPtr = " << longPtr << endl;

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

