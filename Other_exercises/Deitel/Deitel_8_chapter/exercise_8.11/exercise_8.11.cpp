//////// SOLUTION of the exercise_8.11  ////////////////////////////////////
// Assume that unsigned integers are stored in two bytes and that the  
// starting address of the built-in array is at location 1002500 in memory
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
void zero(long bigIntegers[]);
int add1AndSum(int oneTooSmall[]);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        /// a)
        cout << "\na)\n";
        int number = 10;
        int *pTrNumber = &number;
        cout << "   pTrNumber = " << pTrNumber << endl;

        /// b)
        cout << "\nb)\n";
        double *realPtr;
        int *integerPtr;

        /// c)
        cout << "\nc)\n";
        int *x, *y;
        x = y;
        cout << "   x = " << x << endl;

        /// d)
        cout << "\nd)\n";
        char s[] = "this is a character array";
        cout << "   s[] = " << s << endl;
        char *sPtr = s;
        cout << "   sPtr = " << sPtr << endl;
        for ( ; *sPtr != '\0'; ++sPtr) {
            cout << *sPtr << ' ';
        }
        cout << endl;

        /// e)
        cout << "\ne)\n";
        short number2 = 5;
        short *numPtr = &number2;
        short result;
        void *genericPtr = numPtr;
        result = *(static_cast<short*>(genericPtr)) + 7;
        cout << "   result = " << result << endl;

        /// f)
        cout << "\nf)\n";
        double x1 = 19.34;
        double *x1Ptr = &x1;
        cout << "   x1Ptr = " << x1Ptr << endl;

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void zero(long bigIntegers[])
{
}


int add1AndSum(int oneTooSmall[])
{
    return 0;
}
