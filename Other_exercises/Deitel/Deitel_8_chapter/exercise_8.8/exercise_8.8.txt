////////////// ANSWERS of the exercise_8_7 //////////////////////

a)  TRUE

b)  FALSE, because:
    for example the char array names during printing yields the whole string rather than 
    the address of the first element.
