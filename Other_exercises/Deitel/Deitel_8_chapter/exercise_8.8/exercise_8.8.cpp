//////// SOLUTION of the exercise_8.8  ////////////////////////////////////
// Assume that unsigned integers are stored in two bytes and that the  
// starting address of the built-in array is at location 1002500 in memory
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        /// a)
        const int SIZE = 5;
        unsigned int values[SIZE] = {2, 4, 6, 8, 10};

        /// b)
        unsigned int *vPtr;

        /// c)
        cout << "c) Use a for statement to display the elements of built-in\n"
             << "   array values using array subscript notation \n";
        for (size_t index = 0; index < SIZE; ++index) {
            cout << "   values[" << index << "] = " << values[index] << endl;
        }

        /// d)
        cout << "\nd) Write two separate statements that assign the starting\n"
             << "   address of built-in array values to pointer variable vPtr\n";
        vPtr = values; 
        cout << "   (vPtr = values):     vPtr = " << vPtr << endl;
        vPtr = &values[0];
        cout << "   (vPtr = &values[0]): vPtr = " << vPtr << endl;

        /// e)
        cout << "\ne) Use a for statement to display the elements of built-in\n"
             << "   array values using pointer/offset notation \n";
        for (size_t index = 0; index < SIZE; ++index) {
            cout << "   *(vPtr + " << index << ") = " << *(vPtr + index) << endl;
        }

        /// f)
        cout << "\nf) Use a for statement to display the elements of built-in array values\n"
             << "   using pointer/offset notation with the built-in array’s name as the pointer\n";
        for (size_t index = 0; index < SIZE; ++index) {
            cout << "   *(values + " << index << ") = " << *(values + index) << endl;
        }

        /// g)
        cout << "\ng) Use a for statement to display the elements of built-in\n"
             << "   array values by subscripting the pointer to the built-in array \n";
        for (size_t index = 0; index < SIZE; ++index) {
            cout << "   vPtr[" << index << "] = " << vPtr[index] << endl;
        }

        /// h)
        cout << "\nh) Refer to the fifth element of values using array subscript notation\n"
             << "   pointer/offset notation with the built-in array name’s as the pointer,\n"
             << "   pointer subscript notation and pointer/offset notation\n";
        cout << "   values[4] = " << values[4] << endl;
        cout << "   *(values + 4) = " << *(values + 4) << endl;
        cout << "   vPtr[4] = " << vPtr[4] << endl;
        cout << "   *(vPtr + 4) = " << *(vPtr + 4) << endl;

        /// i)
        cout << "\ni) What address is referenced by vPtr + 3? What value is stored\n"
             << "   at that location?\n";
        cout << "   Address of vPtr + 3 is 1002500 + 3 * 2 = 1002506\n"
             << "   Value of vPtr + 3 is *(vPtr + 3) = " << *(vPtr + 3) << endl;

        /// j)
        cout << "\nj) Assuming that vPtr points to values[4], what address is referenced\n"
             << "   by vPtr -= 4?  What value is stored at that location?\n";
        cout << "   Address of vPtr is 1002500 + 4 * 2 = 1002508\n"
             << "   vPtr -= 4 is 1002508 - 4 * 2 = 1002500, and the value at the vPtr is 2\n";

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

