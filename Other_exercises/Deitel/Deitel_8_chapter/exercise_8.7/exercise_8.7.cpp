//////// Tests of Deitel 8 chapter  ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char str1C_style[] = "First C_style string";
        char *pStr1C_style = str1C_style;

        char str2C_style[] = "Second C_style string";
        char *pStr2C_style = str2C_style;

        cout << "str1C_style = " << str1C_style << endl;
        cout << "str2C_style = " << str2C_style << endl;

        cout << endl;
        if (pStr1C_style == pStr2C_style) {
            cout << "pStr1C_style == pStr2C_style" << endl;
        } else {
            cout << "pStr1C_style != pStr2C_style" << endl;
        }

        cout << "pStr1C_style = " << pStr1C_style << endl;
        cout << "pStr2C_style = " << pStr2C_style << endl;
        cout << "*pStr1C_style = " << *pStr1C_style << endl;
        cout << "*pStr2C_style = " << *pStr2C_style << endl;

        cout << "&pStr1C_style[1] = " << &pStr1C_style[1] << endl;
        cout << "&pStr2C_style[1] = " << &pStr2C_style[1] << endl;

        cout << endl;
        if (&pStr1C_style[1] > &pStr1C_style[0]) {
            cout << "&pStr1C_style[1] > &pStr1C_style[0]\n";
        } else {
            cout << "&pStr1C_style[1] <= &pStr1C_style[0]\n";
        }

        ++pStr1C_style;
        ++pStr2C_style;

        cout << endl;
        cout << "After incrementing pStr1C_style and pStr2C_style:\n";
        cout << "pStr1C_style = " << pStr1C_style << endl;
        cout << "pStr2C_style = " << pStr2C_style << endl;
        cout << "*pStr1C_style = " << *pStr1C_style << endl;
        cout << "*pStr2C_style = " << *pStr2C_style << endl;

        cout << endl;
        if (str1C_style == &str1C_style[0]) {
            cout << "str1C_style == &str1C_style[0]" << endl;
        } else {
            cout << "str1C_style != &str1C_style[0]" << endl;
        }

        if (pStr1C_style == &pStr1C_style[0]) {
            cout << "pStr1C_style == &pStr1C_style[0]" << endl;
        } else {
            cout << "pStr1C_style != &pStr1C_style[0]" << endl;
        }

        cout << endl;
        cout << "(int*)pStr1C_style == " << (int*)pStr1C_style << endl;
        cout << "reinterpret_cast<void*>(pStr1C_style) == " 
             << reinterpret_cast<void*>(pStr1C_style) << endl;
        cout << "(int*)pStr2C_style == " << (int*)pStr2C_style << endl;
        cout << "reinterpret_cast<void*>(pStr2C_style) == " 
             << reinterpret_cast<void*>(pStr2C_style) << endl;

        int intArray1[] = {1, 3, 5};
        int intArray2[] = {2, 4, 6};

        cout << "\n\n";
        cout << "intArray1 = " << intArray1 << endl;
        cout << "intArray2 = " << intArray2 << endl;
        cout << "&intArray1[1] = " << &intArray1[1] << endl;
        cout << "&intArray1[0] = " << &intArray1[0] << endl;
        if (&intArray1[1] > &intArray1[0]) {
            cout << "&intArray1[1] > &intArray1[0]\n";
        }

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

