//////// SOLUTION of the exercise_8.10  ////////////////////////////////////
// Assume that unsigned integers are stored in two bytes and that the  
// starting address of the built-in array is at location 1002500 in memory
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
void zero(long bigIntegers[]);
int add1AndSum(int oneTooSmall[]);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        long bigIntegers[3] = {150, 857, 963};
        zero(bigIntegers);

        int oneTooSmall[3] = {1, 2, 3};
        int intNumber = add1AndSum(oneTooSmall);



    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void zero(long bigIntegers[])
{
}


int add1AndSum(int oneTooSmall[])
{
    return 0;
}
