//////// Tests of Deitel 8 chapter  ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void exchange(double* x, double* y);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        cout << "\n/// Exercise 8.5 ///////////////////////////////////////////\n";
        cout << "c)\n";
        char vowel1[] = "AEIOU";
        cout << "vowel1[] = \"AEIOU\";\n";
        for (int index = 0; index < 5; ++index) {
            cout << "\tvowel1[" << index << "] = " << vowel1[index] << endl;
        }

        char vowel2[] = {'A', 'E', 'I', 'O', 'U', '\0'};
        cout << "vowel2[] = {'A', 'E', 'I', 'O', 'U', '\\0'};\n";
        for (int index = 0; index < 5; ++index) {
            cout << "\tvowel2[" << index << "] = " << vowel2[index] << endl;
        }

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

