//////// Tests of Deitel 8 chapter  ////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        cout << "\n/// Exercise 8.4 ///////////////////////////////////////////\n";
        double number1 = 7.3;
        double number2;
        /// a)
        double* fPtr = nullptr;
        cout << "a)\n";
        cout << "\tfPtr = " << fPtr << "\n\n";

        /// b)
        fPtr = &number1;
        cout << "b)\n";
        cout << "\t(fPtr = &number1)  fPtr = " << fPtr << "\n\n";

        /// c)
        cout << "c)\n";
        cout << "\t*fPtr = " << *fPtr << "\n\n";

        /// d)
        number2 = *fPtr;
        cout << "d)\n";
        cout << "\t(number2 = *fPtr)  number2 = " << number2 << "\n\n";

        /// e)
        cout << "\ne)\n";
        cout << "\tnumber2 = " << number2 << "\n\n";

        /// f)
        cout << "\nf)\n";
        cout << "\t&number1 = " << &number1 << "\n\n";

        /// g)
        cout << "\ng)\n";
        cout << "\tfPtr = " << fPtr << "\n\n";

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

