//////// SOLUTION of the exercise_8.13  ////////////////////////////////////
/// mystery function concates the string2 to string1
////////////////////////////////////////////////////////////////////////
#include "../../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
void mystery1( char *, const char * ); // prototype
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char string1[80];
        char string2[80];
        cout << "Enter two strings: ";
        cin >> string1 >> string2;
        mystery1( string1, string2 );
        cout << string1 << endl;

    } catch (exception& e) {
            cerr << "exception: " << e.what() << endl;
            return 1;
    } catch (...) {
            cerr << "exception\n";
            return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// mystery function concates the string2 to string1
void mystery1(char *s1, const char *s2)
{
    while (*s1 != '\0') {
        ++s1;
    }

    for ( ; (*s1 = *s2); ++s1, ++s2) {
        ; // empty statement
    }
} // end function mystery1
