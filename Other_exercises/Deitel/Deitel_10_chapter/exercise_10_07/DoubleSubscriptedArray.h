//////// DoubleSubscriptedArray Class Definition  ////////////////////////////////////
#ifndef DoubleSubscriptedArray_H
#define DoubleSubscriptedArray_H
#include <iostream>

class DoubleSubscriptedArray {

    friend std::ostream &operator<<(std::ostream &outStream, const DoubleSubscriptedArray &arr);
    friend std::istream &operator>>(std::istream &inStream, DoubleSubscriptedArray &arr);

public:
    DoubleSubscriptedArray(int rowSizeArg, int colSizeArg); // constructor with two arguments
    DoubleSubscriptedArray(const DoubleSubscriptedArray& arr); // copy constructor

    ~DoubleSubscriptedArray(); // Destructor
    size_t getRowSize() const { return rowSize; } // getting row-size of array
    size_t getColumnSize() const { return colSize; } // getting column-size of array

    int operator()(int row, int column) const; // subscript operator for rvalue
    int &operator()(int row, int column); // subscript operator for rvalue

    bool operator==(const DoubleSubscriptedArray& arr) const; // equality
    bool operator!=(const DoubleSubscriptedArray& arr) const {return !(arr == *this);} // unequality
    const DoubleSubscriptedArray &operator=(const DoubleSubscriptedArray& arr); // assignment
private:
    size_t rowSize;
    size_t colSize;
    int **pptr;
    void assignZero();
    void assignMembers(const DoubleSubscriptedArray &arr);
    void buildColumns(int **pptr, int rowSize, int colSize);
};
    
#endif
