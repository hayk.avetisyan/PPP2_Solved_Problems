//////// DoubleSubscriptedArray Class's function definitions  ///////////////////////////
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include "DoubleSubscriptedArray.h"

void DoubleSubscriptedArray::assignZero()
{
    for (size_t i = 0; i < rowSize; ++i) { // assigning all members to be 0
        for (size_t j = 0; j < colSize; ++j) {
            pptr[i][j] = 0;
        }
    }
}

void DoubleSubscriptedArray::assignMembers(const DoubleSubscriptedArray &arr)
{
    for (size_t i = 0; i < rowSize; ++i) { // assign arr's members to this' members
        for (size_t j = 0; j < colSize; ++j) {
            pptr[i][j] = arr.pptr[i][j];
        }
    }
}

void DoubleSubscriptedArray::buildColumns(int **pptr, int rowSize, int colSize) 
{
    for (size_t i = 0; i < rowSize; ++i) { // create column list
        pptr[i] = new int[colSize];
    }
}

// Constructor with two arguments
DoubleSubscriptedArray::DoubleSubscriptedArray(int rowSizeArg, int colSizeArg)
    :rowSize(rowSizeArg > 0 ? rowSizeArg : 
        throw std::invalid_argument("Error 1: row-size must be greater than 0\n")),
    colSize(colSizeArg > 0 ? colSizeArg : 
        throw std::invalid_argument("Error 1: column-size must be greater than 0\n")),
    pptr(new int*[rowSize])
{
    buildColumns(pptr, rowSize, colSize); // creation of columns
    assignZero();  // initializing zero all members of the array
}

// Copy constructor 
DoubleSubscriptedArray::DoubleSubscriptedArray(const DoubleSubscriptedArray& arr)
    :rowSize(arr.getRowSize()), colSize(arr.getColumnSize()), pptr(new int*[rowSize])
{
    buildColumns(pptr, rowSize, colSize); // creation of columns
    assignMembers(arr);  // assigning all members of arr-object to current array
}

DoubleSubscriptedArray::~DoubleSubscriptedArray() // Destructor
{
    for (size_t i = 0; i < colSize; ++i) {
        delete [] pptr[i];
    }

    delete [] pptr;
}

int DoubleSubscriptedArray::operator()(int row, int column) const
{
    if (row < 0 || row >= rowSize) {
        throw std::out_of_range("Error 2: row is out of range\n");
    }

    if (column < 0 || column >= colSize) {
        throw std::out_of_range("Error 2: column is out of range\n");
    }

    return pptr[row][column];
}

int& DoubleSubscriptedArray::operator()(int row, int column)
{
    if (row < 0 || row >= rowSize) {
        throw std::out_of_range("Error 2: row is out of range\n");
    }

    if (column < 0 || column >= colSize) {
        throw std::out_of_range("Error 2: column is out of range\n");
    }

    return pptr[row][column];
}


bool DoubleSubscriptedArray::operator==(const DoubleSubscriptedArray& arr) const
{
    if (arr.getRowSize() != rowSize || arr.getColumnSize() != colSize) return false;

    for (size_t i = 0; i < rowSize; ++i) {
        for (size_t j = 0; j < colSize; ++j) {
            if (pptr[i][j] != arr.pptr[i][j]) return false;
        }
    }
    return true;
}

// assignment
const DoubleSubscriptedArray& DoubleSubscriptedArray::operator=(const DoubleSubscriptedArray& arr)
{
    if (arr != *this) {
        if (rowSize != arr.getRowSize()) {
            for (size_t i = 0; i < rowSize; ++i) {
                delete [] pptr[i];
            }

            delete [] pptr;
            rowSize = arr.getRowSize();
            pptr = new int*[rowSize];
            buildColumns(pptr, rowSize, colSize);
        }
        assignMembers(arr);
    }

    return *this;
}

std::ostream &operator<<(std::ostream &outStream, const DoubleSubscriptedArray &arr)
{
    const int ROW_SIZE = arr.getRowSize();
    const int COLUMN_SIZE = arr.getColumnSize();
    for (size_t i = 0; i < ROW_SIZE; ++i) {
        outStream << std::fixed << std::setw(12) << "";
        for (size_t j = 0; j < COLUMN_SIZE; ++j) {
            outStream << std::setw(2) << arr.pptr[i][j] << " ";
        }
        outStream << std::endl; // new line
    }

    return outStream;
}

std::istream &operator>>(std::istream &inStream, DoubleSubscriptedArray &arr)
{
    const int ROW_SIZE = arr.getRowSize();
    const int COLUMN_SIZE = arr.getColumnSize();
    for (size_t i = 0; i < ROW_SIZE; ++i) {
        for (size_t j = 0; j < COLUMN_SIZE; ++j) {
            int intNumber = -999;
            inStream >> intNumber;
            arr.pptr[i][j] = intNumber;
        }
    }

    return inStream;
}

