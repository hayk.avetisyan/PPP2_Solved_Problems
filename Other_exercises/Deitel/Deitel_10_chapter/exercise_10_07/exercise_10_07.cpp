//////// SOLUTION of exercise_10_07  ////////////////////////////////////
#include "../../useful_functions_for_Deitel.h"
#include <iostream>
#include <iomanip>
#include <string>

#include "DoubleSubscriptedArray.h"

int main()
{
    std::cout << "// Delcaring integers1(5,5)\n";
    DoubleSubscriptedArray integers1(5, 5);
    std::cout << "integers1 = \n" << integers1; 
    std::cout << "integers1(2,3) = " << integers1(2,3) << "\n";

    std::cout << "\n// Changing integers1\n";
    const int ROW_SIZE = integers1.getRowSize();
    const int COLUMN_SIZE = integers1.getColumnSize();
    for (size_t i = 0; i < ROW_SIZE; ++i) {
        for (size_t j = 0; j < COLUMN_SIZE; ++j) {
            integers1(i,j) = i * 10 + j;
        }
    }

    std::cout << "integers1 = \n" << integers1; 
    std::cout << "integers1(2,3) = " << integers1(2,3) << "\n";

    std::cout << "\n// Delcaring and inserting integers2(5,5)\n";
    DoubleSubscriptedArray integers2(5, 5);
    std::cin >> integers2;
    std::cout << "integers2 = \n" << integers2; 
    checkEquality(integers1,integers2, "integers1", "integers2");
    
    std::cout << "\n// Delcaring and inserting integers3(5,5)\n";
    DoubleSubscriptedArray integers3(5, 5);
    std::cin >> integers3;
    std::cout << "integers3 = \n" << integers3; 
    checkEquality(integers1,integers3, "integers1", "integers3");

    std::cout << "\n// Delcaring integers4(integers3) with copy constructor";
    DoubleSubscriptedArray integers4(integers3);
    std::cout << "\nintegers4 = \n" << integers4; 
    checkUnequality(integers1,integers4, "integers1", "integers4");
    checkUnequality(integers3,integers4, "integers3", "integers4");

    std::cout << "\n// Assign integers4 to integers1";
    integers1 = integers4;
    std::cout << "\nintegers1 = \n" << integers1 << "\n"; 
    checkEquality(integers1,integers4, "integers1", "integers4");

    return 0;
}    


