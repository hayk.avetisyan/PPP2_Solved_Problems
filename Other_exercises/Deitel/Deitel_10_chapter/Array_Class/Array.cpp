//////// Array Class's function definitions  ///////////////////////////
#include <iostream>
#include <stdexcept>
#include "Array.h"

std::ostream &operator<<(std::ostream &outStream, const Array &arr)
{
    const int ARRAY_SIZE = arr.getSize();
    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        outStream << arr.ptr[i] << " ";
    }

    return outStream;
}

std::istream &operator>>(std::istream &inStream, Array &arr)
{
    const int ARRAY_SIZE = arr.getSize();
    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        int intNumber = -999;
        inStream >> intNumber;
        arr.ptr[i] = intNumber;
    }

    return inStream;
}

Array::Array(int arrSize)
    :size(arrSize > 0 ? arrSize : 
        throw std::invalid_argument("Error 1: array-size must be greater than 0\n")),
    ptr(new int[size])
{
    for (size_t i = 0; i < size; ++i) {
        ptr[i] = 0;
    }
}

int Array::operator[](int index) const
{
    if (index < 0 || index >= size) {
        throw std::out_of_range("Error 2: index is out of range\n");
    }

    return ptr[index];
}

int& Array::operator[](int index)
{
    if (index < 0 || index >= size) {
        throw std::out_of_range("Error 2: index is out of range\n");
    }

    return ptr[index];
}

Array::Array(const Array& arr)
    :size(arr.getSize()), 
    ptr(new int[size])
{
    for (size_t i = 0; i < size; ++i) {
        ptr[i] = arr[i];
    }
}


bool Array::operator==(const Array& arr) const
{
    if (this == &arr) return true;

    if (arr.getSize() == size) {
        for (size_t i = 0; i < size; ++i) {
            if (ptr[i] != arr[i]) return false;
        }

        return true;
    }

    return false;
}

const Array& Array::operator=(const Array& arr)
{
    if (arr != *this) {
        if (size != arr.getSize()) {
            delete [] ptr;
            size = arr.getSize();
            ptr = new int[size];
        }

        for (size_t i = 0; i < size; ++i) {
            ptr[i] = arr[i];
        }
    }

    return *this;
}
