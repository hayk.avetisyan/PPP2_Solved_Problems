//////// Array_Class_test_prog.cpp  ////////////////////////////////////
#include "Array.h"

int main()
{
    int x = 10;
    Array integers1(7);
    std::cout << "integers1 = " << integers1 << "\n"; 

    Array integers2(integers1);
    std::cout << "integers2 = " << integers2 << "\n"; 

    const int integers1Size = integers1.getSize();
    for (size_t i = 0; i < integers1Size; ++i) {
        integers1[i] = i;
    }

    std::cout << "\n";
    std::cout << "integers1 = " << integers1 << "\n"; 
    std::cout << "integers2 = " << integers2 << "\n"; 


    return 0;
}    

