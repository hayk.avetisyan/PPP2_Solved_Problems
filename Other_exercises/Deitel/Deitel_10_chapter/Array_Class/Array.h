//////// Array Class Definition  ////////////////////////////////////
#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>

class Array {

    friend std::ostream &operator<<(std::ostream &outStream, const Array &arr);
    friend std::istream &operator>>(std::istream &inStream, Array &arr);

public:
    explicit Array(int arrSize = 10); // Default constructor
    ~Array() { delete [] ptr; } // Destructor
    size_t getSize() const { return size; } // getting size of array

    int operator[](int index) const; // subscript operator for rvalue
    int &operator[](int index); // subscript operator for rvalue
    Array(const Array& arr); // copy constructor

    bool operator==(const Array& arr) const; // equality
    bool operator!=(const Array& arr) const {return !(arr == *this);} // unequality
    const Array &operator=(const Array& arr); // assignment operator


    //bool operator>(const Array& arr) const; // greater operator
    //bool operator<(const Array& arr) const; // less operator
    //const Array &operator+=(const Array& arr); // adding operator

private:
    size_t size;
    int *ptr;
};
    
#endif
