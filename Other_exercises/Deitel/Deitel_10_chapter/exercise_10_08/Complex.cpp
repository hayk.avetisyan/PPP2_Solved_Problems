// Complex class member-function definitions.
#include <iostream>
#include "Complex.h" // Complex class definition

// Constructor
Complex::Complex( double realPart, double imaginaryPart ) 
   : real( realPart ),
   imaginary( imaginaryPart ) 
{ 
   // empty body
} // end Complex constructor

// Copy constructor
Complex::Complex( const Complex &compToBecopied ) 
   : real( compToBecopied.real ),
   imaginary( compToBecopied.imaginary ) 
{ 
   // empty body
} // end Complex constructor

// addition operator
Complex Complex::operator+( const Complex &operand2 ) const
{
   return Complex( real + operand2.real, 
      imaginary + operand2.imaginary );
} // end function operator+

// subtraction operator
Complex Complex::operator-( const Complex &operand2 ) const
{
   return Complex( real - operand2.real, 
      imaginary - operand2.imaginary );
} // end function operator-

// multiplication operator
Complex Complex::operator*( const Complex &operand2 ) const
{
   return Complex( real * operand2.real - imaginary * operand2.imaginary, 
      real * operand2.imaginary + operand2.real * imaginary );
} // end function operator*

// comparison operator
bool Complex::operator==( const Complex &operand2 ) const
{
    if (real == operand2.real && imaginary == operand2.imaginary) {
        return true;
    }

    return false;
} // end function operator==

// display a Complex object in the form: (a, b)
std::ostream &operator<<(std::ostream& outStream, const Complex &complexNumber)
{ 
    outStream << '(' << complexNumber.real << ", " << complexNumber.imaginary << ')';
    return outStream;
} // end function print

// input a Complex object in the form: (a, b)
std::istream &operator>>(std::istream& inStream,  Complex &complexNumber) 
{ 
    inStream.ignore(); // skip (
    inStream >> complexNumber.real;
    inStream.ignore(2); // skip comma and space
    inStream >> complexNumber.imaginary;
    inStream.ignore(); // skip )

    return inStream;
} // end function print

