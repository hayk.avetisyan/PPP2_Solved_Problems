//////// SOLUTION of exercise_10_08  ////////////////////////////////////
#include "../../useful_functions_for_Deitel.h"
#include <iostream>
#include "Complex.h"

int main()
{
   Complex x;
   Complex y( 4.3, 8.2 );
   Complex z( 3.3, 1.1 );

   std::cout << "x: " << x;
   std::cout << "\ny: " << y;
   std::cout << "\nz: " << z;

   x = y + z;
   std::cout << "\n\nx = y + z:" << std::endl;
   std::cout << x;
   std::cout << " = ";
   std::cout << y;
   std::cout << " + ";
   std::cout << z;

   x = y - z;
   std::cout << "\n\nx = y - z:" << std::endl;
   std::cout << x;
   std::cout << " = ";
   std::cout << y;
   std::cout << " - ";
   std::cout << z;
   std::cout << std::endl;

   x = y * z;
   std::cout << "\n\nx = y * z:" << std::endl;
   std::cout << x;
   std::cout << " = ";
   std::cout << y;
   std::cout << " * ";
   std::cout << z;
   std::cout << std::endl;

   std::cout << std::endl;
   checkEquality(x,y,"x","y");
   checkEquality(x,(y * z),"x","(y * z)");

   return 0;
} // end main


