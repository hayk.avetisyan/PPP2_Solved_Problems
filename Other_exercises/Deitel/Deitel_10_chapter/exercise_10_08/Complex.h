// Complex class definition.
#ifndef COMPLEX_H
#define COMPLEX_H
#include <iostream>

class Complex
{
    friend std::ostream &operator<<(std::ostream& outStream, const Complex& complexNumber);
    friend std::istream &operator>>(std::istream& inStream,  Complex& complexNumber);

public:
   explicit Complex( double = 0.0, double = 0.0 ); // constructor
   Complex( const Complex & ); // subtraction
   Complex operator+( const Complex & ) const; // addition
   Complex operator-( const Complex & ) const; // subtraction
   Complex operator*( const Complex & ) const; // subtraction
   bool operator==( const Complex & ) const; // subtraction
private:
   double real; // real part
   double imaginary; // imaginary part
}; // end class Complex

#endif

