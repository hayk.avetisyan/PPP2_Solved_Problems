// HugeInt member-function and friend-function definitions.
#include "../../useful_functions_for_Deitel.h"
#include <cctype> // isdigit function prototype
#include "Hugeint.h" // HugeInt class definition

size_t HugeInt::indexOfNonZero(const HugeInt &num) const
{
    size_t i = 0;
    while (i < HugeInt::digits && 0 == num.integer[ i ]) {
        ++i;
    } // skip leading zeros

   return i;
}

size_t HugeInt::indexOfNonZero() const
{
    size_t i = 0;
    while (i < HugeInt::digits && 0 == integer[ i ]) {
        ++i;
    } // skip leading zeros

   return i;
}

void HugeInt::assignZero()
{
   for ( short &element : integer )
      element = 0;   
}

// overloaded output operator
std::ostream& operator<<( std::ostream &output, const HugeInt &num )
{
   size_t i = num.indexOfNonZero(num);

   if ( i == HugeInt::digits )
      output << 0;
   else
      for ( ; i < HugeInt::digits; ++i )
         output << num.integer[ i ];

   return output;
} // end function operator<<

// default constructor; conversion constructor that converts
// a long integer into a HugeInt object
HugeInt::HugeInt( long value )
{
   // initialize array to zero
   assignZero();

   // place digits of argument into array 
   for ( size_t j = digits - 1; value != 0 && j >= 0; --j )
   {
      integer[ j ] = value % 10;
      value /= 10;
   } // end for
} // end HugeInt default/conversion constructor

// conversion constructor that converts a character std::string 
// representing a large integer into a HugeInt object
HugeInt::HugeInt( const std::string &number )
{
   // initialize array to zero
    assignZero();

   // place digits of argument into array
   size_t length = number.size();

   for ( size_t j = digits - length, k = 0; j < digits; ++j, ++k )
      if ( isdigit( number[ k ] ) ) // ensure that character is a digit
         integer[ j ] = number[ k ] - '0';
} // end HugeInt conversion constructor

// addition operator; HugeInt + HugeInt
HugeInt HugeInt::operator+( const HugeInt &op2 ) const
{
   HugeInt temp; // temporary result
   int carry = 0;

   for ( int i = digits - 1; i >= 0; --i )
   {
      temp.integer[ i ] = integer[ i ] + op2.integer[ i ] + carry;

      // determine whether to carry a 1
      if ( temp.integer[ i ] > 9 )
      {
         temp.integer[ i ] %= 10;  // reduce to 0-9
         carry = 1;
      } // end if
      else // no carry 
         carry = 0;
   } // end for

   return temp; // return copy of temporary object
} // end function operator+

// addition operator; HugeInt + int
HugeInt HugeInt::operator+( int op2 ) const
{ 
   // convert op2 to a HugeInt, then invoke 
   // operator+ for two HugeInt objects
   return *this + HugeInt( op2 ); 
} // end function operator+

// addition operator;
// HugeInt + std::string that represents large integer value
HugeInt HugeInt::operator+( const std::string &op2 ) const
{ 
   // convert op2 to a HugeInt, then invoke 
   // operator+ for two HugeInt objects
   return *this + HugeInt( op2 ); 
} // end operator+


// comparison operator; HugeInt > HugeInt
bool HugeInt::operator>( const HugeInt &op2 ) const
{ 
    size_t lowLimitThis = indexOfNonZero(*this);
    size_t lowLimitop2 = indexOfNonZero(op2);

    if (lowLimitThis < lowLimitop2) return true;
    else if (lowLimitThis > lowLimitop2) return false;

    for (size_t i = lowLimitThis; i < digits; ++i) {
        if (integer[ i ] > op2.integer[ i ]) return true;
        else if (integer[ i ] < op2.integer[ i ]) return false;
    }

    return false;
} // end operator>

// subtraction operator; HugeInt - HugeInt
HugeInt HugeInt::operator-( const HugeInt &op2 ) const    
{
    HugeInt copyOfThis = *this; // copy this object
    size_t indexOfZero = indexOfNonZero(op2) - 1; // index of zero element
    for (size_t i = digits - 1; i > indexOfZero; --i) {
        if (copyOfThis.integer[ i ] >= op2.integer[i]) {
            copyOfThis.integer[ i ] = copyOfThis.integer[ i ] - op2.integer[ i ];
        } else {
            copyOfThis.integer[ i ] = 10 + copyOfThis.integer[ i ] - op2.integer[ i ];
            size_t j = i - 1;
            while (0 == copyOfThis.integer[ j ] && i > indexOfZero) {
                copyOfThis.integer[ j ] = 9;
                --j;
            }

            --copyOfThis.integer[ j ];
        }
    }

    return copyOfThis;
}

// multiplication operator; HugeInt * HugeInt
HugeInt HugeInt::operator*( const HugeInt &op2 ) const    
{
    HugeInt product;
    size_t Ky = indexOfNonZero(op2) - 1;
    size_t Kx = indexOfNonZero(*this) - 1;

    for (size_t j = digits - 1; j > Ky; --j) {
        int carry = 0;
        size_t p_index = 0;
        for (size_t i = digits - 1; i > Kx; --i) {
            p_index = j + i - (digits - 1);
            product.integer[ p_index ] += carry + integer[ i ] * op2.integer[ j ];
            carry = product.integer[ p_index ] / 10;
            product.integer[ p_index ] = product.integer[ p_index ] % 10;
        }

        product.integer[ p_index - 1 ] += carry;
    }
    return product;
}


// subtraction-short; HugeInt -= HugeInt
HugeInt &HugeInt::operator-=(const HugeInt & op2)    
{
    *this = *this - op2;
    return *this;
}

// prefix increment; ++HugeInt
HugeInt &HugeInt::operator++()    
{
    for (size_t i = digits - 1; ++integer[ i ] > 9; --i) {
        integer[ i ] %= 10;
    }

    return *this;
}

// postfix increment; HugeInt++
HugeInt HugeInt::operator++( int )    
{
    HugeInt copyOfThis = *this;
    for (size_t i = digits - 1; ++integer[ i ] > 9; --i) {
        integer[ i ] %= 10;
    }

    return copyOfThis;
}

// division operator; HugeInt / HugeInt
HugeInt HugeInt::operator/( const HugeInt &op2 ) const    
{
    if (op2 > *this) return 0;
    HugeInt result = 0;
    HugeInt copyOfThis = *this;

    while (copyOfThis > 0) {
        copyOfThis -= op2;
        ++result;
    }
    return result;
}

