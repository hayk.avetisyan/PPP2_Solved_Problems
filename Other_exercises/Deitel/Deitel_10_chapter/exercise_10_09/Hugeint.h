// HugeInt class definition.
#ifndef HUGEINT_H
#define HUGEINT_H

#include <array>
#include <iostream>
#include <string>

class HugeInt
{
   friend std::ostream &operator<<( std::ostream &, const HugeInt & );
   friend std::istream &operator>>( std::istream &,  HugeInt & );
public:
   static const int digits = 30; // maximum digits in a HugeInt
   HugeInt( long = 0); // conversion/default constructor
   HugeInt( const std::string &); // conversion constructor
   HugeInt operator+(const HugeInt &) const; // addition; HugeInt + HugeInt
   HugeInt operator+( int ) const; // addition operator; HugeInt + int           

   // addition operator; 
   // HugeInt + string that represents large integer value
   HugeInt operator+( const std::string & ) const;    

   bool operator>( const HugeInt &op2 ) const; // comparison; HugeInt > HugeInt
   HugeInt operator-( const HugeInt &op2 ) const; // subtraction; HugeInt - HugeInt    
   HugeInt &operator-=( const HugeInt & ); // subtraction-short; HugeInt -= HugeInt
   HugeInt &operator++( ); // prefix increment; ++HugeInt
   HugeInt operator++( int ); // postfix increment; HugeInt++

   HugeInt operator*(const HugeInt &) const; // multiplication; HugeInt * HugeInt
   HugeInt operator/(const HugeInt &) const; // division; HugeInt * HugeInt

private:
   std::array< short, digits > integer;
   size_t indexOfNonZero(const HugeInt &num) const;
   size_t indexOfNonZero() const;
   void assignZero();
}; // end class HugetInt

#endif

