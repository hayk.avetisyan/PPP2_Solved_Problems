//////// SOLUTION of exercise_10_09  ////////////////////////////////////
#include "../../useful_functions_for_Deitel.h"
#include <iostream>
#include "Hugeint.h"

int main()
{
   HugeInt n1( "76543291234" );
   HugeInt n2( "9123566" );
   HugeInt n3( "99999999999999999999999999999" );
   HugeInt n4( "1" );
   HugeInt n5;

   std::cout << "n1 is " << n1 << "\nn2 is " << n2
      << "\nn3 is " << n3 << "\nn4 is " << n4
      << "\nn5 is " << n5 << "\n\n";

   n5 = n1 + n2;
   std::cout << n1 << " + " << n2 << " = " << n5 << "\n\n";

   std::cout << n3 << " + " << n4 << "\n= " << ( n3 + n4 ) << "\n\n";

   n5 = n1 + 9;
   std::cout << n1 << " + " << 9 << " = " << n5 << "\n\n";

   n5 = n2 + "10000";
   std::cout << n2 << " + " << "10000" << " = " << n5 << std::endl;

   n5 = n1 - n2;
   std::cout << n1 << " - " << n2 << " = " << n5 << "\n\n";

   checkGreatness(n2, n1, "n2", "n1");

   n5 = n2 * n1;
   std::cout << n2 << " * " << n1 << " = " << n5 << "\n\n";

   HugeInt nX ("698347769430620444");
   HugeInt nY ("76543291234");
   n5 = nX / nY;
   std::cout << nX << " / " << nY << " = " << n5 << "\n\n";

   return 0;
} // end main


