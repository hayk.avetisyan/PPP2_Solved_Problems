#ifndef useful_functions_for_Deitel_H
#define useful_functions_for_Deitel_H

#include<iostream>
#include<string>

template <typename T>
void checkEquality(T x, T y, std::string xName, std::string yName)
{
    if (x == y) {
        std::cout << xName << " is equal to " << yName << "\n";
    } else {
        std::cout << xName << " is not equal to " << yName << "\n";
    }
}

template <typename T>
void checkUnequality(T x, T y, std::string xName, std::string yName)
{
    if (x != y) {
        std::cout << xName << " is not equal to " << yName << "\n";
    } else {
        std::cout << xName << " is equal to " << yName << "\n";
    }
}

template <typename T>
void checkGreatness(T x, T y, std::string xName, std::string yName)
{
    if (x > y) {
        std::cout << xName << " is greater from " << yName << "\n";
    } else {
        std::cout << xName << " is not greater from " << yName << "\n";
    }
}
template <typename T>
void reportPrint(const std::string & xName, T x, int i = -1)
{
    if (-1 == i) {
        std::cout << xName << " = " << x << "\n";
        return;
    } 

    std::cout << xName << i << "] = " << x << "\n";
}

#endif

