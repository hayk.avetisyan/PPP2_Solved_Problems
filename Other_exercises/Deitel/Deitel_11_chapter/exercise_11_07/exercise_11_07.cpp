//////// SOLUTION of exercise_11_07  ////////////////////////////////////
#include "../../useful_functions_for_Deitel.h"
#include <iostream>
#include <iomanip>
#include <string>

#include "BasePlusCommissionEmployee.h" // class definition

int main()
{
   // instantiate BasePlusCommissionEmployee object
   BasePlusCommissionEmployee 
      employee( "Bob", "Lewis", "333-33-3333", 5000, .04, 300 );
   
   // set floating-point output formatting
   std::cout << std::fixed << std::setprecision( 2 );

   // get commission employee data
   std::cout << "Employee information obtained by get functions: \n" 
      << "\nFirst name is " << employee.getFirstName() 
      << "\nLast name is " << employee.getLastName() 
      << "\nSocial security number is " 
      << employee.getSocialSecurityNumber() 
      << "\nGross sales is " << employee.getGrossSales() 
      << "\nCommission rate is " << employee.getCommissionRate()
      << "\nBase salary is " << employee.getBaseSalary() << std::endl;

   employee.setBaseSalary( 1000 ); // set base salary

   std::cout << "\nUpdated employee information output by print function: \n" 
      << std::endl;
   employee.print(); // display the new employee information
   
   // display the employee's earnings
   std::cout << "\n\nEmployee's earnings: $" << employee.earnings() << std::endl;

    return 0;
}    


