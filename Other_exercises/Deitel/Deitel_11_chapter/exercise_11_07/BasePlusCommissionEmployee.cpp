// Class BasePlusCommissionEmployee member-function definitions.
#include <iostream>
#include <stdexcept>
#include "BasePlusCommissionEmployee.h"

class CommissionEmployee;

// constructor
BasePlusCommissionEmployee::BasePlusCommissionEmployee( 
   const std::string &first, const std::string &last, const std::string &ssn, 
   double sales, double rate, double salary ) 
   // explicitly call base-class constructor
   : comEmpInBase(CommissionEmployee( first, last, ssn, sales, rate ))
{
   setBaseSalary( salary ); // validate and store base salary
} // end BasePlusCommissionEmployee constructor

// set base salary
void BasePlusCommissionEmployee::setBaseSalary( double salary )
{
   if ( salary >= 0.0 )                                        
      baseSalary = salary;                                     
   else                                                        
      throw std::invalid_argument( "Salary must be >= 0.0" );       
} // end function setBaseSalary

// return base salary
double BasePlusCommissionEmployee::getBaseSalary() const
{
   return baseSalary;
} // end function getBaseSalary

// calculate earnings
double BasePlusCommissionEmployee::earnings() const
{
   return getBaseSalary() + comEmpInBase.earnings();
} // end function earnings

// print BasePlusCommissionEmployee object
void BasePlusCommissionEmployee::print() const
{
   std::cout << "base-salaried ";

   // invoke CommissionEmployee's print function
   comEmpInBase.print();                 
   
   std::cout << "\nbase salary: " << getBaseSalary();
} // end function print


////////////////// added functions from CommissionEmployee ////////

// set first name
void BasePlusCommissionEmployee::setFirstName( const std::string &first )
{
   comEmpInBase.firstName = first; // should validate
} // end function setFirstName

// return first name
std::string BasePlusCommissionEmployee::getFirstName() const
{
   return comEmpInBase.firstName;
} // end function getFirstName

// set last name
void BasePlusCommissionEmployee::setLastName( const std::string &last )
{
   comEmpInBase.lastName = last; // should validate
} // end function setLastName

// return last name
std::string BasePlusCommissionEmployee::getLastName() const
{
   return comEmpInBase.lastName;
} // end function getLastName

// set social security number
void BasePlusCommissionEmployee::setSocialSecurityNumber( const std::string &ssn )
{
   comEmpInBase.socialSecurityNumber = ssn; // should validate
} // end function setSocialSecurityNumber

// return social security number
std::string BasePlusCommissionEmployee::getSocialSecurityNumber() const
{
   return comEmpInBase.socialSecurityNumber;
} // end function getSocialSecurityNumber

// set gross sales amount
void BasePlusCommissionEmployee::setGrossSales( double sales )
{
    comEmpInBase.setGrossSales(sales);
} // end function setGrossSales

// return gross sales amount
double BasePlusCommissionEmployee::getGrossSales() const
{
   return comEmpInBase.grossSales;
} // end function getGrossSales

// set commission rate
void BasePlusCommissionEmployee::setCommissionRate( double rate )
{
    comEmpInBase.setCommissionRate(rate);
} // end function setCommissionRate

// return commission rate
double BasePlusCommissionEmployee::getCommissionRate() const
{
   return comEmpInBase.commissionRate;
} // end function getCommissionRate

