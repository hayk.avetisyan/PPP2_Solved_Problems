#ifndef useful_functions_H
#define useful_functions_H

#include<iostream>
#include<string>
#include<stdexcept>

template <class T>
T *
endPointer(T array[], int size)
{
    if (size < 0) {
        throw std::out_of_range("Error at endPointer-func: size must be more or equal than 0\n");
    }

    return ( array + size );
}

template <typename T>
inline T maximum(T arg1, T arg2) // returns maximum of arg1 and arg2
{
    return (arg1 > arg2 ? arg1 : arg2);
}

#endif

