//////// Tree structure simulation  ////////////////////////////////////
#include "../useful_functions.h"
#include <iostream>
#include <string>
#include <iterator>
#include "Electrical_Cell.hpp"
#include "Inverter.hpp"


void printCellInfo_viaObject(const Electrical_Cell& sourceCell)
{
    sourceCell.printInfo();
    std::cout << "\n";
}

void printCellInfo_viaPointer(Electrical_Cell* pTrsourceCell)
{
    pTrsourceCell->printInfo();
    std::cout << "\n";
}

int main()
{
    // Arbitrary Electrical cells
    /////////////////////////////////////////////////////////////////////////////////////////
    
    std::string mux_InArray4[] = { "in1", "in2", "in3", "in4" };
    std::vector<std::string> inputsOfMux4(mux_InArray4, endPointer(mux_InArray4, 4)); 

    std::vector<std::string> outputsOfMux;
    outputsOfMux.push_back("out"); // outputs

    Electrical_Cell mux_4("MULTIPLEXER", "mux_4", inputsOfMux4, outputsOfMux);        // mux_4
    Electrical_Cell * pTr_mux_4 = new Electrical_Cell("MULTIPLEXER", "mux_4", inputsOfMux4, outputsOfMux);  // mux_4

    mux_4.printSymbol(10);
    
    std::vector<std::string> inputsOfDeMux;
    inputsOfDeMux.push_back("in");

    std::string demux_outArray4[] = { "out1", "out2", "out3", "out4" };
    std::vector<std::string> outputsOfDeMux4(demux_outArray4, endPointer(demux_outArray4, 4)); 

    Electrical_Cell demux_4("DEMULTIPLEXER", "demux_4", inputsOfDeMux, outputsOfDeMux4);  // demux_4
    Electrical_Cell * pTr_demux_4 = new Electrical_Cell("DEMULTIPLEXER", "demux_4", inputsOfDeMux, outputsOfDeMux4);  // demux_4

    // Inverters
    ////////////////////////////////////////////////////////////////////////////////
    Inverter inv_2;
    Inverter * pTr_inv_2 = new Inverter();


    Inverter inv_180x("inv180x");
    Inverter * pTr_inv_180x = new Inverter("inv180x");

    // Pointers
    Electrical_Cell * pBase_inv_180x = &inv_180x;
    /*
    std::cout << "pBase_inv_180x->printSymbol():\n";
    pBase_inv_180x->printSymbol();
    */

    std::cout << "\n";
    Inverter * pinv_180x = &inv_180x;
    /*
    std::cout << "pinv_180x->printSymbol():\n";
    pinv_180x->printSymbol();
    */
    std::cout << "\n\n";

    // deleting pointers
    Electrical_Cell * pBase_inv_200x_new = new Inverter("inv_200x");
    delete pBase_inv_200x_new;
    
    std::vector<Electrical_Cell*> vecOfCellPtrs;

    vecOfCellPtrs.push_back(pTr_demux_4);
    vecOfCellPtrs.push_back(pTr_inv_180x);

    std::vector<Electrical_Cell*>::iterator it = vecOfCellPtrs.begin();
    while( it != vecOfCellPtrs.end() ) {

        Inverter* pTr_Inverter = dynamic_cast< Inverter* >(*it);

        if (NULL != pTr_Inverter) {
            std::cout << "ATTENTION: Detected Inverter-type-pointer\n\n";
        }

        // Inverter* pTr_Inverter_static_cast = static_cast< Inverter* >(*it);       ERROR
        // Inverter Inverter_static_cast = static_cast< Inverter >( *(*it) );        ERROR
        
        printCellInfo_viaPointer(*it);
        ++it;
    }

    return 0; /// program ended successfully
}/// end function main


