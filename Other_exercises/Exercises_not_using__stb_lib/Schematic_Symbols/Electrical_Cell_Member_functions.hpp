// Electrical_Cell class's member-function definitions. 
#include <iomanip> 
#include <iostream> 

// constructor
Electrical_Cell::Electrical_Cell(std::string typeOfCell, std::string nameOfCell, 
        std::vector<std::string> inputNames, std::vector<std::string> outputNames) 
{
    setTypeOfCell(typeOfCell);  
    setNameOfCell(nameOfCell); 
    setInputNames(inputNames);  
    setOutputNames(outputNames);  
} 

// constructor
Electrical_Cell::Electrical_Cell(std::string typeOfCell, std::string nameOfCell) 
{
    setTypeOfCell(typeOfCell);  
    setNameOfCell(nameOfCell);  
    inputNames_.push_back("in");
    outputNames_.push_back("out");
} 

// default constructor
Electrical_Cell::Electrical_Cell() 
{
    setTypeOfCell("INVERTER"); 
    setNameOfCell("inv_default"); 
    inputNames_.push_back("in");
    outputNames_.push_back("out");
} 

Electrical_Cell::~Electrical_Cell() 
{
//    std::cout << "Electrical_Cell's destructor calling for " << getNameOfCell() << "\n";
} 

////////////////////////// SET and GET functions for typeOfCell_ ///////
void // setting typeOfCell_ 
Electrical_Cell::setTypeOfCell(std::string typeOfCell)
{
    typeOfCell_ = typeOfCell; 
} 

std::string // returning typeOfCell_'s value 
Electrical_Cell::getTypeOfCell() const
{
    return typeOfCell_; // return the typeOfCell_ 
} 

////////////////////////// SET and GET functions for nameOfCell_ ///////
void // setting nameOfCell_ 
Electrical_Cell::setNameOfCell(std::string nameOfCell)
{
    nameOfCell_ = nameOfCell; 
} 

std::string // returning nameOfCell_'s value 
Electrical_Cell::getNameOfCell() const
{
    return nameOfCell_; // return the nameOfCell_ 
} 

////////////////////////// SET and GET functions for inputNames_ ///////
void // setting inputNames_ 
Electrical_Cell::setInputNames(std::vector<std::string> inputNames)
{
    inputNames_ = inputNames; 
} 

std::vector<std::string> // returning inputNames_'s value 
Electrical_Cell::getInputNames() const
{
    return inputNames_; // return the inputNames_ 
} 

////////////////////////// SET and GET functions for outputNames_ ///////
void // setting outputNames_ 
Electrical_Cell::setOutputNames(std::vector<std::string> outputNames)
{
    outputNames_ = outputNames; 
} 

std::vector<std::string> // returning outputNames_'s value 
Electrical_Cell::getOutputNames() const
{
    return outputNames_; // return the outputNames_ 
} 

int // width of symbol, non-including input-out ports
Electrical_Cell::widthOfSymbol() const
{
    return maximum(typeOfCell_.length(), nameOfCell_.length()) + 4;
}

int // height of symbol non-including the top and bottom bound-lines
Electrical_Cell::heightOfSymbol() const
{
    return 2 * maximum(inputNames_.size(), outputNames_.size()) + 1;
}

int // the longest input-port-name
Electrical_Cell::longestOfInputNameS() const
{
    int longestInputName = inputNames_[0].size();
    for (int i = 1; i < inputNames_.size(); ++i) {
        if ( longestInputName < inputNames_[i].length() ) {
            longestInputName = inputNames_[i].length();
        }
    }

    return longestInputName;
}

int // the longest output-port-name
Electrical_Cell::longestOfOutputNameS() const
{
    int longestOutputName = outputNames_[0].size();
    for (int i = 1; i < outputNames_.size(); ++i) {
        if ( longestOutputName < outputNames_[i].length() ) {
            longestOutputName = outputNames_[i].length();
        }
    }

    return longestOutputName;
}

void // printing of top or bottom bound-line of symbol
Electrical_Cell::printTopOrBottomLine() const
{
    int upperLineSizeOfSymbol = widthOfSymbol() - 2;
    for (int i = 0; i < upperLineSizeOfSymbol; ++i) {
        std::cout << "_";
    }
}

void // printing of input/output port electrod-wire
Electrical_Cell::printPortWire(int length) const
{
    for (int wireCounter = 0; wireCounter < length; ++wireCounter) {
        std::cout << "_";
    }
}

void // print symbol of cell
Electrical_Cell::printSymbol(int abscissa) const
{
    int longestInputName = longestOfInputNameS(); // input-name with maximum length
    int longestOutputName = longestOfOutputNameS(); // output-name with maximum length

    // shifting of cursor with abscissa + 2 + longestInputName size
    std::cout << std::setw(abscissa + 2 + longestInputName); 
    printTopOrBottomLine(); // print upper bound-line
    std::cout << std::endl;

    int width = widthOfSymbol();
    int wallSize = heightOfSymbol() - 1; // count of '|' symbols forming the wall of symbol

    // if input-ports are more or equal than output-ports
    if ( inputNames_.size() >= outputNames_.size() ) {
        bool outputWireIsPrinted = false;
        int shiftFromBottomOfOutputs = ( wallSize - 2 * outputNames_.size() ) / 2;

        // copy outputNames_ vector for manipulating
        std::vector<std::string> outputNames_Copy = outputNames_;

        for (int i = 0; i < wallSize; ++i) {
            if (0 == i % 2) {
                std::cout << std::setw( abscissa + 1 );
                printPortWire( longestInputName ); 
                if (0 != i) {
                    std::cout << "|" << std::setw( width - 1 ) << "|";
                } else { 
                    // first row contain the symbol's type-name
                    std::cout << "|" << " " << typeOfCell_ << std::setw( width-2-typeOfCell_.length() ) << "|";
                }
            } else {
                std::cout << std::setw( abscissa + longestInputName ) 
                          << inputNames_[ i / 2 ] << "|" << std::setw( width - 1 ) << "|";
            }

            if ( i >= shiftFromBottomOfOutputs && !outputNames_Copy.empty() ) {
                if (outputWireIsPrinted) {
                    std::cout << outputNames_Copy.front();
                    outputNames_Copy.erase( outputNames_Copy.begin() );
                    outputWireIsPrinted = false;
                } else {
                    printPortWire( longestOutputName );
                    outputWireIsPrinted = true;
                }
            }

            std::cout << std::endl; // new line
        }
    } else { // if output-ports are more than input-ports
        bool inputWireIsPrinted = false;
        int shiftFromBottomOfInputs = ( wallSize - 2 * inputNames_.size() ) / 2;

        // copy inputNames_ vector for manipulating
        std::vector<std::string> inputNames_Copy = inputNames_; 

        for (int i = 0; i < wallSize; ++i) {
            if ( i >= shiftFromBottomOfInputs && !inputNames_Copy.empty() ) {
                if (inputWireIsPrinted) {
                    // shift the cursor and print first element of inputNames_Copy vector
                    std::cout << std::setw( abscissa + longestInputName ) << inputNames_Copy.front();

                    // remove first element of inputNames_Copy vector
                    inputNames_Copy.erase( inputNames_Copy.begin() );
                    inputWireIsPrinted = false; // there was printed inputname, not input wireport
                } else {
                    std::cout << std::setw( abscissa + 1 );
                    printPortWire( longestInputName ); 
                    inputWireIsPrinted = true; // there was printed input wireport
                }
            } else {
                std::cout << std::setw( abscissa + longestInputName + 1 );
            }

            if (0 != i) {
                std::cout << "|" << std::setw( width - 1 ) << "|";
            } else {
                // first row contain the symbol's type-name
                std::cout << "|" << " " << typeOfCell_ << std::setw( width-2-typeOfCell_.length() ) << "|";
            }

            if (0 == i % 2) {
                printPortWire( longestOutputName ); // print next output-electrode
            } else {
                std::cout << outputNames_[ i / 2 ]; // print next output-port name
            }

            std::cout << std::endl; // new line
        }
    }

    std::cout << std::setw(abscissa + 1 + longestInputName) << "|";
    printTopOrBottomLine(); 
    std::cout << "|\n";
}

void
Electrical_Cell::printInfo() const
{
    std::cout << nameOfCell_ << "'s information:\n";
    std::cout << "      1) type : " << typeOfCell_ << "\n";
    std::cout << "      -----------------\n";
    std::cout << "      2) number of inputs:  " << inputNames_.size() << "\n";
    std::cout << "      -----------------\n";
    std::cout << "      3) number of outputs: " << outputNames_.size() << "\n";
    std::cout << "      -----------------\n";
}


