#ifndef Inverter_HPP
#define Inverter_HPP
// Inverter class's definition. 
#include <string> // standard string class
#include "Standard_Digital_Cell.hpp"

class Inverter : public Standard_Digital_Cell
{
public:
    explicit Inverter(std::string nameOfCell);
    Inverter();
    virtual ~Inverter();

    void printPortWire(int length) const; // 
    virtual void printSymbol(int abscissa = 2) const; // print symbol with abscissa offset from left

    static const int ATTRIBUTE_COUNT = Standard_Digital_Cell::ATTRIBUTE_COUNT + 1;
    virtual void printInfo() const;

protected:
    int delay_typical;
}; // end of the Inverter class declaration 


Inverter::Inverter(std::string nameOfCell)
    :delay_typical(10)
{
    setTypeOfCell("inverter");
    setNameOfCell(nameOfCell);
} 

Inverter::Inverter()
    :delay_typical(10)
{
    setTypeOfCell("inverter");
} 

Inverter::~Inverter()
{
//    std::cout << "Inverter's destructor calling for " << getNameOfCell() << "\n";
} 

void // printing of input/output port electrod-wire
Inverter::printPortWire(int length) const
{
    for (int wireCounter = 0; wireCounter < length; ++wireCounter) {
        std::cout << "-";
    }
}


void // print symbol of cell
Inverter::printSymbol(int abscissa) const
{
    int longestInputName = longestOfInputNameS(); // input-name with maximum length
    int longestOutputName = longestOfOutputNameS(); // output-name with maximum length

    // shifting of cursor with abscissa + 2 + longestInputName size
    std::cout << std::setw(abscissa + 2 + longestInputName); 
    printTopOrBottomLine(); // print upper bound-line
    std::cout << std::endl;

    int width = widthOfSymbol();
    int wallSize = heightOfSymbol() - 1; // count of '|' symbols forming the wall of symbol

    // if input-ports are more or equal than output-ports
        bool outputWireIsPrinted = false;
        int shiftFromBottomOfOutputs = ( wallSize - 2 * getOutputNames().size() ) / 2;

        // copy outputNames_ vector for manipulating
        std::vector<std::string> outputNames_Copy = getOutputNames();


        std::cout << std::setw( abscissa + 3 );
        std::cout << "|" << " " << getTypeOfCell() << std::setw( width-1-getTypeOfCell().length() ) << "|\n";

        std::cout << std::setw( abscissa + 1 );
        printPortWire( longestInputName ); 
        std::cout << "|" << " " << getNameOfCell() << std::setw( width-2-getNameOfCell().length() ) << "|";
        std::cout << 'o'; 
        printPortWire( longestInputName ); 
        std::cout << "\n";

        std::cout << std::setw( abscissa + 3 );
        std::cout << "|";
        printTopOrBottomLine(); 
        std::cout << "|\n";
}


void
Inverter::printInfo() const
{
    Standard_Digital_Cell::printInfo();
    std::cout << "      " << ATTRIBUTE_COUNT << ") delay_typical: " << delay_typical << "ps\n";
    std::cout << "      -----------------\n";
}


#endif 
