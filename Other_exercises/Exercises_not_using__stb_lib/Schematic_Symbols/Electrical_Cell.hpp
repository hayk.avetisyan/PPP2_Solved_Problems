#ifndef Electrical_Cell_HPP
#define Electrical_Cell_HPP
// Electrical_Cell class's definition.
#include <string> // standard string class
#include <vector> // standard vector class
#include "../useful_functions.h"


class Electrical_Cell
{
public:
    // constructor
    Electrical_Cell(std::string typeOfCell, std::string nameOfCell, 
                    std::vector<std::string> inputNames, std::vector<std::string> outputNames); 

    Electrical_Cell(std::string typeOfCell, std::string nameOfCell); 
    Electrical_Cell(); 

    virtual ~Electrical_Cell();

    void setTypeOfCell(std::string typeOfCell); 
    std::string getTypeOfCell() const; 

    void setNameOfCell(std::string nameOfCell); 
    std::string getNameOfCell() const;

    void setInputNames(std::vector<std::string> inputNames); 
    std::vector<std::string> getInputNames() const; 

    void setOutputNames(std::vector<std::string> outputNames);
    std::vector<std::string> getOutputNames() const; 

    int widthOfSymbol() const; // 
    int heightOfSymbol() const; // 
    int longestOfInputNameS() const; // 
    int longestOfOutputNameS() const; // 
    void printTopOrBottomLine() const; //  
    void printPortWire(int length) const; // 
    virtual void printSymbol(int abscissa = 2) const; // print symbol with abscissa offset from left

    static const int ATTRIBUTE_COUNT = 3;
    virtual void printInfo() const;

private:
    std::string typeOfCell_; 
    std::string nameOfCell_; 
    std::vector<std::string> inputNames_; 
    std::vector<std::string> outputNames_; 

    Electrical_Cell(const Electrical_Cell& srcCell);
    Electrical_Cell& operator=(const Electrical_Cell& srcCell);
}; // end of the Electrical_Cell class declaration 

#include "Electrical_Cell_Member_functions.hpp"
#endif 
    
