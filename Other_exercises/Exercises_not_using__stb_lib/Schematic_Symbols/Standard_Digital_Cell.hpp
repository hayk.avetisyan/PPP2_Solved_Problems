#ifndef Standard_Digital_Cell_HPP
#define Standard_Digital_Cell_HPP
// Inverter class's definition. 
#include <string> // standard string class
#include "Electrical_Cell.hpp"

class Standard_Digital_Cell : public virtual Electrical_Cell
{
public:
    Standard_Digital_Cell();
    Standard_Digital_Cell(std::string typeOfCell, std::string nameOfCell);
    virtual ~Standard_Digital_Cell()
    { }

    static const int ATTRIBUTE_COUNT = Electrical_Cell::ATTRIBUTE_COUNT + 1;
    virtual void printInfo() const;

protected:
    double freq_max;
};

Standard_Digital_Cell::Standard_Digital_Cell()
    :Electrical_Cell(), freq_max(2.5)
{ }

Standard_Digital_Cell::Standard_Digital_Cell(std::string typeOfCell, std::string nameOfCell)
    :Electrical_Cell(typeOfCell, nameOfCell), freq_max(2.5)
{ }


void
Standard_Digital_Cell::printInfo() const
{
    Electrical_Cell::printInfo();
    std::cout << "      " << ATTRIBUTE_COUNT << ") freq_max: " << freq_max << "GHz\n";
    std::cout << "      -----------------\n";
}


#endif 
