#ifndef CGraph_HPP
#define CGraph_HPP
// CGraph class's definition. 
#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <queue>
#include <stack>
#include <utility>
using namespace std;

struct CEdge {
    char vertex1;
    char vertex2;
    int weight;
    CEdge(char v1, char v2, int w) :vertex1( v1 ), vertex2( v2 ), weight( w )
    { }
};

struct CGraph {
    vector<char> vertecieS;
    vector<CEdge> edgeS;

    vector< pair<char,CEdge> > adjacent(char u) {
        vector< pair<char,CEdge> > adjVector;
        for (CEdge e : edgeS) {
            if (e.vertex1 == u) {
                adjVector.push_back( make_pair(e.vertex2, e) );
            } else if (e.vertex2 == u) {
                adjVector.push_back( make_pair(e.vertex1, e) );
            }
        }

        return adjVector;
    }

    
};
       





 // end of the CGraph class declaration 
#endif 

