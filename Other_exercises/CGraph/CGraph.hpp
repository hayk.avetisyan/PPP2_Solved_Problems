#ifndef CGraph_HPP
#define CGraph_HPP
// CGraph class's definition. 
#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <queue>
#include <stack>
#include <utility>
using namespace std;

template <class T>
class Vertex {
public:
    Vertex() 
        :number_(0), name_(""), data_( T() ) 
    { }

    Vertex(const int& sourceInt) 
        :number_( sourceInt ), name_(""), data_( T() ) 
    { }

    Vertex(const int& sourceInt, const string& sourceName) 
        :number_( sourceInt ), name_( sourceName ), data_( T() ) 
    { }

    int get_number_() const
    {
        return number_;
    }

    const string& get_name_() const
    {
        return name_;
    }

    void print() const;

private:
    int number_;
    string name_;
    T data_;
};

template <class T>
class Edge {
public:
    Edge() 
        :pairOfVertecies( T(), T() )
    { }

    Edge( const T& sourceInt1, const T& sourceInt2 ) 
        :pairOfVertecies( sourceInt1, sourceInt2 )
    { }

    void printEdge() const
    {
        cout << pairOfVertecies.first << " --- " 
             << pairOfVertecies.second << "\n";
    }

    friend ostream& 
    operator<<(ostream& os, const Edge<T>& sourceEdge)
    {
        os << sourceEdge.pairOfVertecies.first << " --- " 
           << sourceEdge.pairOfVertecies.second << "\n";
        return os;
    }

    T first() const
    {
        return pairOfVertecies.first;
    }

    T second() const
    {
        return pairOfVertecies.second;
    }

private:
    pair< T,T > pairOfVertecies;
};

template <class T>
class CGraph
{
public:
    CGraph() { }
    
    CGraph(const string& name, const vector< Edge<T> >& sourceEdges) 
        :nameOfThis_( name ), edgeS( sourceEdges )
    {
        create_Set_Vertecies();
        create_AdjacencyListS();
    }

    void print_Edges()
    {
        cout << "\nEdges of \"" << name() << "\" graph:\n";
        for ( typename vector< Edge<T> >::iterator iter = edgeS.begin(); 
              iter != edgeS.end(); ++iter )
        {
            cout << *iter;
        }
        cout << "\n";
    }

    void create_Set_Vertecies()
    {
        for ( typename vector< Edge<T> >::const_iterator iter = edgeS.begin(); 
              iter != edgeS.end(); ++iter )
        {
            Edge<T> tempEdge = *iter;
            vertecieS.insert( tempEdge.first() );
            vertecieS.insert( tempEdge.second() );
        }
    }

    void print_Set_Vertecies()
    {
        cout << "\nVertecies of \"" << name() << "\" graph:\n";
        for ( typename set<T, less<T> >::iterator iter = vertecieS.begin(); 
              iter != vertecieS.end(); ++iter )
        {
            cout << *iter << " ";
        }
        cout << "\n";
    }

    void create_AdjacencyListS()
    {
        for (typename vector< Edge<T> >::const_iterator iter = edgeS.begin(); 
              iter != edgeS.end(); ++iter )
        {
            const Edge<T> tempEdge = *iter;
            T first = tempEdge.first();
            T second = tempEdge.second();
            AdjListS[ first ].push_back( second );
            AdjListS[ second ].push_back( first );
        }

        for (typename set<T, less<T> >::const_iterator iterSet = vertecieS.begin(); 
            iterSet != vertecieS.end(); ++iterSet) 
        {
            T vertex = *iterSet;
            AdjListS[ vertex ].push_front( vertex );
        }
    }

    void print_AdjacencyListS()
    {
        cout << "\nAdjacencyListS of \"" << name() << "\" graph:\n";
        for (typename set<T, less<T> >::const_iterator iterSet = vertecieS.begin();
            iterSet != vertecieS.end(); ++iterSet) 
        {
            T vertex = *iterSet;
            for (typename list<T>::const_iterator iterList = AdjListS[ vertex ].begin();
                iterList != AdjListS[ vertex ].end(); ++iterList)
            {
                cout << *iterList << " --> " ;
            }

            cout << "NULL\n";
        }
        cout << "\n";
    }


    const string name() const
    {
        return nameOfThis_;
    }
    
    const int edgeCount() const
    {
        return edgeS.size();
    }

    const int vertexCount() const
    {
        return vertecieS.size();
    }


    vector< Edge<T> > get_edgeS() const
    {
        return edgeS;
    }

    set<T, less<T> > get_vertecieS() const
    {
        return vertecieS;
    }

    map< T, list<T> > get_AdjListS() const
    {
        return AdjListS;
    }

    void BFS_Search(const T& root, const T& goal)
    {
        set<T, less<T> > S;
        S.insert( root );

        queue<T> Q;
        Q.push( root );

        while (!Q.empty() ) {
            T curr = Q.front();
            Q.pop();

            cout << "curr = " << curr << "\n";
            if (goal == curr) {
                cout << "Found " << curr << " vertex\n";
                break;
            }

            list<T> adjList = AdjListS[ curr ];
            for (typename list<T>::const_iterator iterList = adjList.begin();
                iterList != adjList.end(); ++iterList )
            {
                int size1ofS = S.size();
                S.insert( *iterList );
                int size2ofS = S.size();
                if (size1ofS != size2ofS) {
                    Q.push( *iterList );
                }
            }

        }
    }

    void DFS_Search(const T& root, const T& goal)
    {
        set<T, less<T> > S;
        S.insert( root );

        stack<T> Q;
        Q.push( root );

        while (!Q.empty() ) {
            T curr = Q.top();
            Q.pop();

            cout << "curr = " << curr << "\n";
            if (goal == curr) {
                cout << "Found " << curr << " vertex\n";
                break;
            }

            list<T> adjList = AdjListS[ curr ];
            for (typename list<T>::const_iterator iterList = adjList.begin();
                iterList != adjList.end(); ++iterList )
            {
                int size1ofS = S.size();
                S.insert( *iterList );
                int size2ofS = S.size();
                if (size1ofS != size2ofS) {
                    Q.push( *iterList );
                }
            }

        }
    }


    

private:
    vector< Edge<T> > edgeS; 
    set<T, less<T> > vertecieS;
    map< T, list<T> > AdjListS;
    string nameOfThis_;
}; // end of the CGraph class declaration 

#endif 

