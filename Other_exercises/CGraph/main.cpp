//////// Tree structure simulation  ////////////////////////////////////
#include "Weighted_CGraph.hpp"
#include <iterator>
#include <queue>
#include <string>
#include <unordered_map>
#include <cstdint>
#include <climits>
#include <algorithm>

int main()
{
    try {
        cout << "hello\n";

        vector< CEdge > edgeS;
        char v1 = '|';
        char v2 = '|';
        int weight = -999;
        while (cin >> v1 >> v2 >> weight) {
            edgeS.push_back( CEdge(v1, v2, weight) );
        }


        CGraph g;
        char v[] = {'a', 'b', 'c', 'd', 'e', 'f'};
        g.vertecieS = vector<char>(v, v + sizeof(v)/sizeof(v[0]) );

        g.edgeS.push_back( CEdge('a', 'b', 4) );
        g.edgeS.push_back( CEdge('a', 'f', 2) );
        g.edgeS.push_back( CEdge('f', 'b', 3) );
        g.edgeS.push_back( CEdge('c', 'b', 6) );
        g.edgeS.push_back( CEdge('c', 'f', 1) );
        g.edgeS.push_back( CEdge('f', 'e', 4) );
        g.edgeS.push_back( CEdge('d', 'e', 2) );
        g.edgeS.push_back( CEdge('d', 'c', 3) );

 
        // prim's algorithm
        unordered_map<char, char> MST;
        unordered_map<char, char> PARENT;
        unordered_map<char, int> KEY;

        for (auto v : g.vertecieS) {
            PARENT[v] = '\0';
            KEY[v] = INT_MAX;
        }

        KEY['a'] = 0; // Root

        vector<char> Q = g.vertecieS;
        while ( !Q.empty() ) {
            char u = *min_element(Q.begin(), Q.end(), [&](char x, char y){return KEY[x] < KEY[y];});
            vector<char>::iterator iter = remove(Q.begin(), Q.end(), u);
            Q.erase( iter, Q.end() );

            if ('\0' != PARENT[ u ]) {
                MST[ u ] = PARENT[ u ];
            }

            vector< pair<char, CEdge> > adj = g.adjacent(u);
            for (pair<char, CEdge> vertexEdgePair : adj ) {
                if ( find( Q.begin(), Q.end(), vertexEdgePair.first) != Q.end() ) {
                    if ( vertexEdgePair.second.weight < KEY[vertexEdgePair.first] )
                    {
                        KEY[ vertexEdgePair.first ] = vertexEdgePair.second.weight;
                        PARENT[ vertexEdgePair.first ] = u;
                    }
                }
            }


        }

        for (auto e: MST) 
        {
            cout << "{" << e.first << ", " << e.second << "} ";
        }

        cout << "\n";



    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


