//////// Tree structure simulation  ////////////////////////////////////
#include <iostream>
#include <exception>
#include <stdexcept>

template <int N>
int Factorial_func() 
{
    return N * Factorial_func<N-1>();
}

template <>
int Factorial_func<1>()
{
    return 1;
}

template <>
int Factorial_func<0>()
{
    return 1;
}


template <int N>
inline int Fibonacci_func() 
{
    return Fibonacci_func<N-1>() + Fibonacci_func<N-2>();
}

template <>
int Fibonacci_func<1>()
{
    return 1;
}

template <>
int Fibonacci_func<2>()
{
    return 1;
}

template <int N>
struct Factorial {
    static const int value = N * Factorial<N-1>::value;
};

template <>
struct Factorial<1>
{
    static const int value = 1;
};

template <>
struct Factorial<0>
{
    static const int value = 1;
};


class Error_of_Fibonacci { };

template <int N>
struct Fibonacci {

    static const int value = Fibonacci<N-1>::value + Fibonacci<N-2>::value;
};

template <>
struct Fibonacci<2>
{
    static const int value = 1;
};

template <>
struct Fibonacci<1>
{
    static const int value = 1;
};

int main()
{

    std::cout << "Factorial_func<10> = " << Factorial_func<10>() << "\n";
    std::cout << "Fibonacci_func<10> = " << Fibonacci_func<10>() << "\n";

    std::cout << "Factorial<10>::value = " << Factorial<10>::value << "\n";
    std::cout << "Fibonacci<10>::value = " << Fibonacci<10>::value << "\n";

    int intNumber = -999;
    while (std::cin >> intNumber) {

        try {
            if (intNumber < 0) {
                throw Error_of_Fibonacci();
            }

            std::cout << "OK..\n";

        } catch (...) {
            std::cout << "Error: Fibonacci's argument must be greater or equal to zero\n";
            continue;
        } 
    }
    
    return 0; /// program ended successfully
}/// end function main


