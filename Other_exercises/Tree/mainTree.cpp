//////// Tree structure simulation  ////////////////////////////////////
#include "Tree.h"
#include "Tree_Member_Functions.h"

int main()
{
    try {
        Tree<string> treeOfSTL("STL"); // STL tree
        
        cout << "Please insert some STL structure:\n\n";
        string stlNames = "";
        while (cin >> stlNames) {
            treeOfSTL.insertUnderRoot(new TreeNode<string>(stlNames));
        }

        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Sequence containers"));
        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Associative containers"));
        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Container adaptors"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("vector"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("deque"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("list"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("set"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("multiset"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("map"));

        treeOfSTL.printTree();
        cout << endl << endl;

        cout << "The height of STL tree is: " << treeOfSTL.height() << "\n";

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main


