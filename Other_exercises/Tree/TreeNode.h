#ifndef TREENODE_H
#define TREENODE_H

#include "../../std_lib_facilities_haykav.h" /// all include stuff

template <typename TREENODE_TYPE>
class TreeNode {

    template <typename TREE_TYPE> friend class Tree;

private:
    TREENODE_TYPE key;
    vector<TreeNode<TREENODE_TYPE>*> childVec;
    TreeNode<TREENODE_TYPE> * parentNode;

public:
    explicit TreeNode(const TREENODE_TYPE &keyArg); // constructor

    // constructor including tree node's parent param
    TreeNode(const TREENODE_TYPE &keyArg, TreeNode<TREENODE_TYPE> * parentArg);

    ~TreeNode(); // destructor
    TreeNode(const TreeNode<TREENODE_TYPE> & treeToBeCopied); // copy constructor

    // return pointer to parent
    TreeNode<TREENODE_TYPE> * parent() const { return parentNode; }

    // return vector of children
    vector<TreeNode<TREENODE_TYPE>*> children() const { return childVec; }

    // checking  is the tree node is root or not
    inline bool isRoot() const {return (nullptr == parentNode); }

    bool isLeaf() const;  // whether the tree-node is leaf or not

    // insert a tree node as child under this object
    void insertAsChild(TreeNode<TREENODE_TYPE> * ptreeArg); 

    // insert a tree node under exact tree-node containing key 
    void insertUnderKey(const TREENODE_TYPE &keyArg, TreeNode<TREENODE_TYPE> * ptreeArg);

    // return depth of the tree node
    int depth();
};

#endif
