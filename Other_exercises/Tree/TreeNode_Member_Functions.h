#include "TreeNode.h"
/****************************************************
********                                      *******
********              TreeNode's              *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <typename TREENODE_TYPE>
TreeNode<TREENODE_TYPE>::TreeNode(const TREENODE_TYPE &keyArg) // constructor 
    : key( keyArg ), parentNode( nullptr )
{ }

template <typename TREENODE_TYPE>
TreeNode<TREENODE_TYPE>::~TreeNode() // destructor
{
    parentNode = nullptr;
    if ( !childVec.empty() ) {
        for (int i = 0; i < childVec.size(); ++i) {
            delete childVec[i];
        }
    }
}

template <typename TREENODE_TYPE> // constructor including tree node's parent param
TreeNode<TREENODE_TYPE>::TreeNode(const TREENODE_TYPE &keyArg, TreeNode<TREENODE_TYPE> * parentArg)  
    : key( keyArg ), parentNode( parentArg )
{ }


template <typename TREENODE_TYPE> // copy constructor
TreeNode<TREENODE_TYPE>::TreeNode(const TreeNode<TREENODE_TYPE> & treeToBeCopied) 
{
    parentNode = nullptr;
    key = treeToBeCopied.key;

    int childCount = treeToBeCopied.childVec.size();
    for (int i = 0; i < childCount; ++i) {
        TreeNode<TREENODE_TYPE> childToBeCopied = *( treeToBeCopied.childVec[i] );
        TreeNode<TREENODE_TYPE> * ptrToCopiedChild = new TreeNode<TREENODE_TYPE>(childToBeCopied);
        childVec.push_back( ptrToCopiedChild );
        ptrToCopiedChild->parentNode = this;
    }
}

template <typename TREENODE_TYPE> 
bool 
TreeNode<TREENODE_TYPE>::isLeaf() const // checking  is the tree node is external or not
{
    return ( childVec.empty() ? true : false ); 
}

template <typename TREENODE_TYPE> 
void 
TreeNode<TREENODE_TYPE>::insertAsChild(TreeNode<TREENODE_TYPE> * ptreeArg) 
// insert a tree node as child under this object
{
    childVec.push_back(ptreeArg);
    ptreeArg->parentNode = this;
}

template <typename TREENODE_TYPE> 
void 
// checking  is the tree node is external or not
TreeNode<TREENODE_TYPE>::insertUnderKey(const TREENODE_TYPE &keyArg, TreeNode<TREENODE_TYPE> * ptreeArg) 
{
    if (key == keyArg) {
        insertAsChild(ptreeArg);
    }

    if ( !isLeaf() ) {
        for (int i = 0; i < childVec.size(); ++i) {
            childVec[i]->insertUnderKey(keyArg, ptreeArg);
        }
    }
}

template <typename TREENODE_TYPE> 
int 
TreeNode<TREENODE_TYPE>::depth() // return depth of the tree node 
{
    if ( isRoot() ) { return 0; }

    int depth = 0;
    TreeNode<TREENODE_TYPE> * piter = this;
    while ( !piter->isRoot() ) {
        ++depth;
        piter = piter->parentNode;
    }

    return depth;
}

