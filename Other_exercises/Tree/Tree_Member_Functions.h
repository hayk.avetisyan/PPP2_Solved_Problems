#include "Tree.h"
/****************************************************
********                                      *******
********      Tree's DFS_tree_iterator's      *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <typename TREE_TYPE>
Tree<TREE_TYPE>::DFS_tree_iterator::DFS_tree_iterator()
    :curr{nullptr}, rootInIter{nullptr}
{ }

template <typename TREE_TYPE>
Tree<TREE_TYPE>::DFS_tree_iterator::DFS_tree_iterator(const Tree &treeArg)
    :curr{treeArg.root}, rootInIter{treeArg.root}
{ }

template <typename TREE_TYPE>
Tree<TREE_TYPE>::DFS_tree_iterator::~DFS_tree_iterator()
{
    delete curr;
    delete rootInIter;
}

template <typename TREENODE_TYPE> 
bool 
Tree<TREENODE_TYPE>::DFS_tree_iterator::isFirstChild() const // checking  is the tree node is external or not
{
    return ( ( !curr->isRoot() && curr == curr->parentNode->childVec[0] ) ? true : false );
}

template <typename TREE_TYPE>
bool 
Tree<TREE_TYPE>::DFS_tree_iterator::RightChildExist() const
{
    return ( !curr->isRoot() && curr != curr->parentNode->childVec.back() ? true : false ); 
}

template <typename TREE_TYPE>
bool 
Tree<TREE_TYPE>::DFS_tree_iterator::operator==(const DFS_tree_iterator &iterArg) const
{
    return (curr == iterArg.curr && rootInIter == iterArg.rootInIter);
}

template <typename TREE_TYPE>
bool 
Tree<TREE_TYPE>::DFS_tree_iterator::operator!=(const DFS_tree_iterator &iterArg) const
{
    return !(*this == iterArg);
}


template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::DFS_tree_iterator::GoToRightChild()
{
    ++stackOfIndexeS.top(); // increment the index
    int indexOfRightBrother = stackOfIndexeS.top(); 

    // go to right-side child
    curr = curr->parentNode->childVec[ indexOfRightBrother ]; 
}

template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::DFS_tree_iterator::GoUp()
{
    while ( nullptr != curr && !RightChildExist() ) {
        stackOfIndexeS.pop(); // remove from stack the index of current Tree Node
        curr = curr->parentNode; // go to parent Node
    }
    
    if (nullptr == curr) { // if curr is nullptr, all Nodes are passed
        rootInIter = nullptr; 
    } else {
        GoToRightChild(); // got to right side child
    }
}

template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::DFS_tree_iterator::GoToFirstChild()
{
    curr = curr->childVec[0];
    stackOfIndexeS.push(0); // set 0 the index of the current Tree-Node
}

template <typename TREE_TYPE>
typename Tree<TREE_TYPE>::DFS_tree_iterator & 
Tree<TREE_TYPE>::DFS_tree_iterator::operator++() 
{
    if ( isLeaf() ) { // if current Node is Leaf
        if ( RightChildExist() ) { // if current Node has right-side child
            GoToRightChild(); // jump to right-side child
        } else {
            GoUp(); // go up until right-side child exist
        }
    } else {
        GoToFirstChild(); // go to first child of current Node
    }

    return *this;
}
 
template <typename TREE_TYPE>
TREE_TYPE & 
Tree<TREE_TYPE>::DFS_tree_iterator::operator*()
{
    return curr->key;
}
 
template <typename TREE_TYPE>
TreeNode<TREE_TYPE> * 
Tree<TREE_TYPE>::DFS_tree_iterator::getTreeNode()
{
    return curr;
}
 

/****************************************************
********                                      *******
********               Tree's                 *******
********           member-functions           *******
********                                      *******
*****************************************************/
template <typename TREE_TYPE>
Tree<TREE_TYPE>::Tree() // default constructor without param
    : root( nullptr )
{ } 

template <typename TREE_TYPE>
Tree<TREE_TYPE>::Tree(TreeNode<TREE_TYPE> * ptreeArg) // default constructor
{
    if (nullptr == ptreeArg->parentNode)
        root = ptreeArg;
}

template <typename TREE_TYPE>
Tree<TREE_TYPE>::Tree(const TREE_TYPE & keyArg) // default constructor
    :root( new TreeNode<TREE_TYPE>(keyArg) )
{ }

template <typename TREE_TYPE>
Tree<TREE_TYPE>::~Tree()
{
    delete root;
}

template <typename TREE_TYPE>
Tree<TREE_TYPE>::Tree(const Tree<TREE_TYPE> & treeToBeCopied)
{
    TreeNode<TREE_TYPE> rooTreeNodeToBeCopied= *( treeToBeCopied.root );
    root = new TreeNode<TREE_TYPE>( rooTreeNodeToBeCopied );
}

template <typename TREE_TYPE>
typename Tree<TREE_TYPE>::DFS_tree_iterator 
Tree<TREE_TYPE>::begin()
{
    return DFS_tree_iterator(*this);
} 

template <typename TREE_TYPE>
typename Tree<TREE_TYPE>::DFS_tree_iterator 
Tree<TREE_TYPE>::end()
{
    DFS_tree_iterator zeroDFS_iterator;
    return zeroDFS_iterator;
} 

template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::insertUnderRoot(TreeNode<TREE_TYPE> * ptreeArg)
{
    root->childVec.push_back(ptreeArg);
    ptreeArg->parentNode = root;
}

template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::insertUnderKey(const TREE_TYPE &keyArg, TreeNode<TREE_TYPE> * parentOfInserted)
{
    root->insertUnderKey(keyArg, parentOfInserted);
}

template <typename TREE_TYPE>
int 
Tree<TREE_TYPE>::height()
{
    int h = 0;
    int treeNodeDepth = 0;

    for (DFS_tree_iterator it = begin(); it != end(); ++it) {
        if ( !it.getTreeNode()->isLeaf() ) {
            ++treeNodeDepth;
        } else {
            h = maximum(h, treeNodeDepth);
            treeNodeDepth = 0;
        }
    }

    return h;
}

template <typename TREE_TYPE>
void 
Tree<TREE_TYPE>::printTree()
{
    for (DFS_tree_iterator it = begin(); it != end(); ++it) {
        int depth = it.getTreeNode()->depth();
        printSpaces(2 * depth);
        cout << *it << "\n";
    }
}

