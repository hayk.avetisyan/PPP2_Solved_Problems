#ifndef TREE_H
#define TREE_H

#include "TreeNode_Member_Functions.h"
#include <stack>

template <typename TREE_TYPE> 
class Tree {
private:
    TreeNode<TREE_TYPE> * root; // pointer to root
public:

    class DFS_tree_iterator {
    private:
        TreeNode<TREE_TYPE> * curr; // pointer to current TreeNode
        TreeNode<TREE_TYPE> * rootInIter; // pointer to root TreeNode
        stack<int> stackOfIndexeS; // stack of indexes of each hierarchy
    public:
        DFS_tree_iterator(); // default constructor
        DFS_tree_iterator(const Tree &treeArg); // constructor
        ~DFS_tree_iterator(); // destructor

        // whether current tree-node is leaf or not
        inline bool isLeaf() const { return curr->isLeaf(); }  

        // whether current tree-node is leaf or not
        inline bool isFirstChild() const;  

        // check if right side child exist or not
        bool RightChildExist() const; 

        bool operator==(const DFS_tree_iterator &iterArg) const;
        bool operator!=(const DFS_tree_iterator &iterArg) const;

        // jump to the first child of current tree-node 
        void GoToRightChild();

        // jump to the first child of current tree-node 
        void GoUp();

        // jump to the first child of current tree-node 
        void GoToFirstChild();

        DFS_tree_iterator & operator++(); 
        TREE_TYPE & operator*(); // returns key of TreeNode pointed by iterator
        TreeNode<TREE_TYPE> * getTreeNode(); // returns pointer to TreeNode
    };

public:
    Tree(); // default constructor
    Tree(TreeNode<TREE_TYPE> * ptreeArg); // constructor
    explicit Tree(const TREE_TYPE & keyArg); // constructor
    ~Tree(); // destructor
    Tree(const Tree<TREE_TYPE> & treeToBeCopied); // copy constructor
    DFS_tree_iterator begin(); // begin ietrator
    DFS_tree_iterator end(); // end iterator;

    void insertUnderRoot( TreeNode<TREE_TYPE> * ptreeArg );
    void insertUnderKey( const TREE_TYPE &keyArg, TreeNode<TREE_TYPE> * parentOfInserted );

    int height();
    void printTree();
};

#endif
