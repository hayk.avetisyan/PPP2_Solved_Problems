#ifndef STD_LIB_FACILITIES_HAYKAV_H
#define STD_LIB_FACILITIES_HAYKAV_H


#include<iostream>
#include<fstream>
#include<sstream>
#include<cmath>
#include<cstdlib>
#include<string>
#include<list>
#include<vector>
#include<algorithm>
#include<stdexcept>
#include <exception> /// error handling directive

using namespace std;

inline void error(const string& s)
{
    throw runtime_error(s);
}

inline void error(const string& s, const string& s2)
{
    error(s+s2);
}

inline void error(const string& s, int i)
{
    ostringstream os;
    os << s << ": " << i;
    error(os.str());
}

// run-time checked narrowing cast (type conversion):
template<class R, class A> R narrow_cast(const A& a)
{
    R r = R(a);
    if (A(r)!=a) error(string("info loss"));
    return r;
}

inline void newLine(int newLinesCount) 
{
    for (int counter = 0; counter < newLinesCount; ++counter) {
        cout << endl;
    }
}

inline void printSpaces(int spaceCount) 
{
    for (int counter = 0; counter < spaceCount; ++counter) {
        cout << " ";
    }
}

template <typename T>
inline string to_string(const T& object) {
    ostringstream ss;
    ss << object;
    return ss.str();
}
 
template <typename T>
inline T maximum(T arg1, T arg2)
{
    return (arg1 > arg2 ? arg1 : arg2);
}

#endif
