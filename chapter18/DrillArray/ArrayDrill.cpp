//////// Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

int ga[10] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512};

void f(int arr[], int arrSize)
{
    int la[10] = {0};
    for (size_t index = 0; index < arrSize; ++index) {
        la[index] = arr[index];
        cout << "la[" << index << "] = " << la[index] << endl;
    }

    int *p = new int[arrSize];
    for (int counter = 0; counter < arrSize; ++counter) {
        p[counter] = arr[counter];
        cout << "p[" << counter << "] = " << p[counter] << endl;
    }

    delete [] p;
}

int main()
{
    try {
        f(ga, 10);
        int aa[10] = {1, 2*1, 3*2*1, 4*3*2*1, 5*4*3*2*1, 6*5*4*3*2*1, 7*6*5*4*3*2*1,
                      8*7*6*5*4*3*2*1, 9*8*7*6*5*4*3*2*1, 10*9*8*7*6*5*4*3*2*1};

        f(aa, 10);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

