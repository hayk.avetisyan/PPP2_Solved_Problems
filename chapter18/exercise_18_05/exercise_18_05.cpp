//////// SOLUTION of exercise_18_05 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
string cat_dot(const string& s1, const string& s2);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        string name = "Niels";
        string lastName = "Bohr";

        cout << "cat_dot(" << name << ", " << lastName << ") = " 
             << cat_dot(name, lastName) << endl;


    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
string cat_dot(const string& s1, const string& s2)
{
    string newString = s1 + "." + s2;
    return newString;
}

