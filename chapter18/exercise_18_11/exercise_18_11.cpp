//////// SOLUTION of exercise_18_11 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h" /// all include stuff
#include "Link.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class Link_skip {

    Link_skip(int intArg, Link_skip *prevInit = nullptr, Link_skip *succInit = nullptr,
                          Link_skip *belowInit = nullptr, Link_skip *aboveInit = nullptr)
            :prev{prevInit}, succ{succInit}, below{belowInit}, above{aboveInit}
    {
        setKey(intArg);
    }

    int getKey() const { return key; } // getting key
    void setKey(int intArg) { key = intArg; } // setting key
    Link_skip* insert_horizon(Link_skip *linkNew); // insert n before this object
    Link_skip* insert_verical(Link_skip *linkNew); // insert n before this object
    Link_skip* add_horizon(Link_skip *linkPrev); // insert n after this object
    Link_skip* add_vertical(Link_skip *linkPrev); // insert n after this object
    void erase(); // remove this object from list  
    Link_skip* find(int intArg); // find s in list
    const Link_skip* find(int intArg) const; // find intArg in const list 
    Link_skip* advance_horizon(int n); // move n positions in list
    Link_skip* advance_vertical(int n); // move n positions in list
    Link_skip* next() const { return succ; }
    Link_skip* previous() const { return prev; }
    Link_skip* up() const { return above; }
    Link_skip* down() const { return below; }

    Link* add_ordered(Link *linkNew);

private:
    int key;
    Link_skip *prev;
    Link_skip *succ;
    Link_skip *below;
    Link_skip *above;
};

Link_skip *Link_skip::insert_horizon(Link_skip *linkNew_horizon)
{
    if (nullptr == this) return linkNew_horizon;
    if (nullptr == linkNew_horizon) return this;

    linkNew_horizon->succ = this;
    prev = linkNew_horizon;

    return linkNew_horizon;
}

void print_linked_skip_list(Link_skip *pLink_skip)
{
    cout << "{ \n";
    Link_skip * pIterHeight = pLink_skip->up();
    Link_skip * pLeftTop = pLink_skip;
    while (pIterHeight) {
        pLeftTop = pIterHeight;
        pIterHeight = pIterHeight->up();
    }

    cout << pLeftTop->getKey()


    cout << " }\n";
}

int main()
{
    try {
        Link_skip *numbers = new Link_skip{10};
        numbers = numbers->insert_horizon(new Link_skip{9});
        //print_linked_list(numbers);
        newLine(3);
    } catch (exception& e) {
    cerr << "exception: " << e.what() << endl; return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

