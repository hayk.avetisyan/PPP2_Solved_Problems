#include "Link.h"

Link *Link::insert(Link *linkNew)
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;

    linkNew->succ = this;
    if(this->prev) this->prev->succ = linkNew;
    linkNew->prev = this->prev;
    this->prev = linkNew;
    return linkNew;
}

Link *Link::add(Link *linkNew) 
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;
    linkNew->succ = this->succ;
    linkNew->prev = this;
    if (this->succ) this->succ->prev = linkNew;
    this->succ = linkNew;
    return linkNew;
}

void Link::erase()
{
    if (nullptr == this) return;
    if (this->succ) this->succ->prev = this->prev;
    if (this->prev) this->prev->succ = this->succ;
    this->prev = nullptr;
    this->succ = nullptr;
}

Link *Link::find(int intArg) // find s in list
{
    Link *pIterator = this;
    while(pIterator) {
        if (intArg == pIterator->key) return pIterator;
        pIterator = pIterator->succ;
    }
    
    return nullptr;
}

Link *Link::advance(int n) // move n positions in list
{
    if (nullptr == this) return nullptr;

    Link *pIterator = this;
    if (n > 0) {
        while (n--) {
            if (nullptr == pIterator->succ) return nullptr;
            pIterator = pIterator->succ;
        }
    } else if (n < 0) {
        while (n++) {
            if (nullptr == pIterator->prev) return nullptr;
            pIterator = pIterator->prev;
        }
    }

    return this;
}

Link *Link::add_ordered(Link *linkNew)
{
    if (0 == linkNew) return this;
    if (0 == this) {
        linkNew->succ = 0;
        linkNew->prev = 0;
        return linkNew;
    }

    if (linkNew->key <= key) {
        Link *pIterator = insert(linkNew);
        return pIterator;
    }

    Link *pIterator = this;
    while(linkNew->key > pIterator->key) {
        if (pIterator->succ) {
            pIterator = pIterator->succ;
        } else {
            pIterator->add(linkNew);
            return this;
        }
    }

    pIterator = pIterator->prev;
    pIterator->add(linkNew);
    return this;

}

Link *extract(Link **ppLink, int intArg)
{
    Link *tempLink = (*ppLink)->find(intArg);
    if (nullptr == tempLink) return nullptr;
    if (tempLink == *ppLink) *(ppLink) = (*ppLink)->next();

    tempLink->erase();
    return tempLink;
} 
