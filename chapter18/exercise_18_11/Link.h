#ifndef LINK_H
#define LINK_H

class Link {
public:
    Link(int intArg, Link *prevInit = nullptr, Link *succInit = nullptr)
        :prev{prevInit}, succ{succInit} 
    { 
        setKey(intArg);
    }

    int getKey() const { return key; } // getting key
    void setKey(int intArg) { key = intArg; } // setting key
    Link* insert(Link *linkNew); // insert n before this object
    Link* add(Link *linkPrev); // insert n after this object
    void erase(); // remove this object from list  
    Link* find(int intArg); // find s in list
    const Link* find(int intArg) const; // find intArg in const list 
    Link* advance(int n); // move n positions in list
    Link* next() const { return succ; }
    Link* previous() const { return prev; }

    Link* add_ordered(Link *linkNew);
private:
    int key;
    Link *prev;
    Link *succ;
};

#endif
