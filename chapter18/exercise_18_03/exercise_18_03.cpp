//////// SOLUTION of exercise_18_03 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
int strcmp(const char* s1, const char* s2);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        const char *s11 = "Hayk Avetisyan";
        const char *s12 = "Avetisyan";
        cout << "strcmp(" << s11 << ", " << s12 << ") = " << strcmp(s11, s12) << endl;

        const char *s21 = "C++ C Assembler";
        const char *s22 = "Pascal";
        cout << "strcmp(" << s21 << ", " << s22 << ") = " << strcmp(s21, s22) << endl;

        const char *s31 = "Hayk Avetisyan";
        const char *s32 = "Hayk";
        cout << "strcmp(" << s31 << ", " << s32 << ") = " << strcmp(s31, s32) << endl;

        const char *s41 = "Avetisyan";
        const char *s42 = "Avetisyan";
        cout << "strcmp(" << s41 << ", " << s42 << ") = " << strcmp(s41, s42) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
int strcmp(const char* s1, const char* s2)
{
    while (true) {
        if (*s1 > *s2 || ('\0' != *s1 && '\0' == *s2)) {
            return 1;
        } else if (*s1 < *s2 || ('\0' == *s1 && '\0' != *s2)) {
            return -1;
        } else if ('\0' == *s1 && '\0' == *s2) {
            return 0;
        }

        ++s1;
        ++s2;
    }
}


