//////// SOLUTION of exercise_18_04 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        const char * str_C = "Hayk";
        cout << "str_C = " << str_C << "\n";
        cout << "strLength(str_C) = " << strLength(str_C) << "\n";

        char chars[] = {'a', 'b', 'c'};
        cout << "chars = " << chars << "\n";
        cout << "strLength(chars) = " << strLength(chars) << "\n";
        cout << "chars[2] = " << chars[2] << "\n";


    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////


