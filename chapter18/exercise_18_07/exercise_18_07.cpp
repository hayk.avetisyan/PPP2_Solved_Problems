//////// SOLUTION of exercise_18_07 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
char * cat_dot_C_str_arg(const char *s1, const char *s2, const char separator);

////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        const char * name = "Niels";
        const char *  lastName = "Bohr";

        cout << "\n";
        char separator = ',';
        cout << "separator = \'" << separator << "\'\n";
        char * s3 = cat_dot_C_str_arg(name, lastName, separator);
        cout << "cat_dot_C_str_arg(" << name << ", " << lastName << ", \'" << separator 
             << "\') = " << s3 << endl;
        delete [] s3;

        cout << "\n";
        separator = ':';
        cout << "separator = \'" << separator << "\'\n";
        s3 = cat_dot_C_str_arg(name, lastName, separator);
        cout << "cat_dot_C_str_arg(" << name << ", " << lastName << ", \'" << separator 
             << "\') = " << cat_dot_C_str_arg(name, lastName, separator) << endl;
        delete [] s3;

        cout << "\n";
        separator = ' ';
        cout << "separator = \'" << separator << "\'\n";
        s3 = cat_dot_C_str_arg(name, lastName, separator);
        cout << "cat_dot_C_str_arg(" << name << ", " << lastName << ", \'" << separator 
             << "\') = " << cat_dot_C_str_arg(name, lastName, separator) << endl;
        delete [] s3;


    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
char * cat_dot_C_str_arg(const char *s1, const char *s2, const char separator)
{
    int length_s1 = strLength(s1); // length of s1 C_string
    int length_s2 = strLength(s2); // length of s2 C_string
    int length_s3 = length_s1 + length_s2 + 1; // length of s3 C_string, 1 for separator

    char * s3 = new char[length_s3 + 1];

    while ('\0' != *s1) {
        *s3 = *s1;
        ++s3;
        ++s1;
    }

    *s3 = separator;
    ++s3;

    while ('\0' != *s2) {
        *s3 = *s2;
        ++s3;
        ++s2;
    }

    *s3 = '\0';
    s3 -= length_s3;
    return s3;
}

