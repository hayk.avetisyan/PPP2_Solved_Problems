//////// SOLUTION of exercise_18_10 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
bool is_palindrome_array(const char s[]);
void palindrome_Report_array(const char s[]);
void palindrome_Report_array_short(const char s[]);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        // char array part
        cout << "\nArray part:\n";
        const char * name_C_str = "mayam";
        const char * lastName_C_str = "Bohr";
        palindrome_Report_array(name_C_str);
        palindrome_Report_array(lastName_C_str);
        const char * str_C_1 = "koblach";
        palindrome_Report_array(str_C_1);
        const char * str_C_2 = "kobok";
        palindrome_Report_array(str_C_2);
        const char * str_C_3 = "abenadabadaneba";
        palindrome_Report_array(str_C_3);
        palindrome_Report_array_short(str_C_3);

     } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

bool is_palindrome_array(const char s[])
{
    int length = strLength(s);
    int firstIndex = 0;
    int lastIndex = length - 1;
    while (firstIndex < lastIndex) {
        if (s[firstIndex] != s[lastIndex]) return false;
        ++firstIndex;
        --lastIndex;
    }

    return true;
}

bool is_palindrome_array_short(const char s[])
{
    int length = strLength(s);
    if (length > 10) {
        error("Error 1: the string is too long\n");
    }

    int firstIndex = 0;
    int lastIndex = length - 1;
    while (firstIndex < lastIndex) {
        if (s[firstIndex] != s[lastIndex]) return false;
        ++firstIndex;
        --lastIndex;
    }

    return true;
}

void palindrome_Report_array(const char s[])
{
    if (is_palindrome_array(s)) {
        cout << s << " is palindrome " << endl;
    } else {
        cout << s << " is not palindrome " << endl;
    }
}

void palindrome_Report_array_short(const char s[])
{
    if (is_palindrome_array_short(s)) {
        cout << s << " is palindrome " << endl;
    } else {
        cout << s << " is not palindrome " << endl;
    }
}
