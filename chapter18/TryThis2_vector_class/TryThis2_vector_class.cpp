//////// Try This ////////////////////////////////////
//#include "../../std_lib_facilities_haykav.h"
#include<iostream>
#include<algorithm>
#include<stdexcept>
#include<exception> /// error handling directive
#include<list>
#include "vector.h"
using namespace std;

void f(const vector& cv)
{
    double d = cv[1]; // error, but should be fine
    //cv[1] = 2.0; // error (as it should be)
}

int main()
{
    try {
        /// TRY THIS page:646 
        vector v(10);
        double x = v[2]; // fine
        v[3] = x; // error: v[3] is not an lvalue
        f(v);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

