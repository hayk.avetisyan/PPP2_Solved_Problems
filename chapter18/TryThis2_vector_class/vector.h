// class vector //
#ifndef VECTOR_H
#define VECTOR_H

#include<list>
#include<algorithm>
using namespace std;


class vector {
    int sz; // the size
    double* elem; // a pointer to the elements
public:
    vector(int s); // constructor (s is the element count)
    vector(vector&& a); // move constructor
    vector(initializer_list<double> lst); // initializer-list constructor
    vector(const vector&); // copy constructor: define copy
    ~vector() { delete[] elem; } // destructor deallocates memo

    vector& operator=(const vector&) ; // copy assignment
    vector& operator=(vector&&); // move assignment
    // . . .
    double operator[] (int n) const { return elem[n]; } // return element
    double &operator[] (int n) { return elem[n]; } // return element
};

#endif
