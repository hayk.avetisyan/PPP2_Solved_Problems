// vector.cpp //
#include "vector.h"

vector::vector(int s) // constructor (s is the element count)
    :sz{s}, elem{new double[sz]} // uninitialized memory for elements
{
    for (int i = 0; i<sz; ++i) elem[i] = 0.0; // initialize
}

vector::vector(vector&& a)
    :sz{a.sz}, elem{a.elem} // copy a’s elem and sz
{
    a.sz = 0; // make a the empty vector
    a.elem = nullptr;
}

vector::vector(initializer_list<double> lst) // initializer-list constructor
    :sz{lst.size()}, elem{new double[sz]} // uninitialized memory for elements
{
    copy( lst.begin(),lst.end(),elem); // initialize (using std::copy(); §B.5.2)
}

vector:: vector(const vector& arg) // allocate elements, then initialize them by copying
    :sz{arg.sz}, elem{new double[arg.sz]}
{
    copy(arg.elem,arg.elem+sz,elem); // std::copy(); see §B.5.2
}

vector& vector::operator=(const vector& a) // make this vector a copy of a
{
    double* p = new double[a.sz]; // allocate new space
    copy(a.elem,a.elem+a.sz,elem); // copy elements
    delete[] elem; // deallocate old space
    elem = p; // now we can reset elem
    sz = a.sz;
    return *this; // return a self-reference (see §17.10)
}

vector& vector::operator=(vector&& a) // move a to this vector
{
    delete[] elem; // deallocate old space
    elem = a.elem; // copy a’s elem and sz
    sz = a.sz;
    a.elem = nullptr; // make a the empty vector
    a.sz = 0;
    return *this; // return a self-reference (see §17.10)
}

