/// C_str_functions.h
#ifndef C_STR_FUNCTIONS_H
#define C_STR_FUNCTIONS_H

char * strdup(const char* str_C) // copies C_style str in free store
{
    int lengthNonZero = 0;
    while (*str_C != 0) {
        ++lengthNonZero;
        ++str_C;
    }
    str_C -= lengthNonZero;

    char * str_C_copy = new char[lengthNonZero + 1];
    while (*str_C != 0) {
        *str_C_copy = *str_C;
        ++str_C_copy;
        ++str_C;
    }
    *str_C_copy = '\0';
    str_C_copy -= lengthNonZero;

    return str_C_copy;
}

char* findx(const char* s, const char* x) // returns first occurense of x in s
{
    while (*s != *x && *s != '\0') {
        ++s;
    }

    char ch = '\0';
    char *pch = &ch;
    if (*s == '\0') {
        return pch;
    }

    char * found = new char;
    *found = *s;

    while (*s == *x) {
        ++s;
        ++x;
        if ('\0' == *x || '\0' == *s) break;
    }

    if ('\0' == *x) { return found; } 
    else { return pch; } 
}

int strcmp(const char* s1, const char* s2) // compares s1 and s2 lexi-lly
{
    while (true) {
        if (*s1 > *s2 || ('\0' != *s1 && '\0' == *s2)) {
            return 1;
        } else if (*s1 < *s2 || ('\0' == *s1 && '\0' != *s2)) {
            return -1;
        } else if ('\0' == *s1 && '\0' == *s2) {
            return 0;
        }

        ++s1;
        ++s2;
    }
}

int strLength(const char * str_C_style)
{
    int length = 0;
    while ('\0' != *str_C_style) {
        ++str_C_style;
        ++length;
    }

    return length;
}

#endif
