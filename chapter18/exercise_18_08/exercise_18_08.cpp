//////// SOLUTION of exercise_18_08 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
bool is_palindrome_str(const string& s);
void palindrome_Report_str(const string& s);
bool is_palindrome_array(const char s[]);
void palindrome_Report_array(const char s[]);
bool is_palindrome_pointer(const char s[]);
void palindrome_Report_pointer(const char s[]);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        // string part
        string name = "mayam";
        string lastName = "Bohr";
        palindrome_Report_str(name);
        palindrome_Report_str(lastName);
        palindrome_Report_str("koblach");
        palindrome_Report_str("kobok");

        // char array part
        cout << "\nArray part:\n";
        const char * name_C_str = "mayam";
        const char * lastName_C_str = "Bohr";
        palindrome_Report_array(name_C_str);
        palindrome_Report_array(lastName_C_str);
        const char * str_C_1 = "koblach";
        palindrome_Report_array(str_C_1);
        const char * str_C_2 = "kobok";
        palindrome_Report_array(str_C_2);

        // char pointer part
        cout << "\nPointer part:\n";
        palindrome_Report_pointer(name_C_str);
        palindrome_Report_pointer(lastName_C_str);
        palindrome_Report_pointer(str_C_1);
        palindrome_Report_pointer(str_C_2);
    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
bool is_palindrome_str(const string& s)
{
    string s_backward = s;
    int indexOfLast = s.length() - 1;

    /*
    for (int i = indexOfLast; i > -1; --i) {
        s_backward[i] = s[indexOfLast - i];
    }
    */

    size_t i = indexOfLast;
    for (char ch : s) {
        s_backward[i] = ch;
        --i;
    }

    return (s_backward == s ? true : false);
}

void palindrome_Report_str(const string& strArg)
{
    if (is_palindrome_str(strArg)) {
        cout << strArg << " is palindrome " << endl;
    } else {
        cout << strArg << " is not palindrome " << endl;
    }
}

bool is_palindrome_array(const char s[])
{
    int length = strLength(s);
    char * s_backward = new char[length + 1];

    size_t indexOfPreLast = length - 1;
    for (size_t i = 0; i <= indexOfPreLast; ++i) {
        s_backward[ indexOfPreLast - i ] = s[i];
    }

    s_backward[ length ] = '\0';
    for (size_t i = 0; i <= indexOfPreLast; ++i) {
        if (s_backward[ i ] != s[i]) {
            delete [] s_backward;
            return false;
        }
    }

    delete [] s_backward;
    return true;
}

void palindrome_Report_array(const char s[])
{
    if (is_palindrome_array(s)) {
        cout << s << " is palindrome " << endl;
    } else {
        cout << s << " is not palindrome " << endl;
    }
}

bool is_palindrome_pointer(const char s[])
{
    int length = strLength(s);
    char * s_backward = new char[length + 1];

    s_backward += length - 1;
    while ('\0' != *s) {
        *s_backward = *s;
        ++s;
        --s_backward;
    }
    ++s_backward;

    s -= length;
    while ('\0' != *s) {
        if (*s != *s_backward) {
            delete [] s_backward;
            return false;
        }
        ++s;
        ++s_backward;
    }

    delete [] s_backward;
    return true;
}

void palindrome_Report_pointer(const char s[])
{
    if (is_palindrome_array(s)) {
        cout << s << " is palindrome " << endl;
    } else {
        cout << s << " is not palindrome " << endl;
    }
}

