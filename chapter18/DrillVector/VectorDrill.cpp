//////// VectorDrill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

vector<int> gv = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512};

void f(vector<int> argVec)
{
    vector<int> lv;
    lv = argVec;

    for (int intNumber : lv) {
        size_t index = 0;
        cout << "lv[" << index << "] = " << intNumber << endl;
        ++index;
    }

    vector<int> lv2 = argVec;
    for (size_t iter = 0; iter < lv2.size(); ++iter) {
        cout << "lv2[" << iter << "] = " << lv2[iter] << endl;
    }
}

int main()
{
    try {
        f(gv);
        vector<int> vv = {1, 2*1, 3*2*1, 4*3*2*1, 5*4*3*2*1, 6*5*4*3*2*1, 7*6*5*4*3*2*1,
                          8*7*6*5*4*3*2*1, 9*8*7*6*5*4*3*2*1, 10*9*8*7*6*5*4*3*2*1};
        f(vv);

    } catch (exception& e) { cerr << "exception: " << e.what() << endl; return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

