//////// SOLUTION of exercise_18_01 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
char * strdup(const char*);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        const char *str_C = "Hayk";
        cout << "str_C = " << str_C <<  endl;

        char * copy_str_C = strdup(str_C);
        cout << "copy_str_C = " << copy_str_C <<  endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
char * strdup(const char* str_C)
{
    int lengthNonZero = 0;
    while (*str_C != 0) {
        ++lengthNonZero;
        ++str_C;
    }
    str_C -= lengthNonZero;

    char * str_C_copy = new char[lengthNonZero + 1];
    while (*str_C != 0) {
        *str_C_copy = *str_C;
        ++str_C_copy;
        ++str_C;
    }
    *str_C_copy = '\0';
    str_C_copy -= lengthNonZero;

    return str_C_copy;
}

