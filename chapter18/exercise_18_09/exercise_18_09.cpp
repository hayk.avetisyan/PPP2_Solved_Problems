//////// SOLUTION of exercise_18_09 
#include "../../std_lib_facilities_haykav.h"
#include "../C_str_functions.h"
/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
char * cat_dot_C_str_arg(const char *s1, const char *s2, const char separator);
////////////////////////////////////////////////////////////////////////

int globalInt = 7;
int *pglobalInt = &globalInt;

int globalInt2 = 20;
int *pglobalInt2 = &globalInt2;


int main()
{
    try {
        cout << "Address of the globalInt (static storage): " << (int)pglobalInt << endl;
        cout << "Address of the globalInt2 (static storage): " << (int)pglobalInt2 << endl;

        const char * name = "Niels";
        const char *  lastName = "Bohr";
        int lengthOfName = strLength(name);
        int *plengthOfName = &lengthOfName;
        cout << "Address of lengthOfName in main : " << (int)plengthOfName << endl;
        cout << "Address of name in main : " << (int)name << endl;

        int testInt = 10;
        int * ptestInt = &testInt;
        cout << "Address of testInt in main :      " << (int)ptestInt << endl;

        cout << "\n";
        char separator = ',';
        cout << "separator = \'" << separator << "\'\n";
        char * s3 = cat_dot_C_str_arg(name, lastName, separator);
        cout << "Address of the s3 in main : " << (int)s3 << endl; 
        cout << "cat_dot_C_str_arg(" << name << ", " << lastName << ", \'" << separator 
             << "\') = " << s3 << endl;
        cout << "before deleting *s3 = " << *s3 <<  endl;
        delete [] s3;
        cout << "after deleting *s3 = " << *s3 <<  endl;
        cout << "Address of the s3 in main after delete [] s3 : " << (int)s3 << endl; 

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) { cerr << "exception\n"; return 2; }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
char * cat_dot_C_str_arg(const char *s1, const char *s2, const char separator)
{
    int length_s1 = strLength(s1); // length of s1 C_string
    int * plength_s1 = &length_s1;
    cout << "Address of length_s1 in cat_dot_C_str_arg(arg) function (stack storage) : " << (int)plength_s1 <<  endl;
    int length_s2 = strLength(s2); // length of s2 C_string
    int * plength_s2 = &length_s2;
    cout << "Address of length_s2 in cat_dot_C_str_arg(arg) function (stack storage) : " << (int)plength_s2 <<  endl;
    int length_s3 = length_s1 + length_s2 + 1; // length of s3 C_string, 1 for separator

    char * s3 = new char[length_s3 + 1];

    while ('\0' != *s1) {
        *s3 = *s1;
        cout << "Address of s3 in cat_dot_C_str_arg(arg) function (free store - heap) : " << (int)s3 << endl;
        cout << "Address of s1 in cat_dot_C_str_arg(arg) function (free store - heap) : " << (int)s1 << endl;
        ++s3;
        ++s1;
    }

    *s3 = separator;
    cout << "Address of s3 in cat_dot_C_str_arg(arg) function (free store - heap) : " << (int)s3 << endl;
    ++s3;

    while ('\0' != *s2) {
        *s3 = *s2;
        cout << "Address of s3 in cat_dot_C_str_arg(arg) function (free store - heap) : " << (int)s3 << endl;
        ++s3;
        ++s2;
    }

    *s3 = '\0';
    s3 -= length_s3;


    int *ptestInt = new int[10];
    cout << "ptestInt in cat_dot_C_str_arg(arg) function (free store - heap) : " << (int)ptestInt << endl;

    return s3;
}

