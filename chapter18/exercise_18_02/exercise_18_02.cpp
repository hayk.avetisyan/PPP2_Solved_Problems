//////// SOLUTION of exercise_18_02 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS ////////////////////////
char* findx(const char* s, const char* x);
////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        const char *s = "Hayk Avetisyan";
        const char *x = "Avetisyan";
        cout << "*findx(" << s << ", " << x << ") = " << *findx(s, x) << endl;

        const char *s2 = "C++ C Assembler";
        const char *x2 = "Pascal";
        cout << "*findx2(" << s2 << ", " << x2 << ") = " << *findx(s2, x2) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
char* findx(const char* s, const char* x)
{
    while (*s != *x && *s != '\0') {
        ++s;
    }

    char ch = '\0';
    char *pch = &ch;
    if (*s == '\0') {
        return pch;
    }

    char * found = new char;
    *found = *s;
    cout << "*found = " << *found <<  endl;

    while (*s == *x) {
        ++s;
        ++x;
        if ('\0' == *x || '\0' == *s) break;
    }

    if ('\0' == *x) { return found; } 
    else { return pch; } 
}


