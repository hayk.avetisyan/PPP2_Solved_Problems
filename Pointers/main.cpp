//////// Tree structure simulation  ////////////////////////////////////
#include "../std_lib_facilities_haykav.h"
#include <typeinfo>

int main()
{
    void * pVoid = NULL;
    cout << " pVoid = " << pVoid << "\n";

    int * pInt = NULL;
    cout << " pInt =  " << pInt << "\n";
    
    int xInt = 10;
    pInt = &xInt;
    cout << " pInt =  " << pInt << "\n";

    size_t dec_pInt = reinterpret_cast<size_t>(pInt);
    cout << "\nAfter \"size_t dec_pInt = reinterpret_cast<size_t>(pInt)\": \n";
    cout << " dec_pInt = " << dec_pInt << "\n";

    pVoid = pInt;
    cout << "\nAfter \"pVoid = pInt\": \n";
    cout << " pInt =  " << pInt << "\n";
    cout << " pVoid = " << pVoid << "\n";

    pInt = static_cast<int*>(pVoid);

    char * pChar = NULL;
    char ch = 'W';
    cout << " &ch = " << &ch << "\n";

    pChar = &ch;
    cout << "\nAfter \"pChar = &ch\": \n";
    cout << " pChar = " << pChar << "\n";

    size_t dec_pChar = reinterpret_cast<size_t>(pChar);
    cout << "\nAfter \"size_t dec_pChar = reinterpret_cast<size_t>(pChar)\": \n";
    cout << " dec_pChar = " << dec_pChar << "\n";

    pVoid = pChar;
    cout << "\nAfter \"pVoid = pChar\": \n";
    cout << " pVoid = " << pVoid << "\n";

    pVoid = &ch;
    cout << "\nAfter \"pVoid = &ch\": \n";
    cout << " pVoid = " << pVoid << "\n";


    size_t dec_pVoid = reinterpret_cast<size_t>(pVoid);
    cout << "\nAfter \"size_t dec_pVoid = reinterpret_cast<size_t>(pVoid)\": \n";
    cout << " dec_pVoid = " << dec_pVoid << "\n";

    if (dec_pVoid == dec_pChar) {
        cout << "\ndec_pVoid == dec_pChar\n";
    }


    cout << "(int*)(0) = " << (int*)(0) << "\n";
    cout << "typeid( (int*)(0) ).name() = " << typeid( (int*)(0) ).name() << "\n";
    cout << "(double*)(0) = " << (double*)(0) << "\n";
    cout << "(bool*)(0) = " << (bool*)(0) << "\n";

    int i = 20;
    int* p1i = &i;
    int* pi2 = p1i;
    ++pi2;
    cout << "   pi2 - p1i = " << pi2 - p1i << "\n";

    char* p1ch = reinterpret_cast<char*>(p1i);
    char* p2ch = reinterpret_cast<char*>(pi2);
    cout << "   p2ch - p1ch = " << p2ch - p1ch << "\n";

    return 0; /// program ended successfully
}/// end function main


