
#include "../../std_lib_facilities_haykav.h"

class Roman_int {
public:
    char romanDigitChar;
    string romanNumber = "";
    vector<int> intVector; 
    Roman_int() { }
    Roman_int(const string& inputRomanNumber);

    void add_RomDigit_to_RomNumber( char inputromandigit);
    int romanDigit_to_int( char romanDigit);
    vector<int> romanNumber_to_intVector(const string& inputRomanNumber);
    int as_int();
    string integer_to_Roman_int(const int& inputNumber);
    bool is_Roman_int(const string& inputString);

    char First_Symbol(const int& digitSystemValue);
    char Fifth_Symbol(const int& digitSystemValue);
    string RomanDigitArranging(const int& digitSystemValue, const int& digitValue);

};

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
Roman_int::Roman_int(const string& inputRomanNumber) 
    :romanNumber{ inputRomanNumber } 
{ 
    intVector = romanNumber_to_intVector(inputRomanNumber); 
}

char
Roman_int::First_Symbol(const int& digitSystemValue)
{
    switch (digitSystemValue) {
    case 0: return '0';
    case 1: return 'I';
    case 2: return 'X';
    case 3: return 'C';
    default:
        error("Error: In First_Symbol function invalid digitSystemValue: ", digitSystemValue);
    }
}
   
char
Roman_int::Fifth_Symbol(const int& digitSystemValue)
{
    switch (digitSystemValue) {
    case 1: return 'V';
    case 2: return 'L';
    case 3: return 'D';
    default:
        error("Error: In Fifth_Symbol function invalid digitSystemValue: ", digitSystemValue);
    }
}
  
string
Roman_int::RomanDigitArranging(const int& digitSystemValue, const int& digitValue)
{
    string str = "";
    char firstSymbolOfSystem = First_Symbol(digitSystemValue);
    switch (digitValue) {
    case 0: {
        return str; }
    case 1:
    case 2:
    case 3: {
        for (int counter = 1; counter <= digitValue; ++counter) {
            str += firstSymbolOfSystem;
        }
        return str; }
    case 4: {
        str += firstSymbolOfSystem;
        str += Fifth_Symbol(digitSystemValue);
        return str; }
    case 5: {
        str += Fifth_Symbol(digitSystemValue);
        return str; }
    case 6:
    case 7:
    case 8: {
        str += Fifth_Symbol(digitSystemValue);
        int loopLimit = digitValue - 5;
        for (int counter = 1; counter <= loopLimit; ++counter) {
            str += firstSymbolOfSystem;
        }
        return str; }
    default: {
        str += First_Symbol(digitSystemValue);
        str += First_Symbol(digitSystemValue + 1);
        return str; }
    }
}

string
Roman_int::integer_to_Roman_int(const int& inputNumber) 
{
    int copyOf_inputNumber = inputNumber;
    vector<int> digitValues;

    /// finding digit-system
    do {
        int digit = copyOf_inputNumber % 10;
        digitValues.push_back(digit);
        copyOf_inputNumber /= 10;
    } while (0 != copyOf_inputNumber);

    string romanNumberString;
    for (int digitSystemValue = digitValues.size(); digitSystemValue > 0; --digitSystemValue) {
        int index = digitSystemValue - 1;
        int digitValue = digitValues[index];
        romanNumberString += RomanDigitArranging(digitSystemValue, digitValue);
    }

    return romanNumberString;
}
    
void Roman_int::add_RomDigit_to_RomNumber(char inputromandigit)
{
    romanNumber += inputromandigit;
}
   
int Roman_int::romanDigit_to_int(char romanDigit) 
{
    switch (romanDigit) {
    case 'I': return 1;
    case 'V': return 5;
    case 'X': return 10;
    case 'L': return 50;
    case 'C': return 100;
    case 'D': return 500;
    case 'M': return 1000;
    default: {
        error("Bad Roman digit");
        }
    }
}
        
vector<int>
Roman_int::romanNumber_to_intVector(const string& inputRomanNumber)
{
    vector<int> result;
    for (const char& ch : inputRomanNumber) {
        int tempInt = romanDigit_to_int(ch);
        result.push_back(tempInt);
    }

    return result;
}

istream& operator >>(istream& is, Roman_int& romanInt) 
{
    if (!romanInt.romanNumber.empty()) {
        romanInt.romanNumber.clear();
    }

    if (!romanInt.intVector.empty()) {
        romanInt.intVector.clear();
    }

    string inputString;
    is >> inputString; 
    romanInt.intVector = romanInt.romanNumber_to_intVector(inputString);
    romanInt.romanNumber = inputString;

    return is;
}

ostream& operator <<(ostream& os, const Roman_int& romanInt) 
{
    if (0 == romanInt.romanNumber.size()) {
        error("There is no roman number to print\n");
    }

    return os << romanInt.romanNumber;;
}

bool Roman_int::is_Roman_int(const string& inputString) 
{
    for (const char& ch : inputString) {
        switch (ch) {
        case 'I': 
        case 'V': 
        case 'X':
        case 'L':
        case 'C':
        case 'D':
        case 'M': break;
        default: {
            return false;
            }
        }
    }

    return true;
}

int Roman_int::as_int()
{
    if (intVector.size() > 1) {
        for (int index = 1; index < intVector.size(); ++index) {
            if (intVector.at(index) > intVector.at(index - 1)) {
                intVector.at(index - 1) = intVector.at(index) - intVector.at(index - 1);
                intVector.erase(intVector.begin() + index);
            }
        }
    }

    int sum = 0;
    for (int elementOfVector : intVector) {
        sum += elementOfVector;
    }

    return sum;
}

bool is_RomanNumber(const string& inputString)
{
    for (const char& ch : inputString) {
        switch (ch) {
        case 'I': 
        case 'V': 
        case 'X':
        case 'L':
        case 'C':
        case 'D':
        case 'M': break;
        default: {
            return false;
            }
        }
    }

    return true;
} 

