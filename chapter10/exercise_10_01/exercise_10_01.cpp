/*

*/
#include "../../std_lib_facilities_haykav.h"


/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void Read_Ints_From_File(string& inputFileName, vector<int>& IntNumberS);
int Sum_Of(vector<int>& IntNumberS);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        string fileName = "digits.txt";
        vector<int> IntNumberS;
        Read_Ints_From_File(fileName, IntNumberS);

        cout << fileName << "'s content\n";
        for (int oneNumber : IntNumberS) {
            cout << oneNumber << " ";
        }
        cout << endl;

        cout << "sum of int numbers: " << Sum_Of(IntNumberS) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

void 
Read_Ints_From_File(string& inputFileName, vector<int>& intNumberS)
{
    ifstream ist {inputFileName};
    if (!ist) error("can't open input file ",inputFileName);

    for (int oneInt; ist >> oneInt;) {
        intNumberS.push_back(oneInt);
    }
}

int 
Sum_Of(vector<int>& IntNumberS)
{
    int sum = 0;

    for (int oneInt : IntNumberS) {
        sum += oneInt;
    }

    return sum;
}


