/*

*/
#include "../../std_lib_facilities_haykav.h"

class Point {
public:
    char ch1;
    char ch2;
    char ch3;
    int x;
    int y;
};

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
istream& operator >> (istream& inputStream, Point& inputPoint);
ostream& operator << (ostream& outputStream, Point& outputPoint);
void fill_Points_To_File(vector<Point>& points, string& outFileName);
void read_Points_From_File(string& inputFileName, vector<Point>& points);
bool operator != (const Point& point1, const Point& point2);
bool operator == (const vector<Point>& points1, const vector<Point>& points2);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        cout << "Please enter 7 points (coordinates)\n";

        vector<Point> original_points;
        for (Point onePoint; original_points.size() < 7;) {
            cin >> onePoint;
            original_points.push_back(onePoint);
        }

        for (Point onePoint : original_points) {
            cout << onePoint;
        }

        string fileName = "mydata.txt";
        fill_Points_To_File(original_points, fileName);

        vector<Point> processed_points;
        read_Points_From_File(fileName, processed_points);

        cout << endl;
        cout << "processed_points's content\n";
        for (Point onePoint : processed_points) {
            cout << onePoint;
        }

        if (processed_points == original_points) {
            cout << "Everything is all right\n";
        } else {
            cout << "Something went wrong\n";
        }
     
    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
istream& operator >> (istream& inputStream, Point& inputPoint)
{
    int x = -999;
    int y = -999;
    char ch1 = 'q';
    char ch2 = 'q';
    char ch3 = 'q';

    inputStream >> ch1 >> x >> ch2 >> y >> ch3;
    if (!inputStream) {
        return inputStream;
    }

    if (ch1!='(' || ch2!=',' || ch3!=')') { // oops: format error
        inputStream.clear(ios_base::failbit);
        return inputStream;
    }

    inputPoint.ch1 = ch1;
    inputPoint.x = x;
    inputPoint.ch2 = ch2;
    inputPoint.y = y;
    inputPoint.ch3 = ch3;

    return inputStream;
}

ostream& 
operator << (ostream& outputStream, Point& outputPoint)
{
    outputStream << outputPoint.ch1 << outputPoint.x << outputPoint.ch2
                 << outputPoint.y << outputPoint.ch3;

    return outputStream;
}

void 
fill_Points_To_File(vector<Point>& points, string& outFileName)
{
    ofstream ost {outFileName};
    if (!ost) error("can't open output file ",outFileName);

    for (Point point : points) {
        ost << point;
    }
}

void 
read_Points_From_File(string& inputFileName, vector<Point>& points)
{
    ifstream ist {inputFileName};
    if (!ist) error("can't open input file ",inputFileName);

    for (Point mypoint; ist >> mypoint;) {
        points.push_back(mypoint);
    }
}

bool 
operator != (const Point& point1, const Point& point2)
{
    if (point1.ch1 != point2.ch1 || 
        point1.x != point2.x || 
        point1.ch2 != point2.ch2 || 
        point1.y != point2.y || 
        point1.ch3 != point2.ch3) {
        return true;
    }
   
    return false;
}

bool 
operator == (const vector<Point>& points1, const vector<Point>& points2)
{
    if (points1.size() != points2.size()) {
        return false;
    }

    for (int index = 0; index < points1.size(); ++index) {
        if (points1[index] != points2[index]) {
            return false;
        }
    }

    return true;
}


