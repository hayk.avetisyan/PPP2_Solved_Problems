//////// SOLUTION of the exercise_10_03 ////////////////////////////////////
/// Calculating mean temp
//////////////////////////////////////////////////////////////////////////////////////

/// commonly usable directives and some functions
#include "../../std_lib_facilities_haykav.h" 

struct Reading {        // a temperature reading
    int hour;           // hour after midnight [0:23]
    double temperature; // in Fahrenheit
};

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void From_File_to_Vector(string& inputFileName, vector<Reading>& hourTempS);
int Sum_Of(vector<int>& InthourTempS);
double meanTemp(vector<Reading>& hourTempS);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        vector<Reading> hourTempS;
        string inputFile = "raw_temps.txt";
        From_File_to_Vector(inputFile, hourTempS);

        cout << "mean temperature: " << meanTemp(hourTempS) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void 
From_File_to_Vector(string& inputFileName, vector<Reading>& hourTempS)
{
    ifstream ist {inputFileName};
    if (!ist) error("can't open input file ",inputFileName);

    char ch1;
    char ch2;
    char ch3;
    int hour;
    double temperature;

    while (ist >> ch1 >> hour >> ch2 >> temperature >> ch3) {
        hourTempS.push_back(Reading{hour,temperature});
    }
}

double
meanTemp(vector<Reading>& hourTempS)
{
    double sum = 0;
    int count = 0;

    for (Reading hourTemp : hourTempS) {
        sum += hourTemp.temperature;
        ++count;
    }

    return sum / count;
}

