//////// SOLUTION of the exercise_10_04 ////////////////////////////////////
/// Reading type using
//////////////////////////////////////////////////////////////////////////////////////

/// commonly usable directives and some functions
#include "../../std_lib_facilities_haykav.h" 

struct Reading {        // a temperature reading
    int hour;           // hour after midnight [0:23]
    double temperature; // in Fahrenheit
};

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
int greatCommonDivisor(int number1, int number2);
void Fill_Hours_Temps_to_File(const string& filledFileName, vector<Reading>& hourTemps);
void Copy_File_Into_test1out(const string& inputFileName);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        vector<Reading> temps; // store the readings here
        int hour = -1;
        double temperature = -777;
        char tempType = 'q';

        cout << "Enter hour, temp suffix 'c' or 'f' and temperature value\n";
        while (cin >> hour >> tempType >> temperature) {
            if (hour < 0 || 23 < hour) error("hour out of range");

            if ('c' == tempType) {
                temperature = temperature * 1.8 + 32;
            } else if ('f' != tempType) {
                error("wrong temperature type");
            }

            temps.push_back(Reading{hour, temperature});
        }

        string outputFile = "raw_temps.txt";
        ofstream ost {outputFile}; // ost writes to a file named outputFile
        Fill_Hours_Temps_to_File(outputFile, temps);
        Copy_File_Into_test1out(outputFile);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void
Fill_Hours_Temps_to_File(const string& filledFileName, vector<Reading>& hourTemps)
{
    ofstream ost {filledFileName}; // ost writes to a file named outputFile
    for (int i=0; i < hourTemps.size(); ++i) {
        ost << '(' << hourTemps[i].hour << ','
        << hourTemps[i].temperature << ")\n";
    }
}

void
Copy_File_Into_test1out(const string& inputFileName)
{
    string outputFileName = "test1.out";
    ifstream ist {inputFileName};
    ofstream ost_test_out ("test1.out", ios::app);
    ost_test_out << "\nHere is output file content:\n";
    for(string line; getline(ist, line);) {
        ost_test_out << line << endl;
    }
}

