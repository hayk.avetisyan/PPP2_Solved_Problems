//////// SOLUTION of the exercise_10_02 ////////////////////////////////////
/// Reading type using
//////////////////////////////////////////////////////////////////////////////////////

/// commonly usable directives and some functions
#include "../../std_lib_facilities_haykav.h" 

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void Read_Ints_From_File(string& inputFileName, vector<int>& IntNumberS);
int Sum_Of(vector<int>& IntNumberS);
//////////////////////////////////////////////////////////////////////////////////////

struct Reading {        // a temperature reading
    int hour;           // hour after midnight [0:23]
    double temperature; // in Fahrenheit
};

int main()
{
    try {
        string outputFile = "raw_temps.txt";
        ofstream ost {outputFile}; // ost writes to a file named oname

        vector<Reading> temps; // store the readings here
        int hour = -1;
        double temperature = -777;

        for (int counter = 1; counter <= 50; ++counter) {
            hour = 1 + rand() % 24;
            temperature = 0 + rand() % 50;
            temps.push_back(Reading{hour,temperature});
        }

        for (int i=0; i<temps.size(); ++i) {
            ost << '(' << temps[i].hour << ','
            << temps[i].temperature << ")\n";
        }

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

void 
Read_Ints_From_File(string& inputFileName, vector<int>& intNumberS)
{
    ifstream ist {inputFileName};
    if (!ist) error("can't open input file ",inputFileName);

    for (int oneInt; ist >> oneInt;) {
        intNumberS.push_back(oneInt);
    }
}

int 
Sum_Of(vector<int>& IntNumberS)
{
    int sum = 0;

    for (int oneInt : IntNumberS) {
        sum += oneInt;
    }

    return sum;
}


