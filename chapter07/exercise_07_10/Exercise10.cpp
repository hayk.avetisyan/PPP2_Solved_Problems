
/*
calculator08buggy.cpp

Helpful comments removed.

We have inserted 3 bugs that the compiler will catch and 3 that it won't.
*/

#include "/SCRATCH/Cplusplus/std_lib_facilities_haykav.h"


class Token {   // declaration of token class
public:
	char kind;
	double value;
	string name;
	string func_name;
	Token(char ch) :kind{ ch } { } // character Token constructor
	Token(char ch, double val) :kind{ ch }, value{ val } { } // number Token constructor
	Token(char ch, string n) :kind{ ch }, name{ n } { } // name Token constructor
	Token(string n) :kind{ 'f' }, func_name{ n } { } // name Token constructor
};

class Token_stream {
	bool full{ false }; // initialization of the buffer is false, as it's empty
	Token buffer;
public:
	Token_stream() :full(0), buffer(0) { } //Token_stream constructor

	Token get(); // take the value from the input
	void unget(Token t) { buffer = t; full = true; } // put the input item into the buffer

	void ignore(char); // ignore chars up to the ';' symbol
};

// constants
const char let = 'L';
const char quit = 'q';
const char print = ';';
const char number = '8';
const char name = 'n';
const char math_func = 'f';
//const string declkey = "#";
const string declkey = "let";
const string constkey = "const";
const char constkind = 'c';
const char assign = 'a';

vector<Token> tokens;

class Variable { // user-defined "Variable" class for variable names and those appropriate values
	string name;
	double value;
public:
	Variable(string ns, double val) :name(ns), value(val) { } //Variable constructor
	string get_name() { return name; };
	void set_value(double x) { value = x; };
	double get_value() { return value; };
};

class Symbol_table {
	vector<Variable> var_table;
public:
	double get(string s);
	void set(string s, double d);
	bool is_declared(string s);
	double declare(string var, double val);

};

Token Token_stream::get()
{
	if (full) { full = false; return buffer; } // first checks the buffer
	char ch;
	cin.get(ch);

	while (ch == ' ') cin.get(ch);

	switch (ch) {
	case '=':
	case '(':
	case ')':
	case '+':
	case '-':
	case '*':
	case '/':
	case '%':
	case ';':
	case ',':
	case '\n':
		return Token(ch);
	case '#':
		return Token(let);
	case '.':
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	{	cin.putback(ch);
	double val;
	cin >> val;
	return Token(number, val);
	}
	default:
	{
		if (isalpha(ch) || ch == '_') { //checks if the 'ch' is letter
			string s;
			s += ch; //Assign the 'ch' to the first character of the s string
			while (cin.get(ch) && (isalpha(ch) || isdigit(ch) || ch == '_')) s += ch; // takes the 'ch' and if the it letter or digit.
			
			if (s == declkey)  {
				return Token(let); // if string is 'let' then returns 'L' Token
			}
			if (s == constkey) {
				cout << "inside const\n";
				return Token(constkind);
			}
			if (s == "exit") {
				cin.putback(ch);
				return Token(quit); // if string is 'let' then returns 'L' Token
			}
			if (s == "sqrt" || s == "pow") {
				cin.putback(ch);
				return Token(s); // if string is 'let' then returns 'L' Token
			}

			while (ch == ' ')  cin.get(ch); 
			if (ch == '=') return Token(assign, s);
			cin.putback(ch);
			return Token(name, s); // If the string is not 'L' then it is the name of the user-defined variable
		}

	}
		error("Bad token");
	}
}

void Token_stream::ignore(char c)  //After wrong input this function ignores all chars upto ';' char, i.e. print.
{
	if (full && c == buffer.kind) { //First look in buffer
		full = false;
		return;
	}
	full = false;

	char ch;
	while (cin >> ch)  // "eats" every char after some incorrect input up to ';' char, i.e. print
		if (ch == c) return;
}



bool Symbol_table::is_declared(string s) // checks if the variable is declared
{
	for (int i = 0; i < var_table.size(); ++i)
		if (var_table[i].get_name() == s) return true;
	return false;
}


double Symbol_table::get(string s)
{
	for (int i = 0; i < var_table.size(); ++i)
		if (var_table[i].get_name() == s) return var_table[i].get_value();
	error("get: undefined name ", s);
}

void Symbol_table::set(string var_name, double var_value)
{

	for (int i = 0; i < var_table.size(); ++i)
	{
		if (var_table[i].get_name() == var_name)
		{
			var_table[i].set_value(var_value);
			return;
		}
	}
	var_table.push_back(Variable(var_name, var_value));

}

Symbol_table Sym_tab;

vector<Variable> var_table; //var_table is the Variable vector.

class Const_var {
public:
	const string name;
	const double value;
	Const_var(string const_var, double const_val) :name(const_var), value(const_val) {}
};

vector<Const_var> Const_table;

double get_const_value(string s) //take value of the defined variable
{
	for (int i = 0; i < Const_table.size(); ++i)
		if (Const_table[i].name == s) return Const_table[i].value;
	error("get: undefined name ", s);
}



bool is_declared_const(string s) // checks if the variable is declared
{
	for (int i = 0; i < Const_table.size(); ++i)
	{
		if (Const_table[i].name == s) return true;
	}
	return false;
}

Token_stream ts; // Token_stream object declaration

double expression(); // expresstion function prototype calling, for to be used in other functions

double primary() // The level - 5 function of our design after term(), do the '*' and '/' operation
{
	Token t = ts.get();
	switch (t.kind) {
	case '(':  // checking existance of '(expr)'
	{	double d = expression();
	t = ts.get();
	if (t.kind != ')') error("')' expected");
	return d;
	}
	case math_func:  // checking existance of '(expr)'
	{
		if (t.func_name == "sqrt")
		{
			t = ts.get();
			if (t.kind != '(') error("'(' expected for sqrt function");
			double d = expression();
			if (d < 0) error("negative value under the sqrt()");
			t = ts.get();
			if (t.kind != ')') error("')' expected for sqrt function");
			return sqrt(d);
		}
		if (t.func_name == "pow")
		{
			t = ts.get();
			if (t.kind != '(') error("'(' expected for pow function");
			double d = primary();
			if (d < 0) error("negative value under the sqrt()");
			t = ts.get();
			if (t.kind != ',') error("',' expected for pow function");
			int i2 = narrow_cast<int>(primary());
			t = ts.get();
			if (t.kind != ')') error("')' expected for pow function");
			return pow(d, i2);
		}
	}
	case '-':
		return -primary(); // getting minus digit
	case '+':
		return primary(); // getting minus digit
	case number:
		return t.value; // if input char is number then assign the number to t.value
	case name:
	{
		if (is_declared_const(t.name))
		{
			return get_const_value(t.name);
		}
		if (!Sym_tab.is_declared(t.name)) error(t.name, "is not declared");
		return Sym_tab.get(t.name); // if input char is name then assign the number to t.name
	}

	default:
		error("primary expected");
	}
}

double term() // The level - 4 function of our design after expression(), do the '*' and '/' operation
{
	double left = primary();
	int i0 = narrow_cast<int>(left);
	Token t = ts.get();
	while (true) {

		switch (t.kind) {
		case '*':
		{   left *= primary(); // Second call of the primary is aimed to take next digit or check do we have '(expr)'later
		t = ts.get();
		break;
		}
		case '/':
		{	double d = primary(); // Second call of the primary is aimed to take next digit or check do we have '(expr)'later
		if (d == 0) error("divide by zero"); //error handling for dividing by zero
		left /= d;
		t = ts.get();
		break;
		}
		case '%':
		{
			int i1 = narrow_cast<int>(left);
			int i2 = narrow_cast<int>(primary());
			if (i2 == 0) error("%: divide by zero");
			left = i1%i2;
			t = ts.get();
			break;
		}
		default:
		{
			ts.unget(t);
			int i_0 = narrow_cast<int>(left);
			return left;
		}
		}
	}
}

double expression() // The level - 3 function of our design after statement(), do the '+' and '-' operation
{
	double left = term();
	int i0 = narrow_cast<int>(left);
	Token t = ts.get();
	while (true) {

		switch (t.kind) {
		case '+':
			left += term(); // Second call of the term is aimed to check do we have '*' or '/' operations later
			t = ts.get();
			break;
		case '-':
			left -= term(); // Second call of the term is aimed to check do we have '*' or '/' operations later
			t = ts.get();
			break;
		default:
		{
			ts.unget(t);
			int i_0 = narrow_cast<int>(left);
			return left;
		}
		}
	}
}



double define_const_name(string var, double val) // definition of user-defined variable
												 //add {var,val} to var table
{
	if (is_declared_const(var)) error(var, " declared twice");
	Const_table.push_back(Const_var(var, val));
	return val;
}


void const_name(string var, double val) // definition of user-defined variable
												 //add {var,val} to var table
{
	if (is_declared_const(var)) error(var, " declared twice");
	Const_table.push_back(Const_var(var, val));
	return;
}


double declaration(Token tin) // The level-3 function of our design after statement()
							  // assume we have seen "let"
							  // handle: name = expression
							  // declare a variable called "name" with the initial value "expression"
{
	switch (tin.kind) {
	case let:
	{
		Token t = ts.get();
		if (t.kind != assign) error("'assign' token expected in const declaration");
		string var_name = t.name;
		double d = expression();
		Sym_tab.set(t.name, d);
		return d;
	}
	case constkind:
	{
		Token t = ts.get();
		if (t.kind != assign) error("'assign' token expected in const declaration");
		string const_name = t.name;
		double d = expression();
		define_const_name(const_name, d);
		return d;
	}
	case assign:
	{
		//cout << "tin.name:" << tin.name << "\n";
		if (is_declared_const(tin.name)) error(tin.name, " is declared as const");
		double d = expression();
		Sym_tab.set(tin.name, d);
		return d;
	}
	}
}

double statement() // The level-2 function of our design after calculate()
{
	Token t = ts.get();
	switch (t.kind) {
	case let:  // if let, then it is declaration of variable
	case constkind:
	case assign:
		return declaration(t);

	default:
	{
		ts.unget(t);
		double d = expression();
		int i0 = narrow_cast<int>(d);
		return d;
	}
	}
}

void clean_up_mess()
{
	ts.ignore(print); // ignore all characters up to ';', i.e. print
}

// constants
const string prompt = "> ";
const string result = "= ";

void calculate()  // The top function of our design
{
	while (cin)
		try {
		cout << prompt; //prompts user to input expressions
		char ch;
		cin >> ch;
		if ((ch == 'h') || (ch == 'q'))
		{
			string s;
			s += ch;
			while (cin.get(ch) && isalpha(ch)) s += ch;
			if (s == "help") cout << "Welcome to help guide\n";
			else if (s == "quit") return;
			else
				error("Invalid command, may be you want to input 'help'.");
		}
		else
		{
			cin.putback(ch);
			Token t = ts.get();
			while (t.kind == print || t.kind == '\n') t = ts.get(); //first discard all 'prints'
			if (t.kind == quit) return; //quit
			ts.unget(t);
			cout << " = " << statement() << endl; // call of the statement function
		}
	}
	catch (runtime_error& e) { // the catch is here for not giving error message if we have a few statements
		cerr << e.what() << endl;
		clean_up_mess(); //after some incorrect input this function eats all chars up to ';'
	}
}

int main()

try {
	const_name("pi", 3.14159265359);
	const_name("e", 2.718281828459045);

	calculate(); // calling calculation function
	return 0;
}
catch (exception& e) {
	cerr << "exception: " << e.what() << endl;
	return 1;
}
catch (...) {
	cerr << "exception\n";
	return 2;
}
