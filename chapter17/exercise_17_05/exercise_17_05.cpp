//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void to_lower(char* s);
char* strdup(const char* s);
const char* findx(const char* s, const char* x);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char s[] = "Hello, World!";
        char ch1 = 'W';
        char ch2 = 'A';
        cout << "ch1 = " << ch1 << endl;
        char *pCh1 = &ch1;
        char *pCh2 = &ch2;
        cout << "s = " << s << endl;
        cout << "pCh1 = " << pCh1 << endl;
        cout << "pCh2 = " << pCh2 << endl;
        cout << "findx(\"" << s << "\", pCh1) =" << findx("Hello, World!", pCh1) << endl;
        cout << "findx(\"" << s << "\", pCh2) =" << findx("Hello, World!", pCh2) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void to_lower(char* s)
{
    while (*s != '\0') {
        if ('A' <= *s && *s <= 'Z') {
            *s += 32;
        }

        ++s;
    }
}

char* strdup(const char* s)
{
    int size = 0;
    while (*s != '\0') {
        ++s;
        ++size;
    }
    s -= size;

    ++size;
    cout << "size = " << size << endl;
    char* sNew = new char[size];
    int loopLimit = size - 2;
    for (int index = 0; index <= loopLimit; ++index) {
        sNew[index] = s[index];
        cout << sNew[index];

    }

    sNew[size - 1] = '\0';
    cout << sNew[size - 1] << endl;

    return sNew;
}

const char* findx(const char* s, const char* x)
{
    for (; *s != '\0'; ++s) {
        if (*x == *s) {
            return s;
        }
    }

    cout << "There is no " << *x << endl;
}



