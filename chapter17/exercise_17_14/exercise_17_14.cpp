//////// SOLUTION of exercise_17_14 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h" /// all include stuff

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

struct god {
    string name = "";
    string mythology = "";
    string vehicle = "";
    string weapon = "";
    god(const string& nameInit, const string& mythInit, const string& weapInit, const string& vehicleInit)
        : name{nameInit}, mythology{mythInit}, weapon{weapInit}, vehicle{vehicleInit} { }
    god(const string& nameInit, const string& mythInit,  const string& weapInit)
        : name{nameInit}, mythology{mythInit}, weapon{weapInit} { }
    god(const string& nameInit, const string& mythInit)
        : name{nameInit}, mythology{mythInit} { }
};

class Link {
public:
    string value;
    god godLink;

    Link(const string& nameInit, const string& mythInit, const string& weapInit, const string& vehicleInit,
         Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit, weapInit, vehicleInit}), succ{succInit} { }

    Link(const string& nameInit, const string& mythInit, const string& weapInit,
         Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit, weapInit}), succ{succInit} { }

    Link(const string& nameInit, const string& mythInit, Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit}), succ{succInit} { }

    Link* insert(Link *pToLinkNew); // insert n before this object
    Link* add(Link *pToLinkNew); // insert n after this object
    void erase(Link *pToErasable); // remove this object from list  
    Link* find(const string& str); // find s in list
    const Link* find(const string& str) const; // find s in const list (see §18.5.1)
    Link* advance(int n); // move n positions in list
    Link* next() const { return succ; }

    Link* add_ordered(Link *pToLinkNew);
private:
    Link *succ;
};

Link *Link::insert(Link *pToLinkNew)
{
    if (nullptr == pToLinkNew) return this;
    if (nullptr == this) return pToLinkNew;

    pToLinkNew->succ = this;
    return pToLinkNew;
}

Link *Link::add(Link *pToLinkNew) 
{
    if (nullptr == pToLinkNew) return this;
    if (nullptr == this) return pToLinkNew;
    pToLinkNew->succ = succ;
    succ = pToLinkNew;
    return pToLinkNew;
}

void Link::erase(Link *pToErasable)
{
    if (nullptr == this) return;
    if (nullptr == pToErasable) return;

    Link *pIterator = this;
    Link *pToPrev = this;
    while (nullptr != pIterator) {
        if (pIterator == pToErasable) {
            pToPrev->succ = pIterator->succ;
            pIterator->succ = 0;
            return;
        }

        pToPrev = pIterator;
        pIterator = pIterator->succ;
    }

    return;
}

Link *Link::find(const string& str) // find s in list
{
    Link *pIterator = this;
    while(pIterator) {
        if (str == pIterator->godLink.name) return pIterator;
        pIterator = pIterator->succ;
    }
    
    return nullptr;
}

Link *extract(Link **ppLink, const string& godName)
{
    Link *pToFoundLink = (*ppLink)->find(godName);

    if (nullptr == pToFoundLink) return nullptr;
    Link *pToTemp = *ppLink;
    if (pToFoundLink == *ppLink) *(ppLink) = (*ppLink)->next();
    pToTemp->erase(pToFoundLink);

    return pToFoundLink;
} 

Link *Link::add_ordered(Link *pToLinkNew)
{
    if (nullptr == pToLinkNew) return this;
    if (nullptr == this) {
        pToLinkNew->succ = 0;
        return pToLinkNew;
    }

    if (pToLinkNew->godLink.name <= godLink.name) {
        Link *pIterator = insert(pToLinkNew);
        return pIterator;
    }

    Link *pIterator = this;
    Link *pToPrev = nullptr;
    while(pToLinkNew->godLink.name > pIterator->godLink.name) {
        if (pIterator->succ) {
            pToPrev = pIterator;
            pIterator = pIterator->succ;
        } else {
            pIterator->add(pToLinkNew);
            return this;
        }
    }

    pToPrev->succ = pToLinkNew;
    pToLinkNew->succ = pIterator;
    return this;
}

void print_all(Link *pLink)
{
    cout << "{\n";
    while (pLink) {
        cout << "\t" << pLink->godLink.name << ", " << pLink->godLink.mythology;
        
        string tempString = pLink->godLink.weapon;
        if ("" != tempString) cout << ", " << tempString;

        tempString = pLink->godLink.vehicle;
        if ("" != tempString) cout << ", " << tempString;

        pLink = pLink->next();
        cout << endl;
    }

    cout << "}";
}

Link *Link::advance(int n) // move n positions in list
{
    if (nullptr == this) return nullptr;

    Link *pIterator = this;
    if (n > 0) {
        while (n--) {
            if (nullptr == pIterator->succ) return nullptr;
            pIterator = pIterator->succ;
        }
    } else if (n < 0) {
        error("Singly-Linked list has no pointer to previous-member access");
        }

    return this;
}

int main()
{
    try {
        Link *gods = new Link{"Odin", "Norse", "Spear called Gungnir", "Eight-legged flying horse called Sleipner"};
        gods = gods->insert(new Link{"Thor", "Norse", "Hammer"});
        gods = gods->insert(new Link{"Loki", "Norse"});
        gods = gods->insert(new Link{"Balder", "Norse"});
        gods = gods->insert(new Link{"Frigg", "Norse"});
        gods = gods->insert(new Link{"Tyr", "Norse", "Spear and shield"});
        gods = gods->insert(new Link{"Mars", "Roman", "Spear and shield"});
        gods = gods->insert(new Link{"Saturn", "Roman"});
        gods = gods->insert(new Link{"Mercury", "Roman"});
        gods = gods->insert(new Link{"Apollo", "Roman"});
        gods = gods->insert(new Link{"Vulcan", "Roman"});
        gods = gods->insert(new Link{"Neptune", "Roman", "Trident", "Sea horse"});
        gods = gods->insert(new Link{"Posidon", "Greek", "Trident"});
        gods = gods->insert(new Link{"Hera", "Greek"});
        gods = gods->insert(new Link{"Hades", "Greek", "Helmet of darkness"});
        gods = gods->insert(new Link{"Hermes", "Greek", "Caduceus"});
        gods = gods->insert(new Link{"Zeus", "Greek", "lightning"});
        gods = gods->insert(new Link{"Dionysus", "Greek"});
        gods = gods->insert(new Link{"Athena", "Greek"});
        gods = gods->insert(new Link{"Ares", "Greek", "Spear and shield"});
        cout << "various gods:\n";
        print_all(gods);

        Link *norse_gods = extract(&gods, "Odin");
        norse_gods = norse_gods->add_ordered(extract(&gods, "Freya"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Tyr"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Thor"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Loki"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Balder"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Frigg"));
        newLine(3);
        cout << "norse_gods after extraction:\n";
        print_all(norse_gods);

        Link *roman_gods = extract(&gods, "Mars");
        roman_gods = roman_gods->add_ordered(extract(&gods, "Saturn"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Mercury"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Apollo"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Vulcan"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Neptune"));
        newLine(3);
        cout << "roman_gods after extraction:\n";
        print_all(roman_gods);

        Link *greek_gods = extract(&gods, "Posidon");
        greek_gods = greek_gods->add_ordered(extract(&gods, "Hera"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Hades"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Zeus"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Dionysus"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Athena"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Ares"));
        newLine(3);
        cout << "greek_gods after extraction:\n";
        print_all(greek_gods);

        newLine(3);
        cout << "various gods:\n";
        print_all(gods);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////

