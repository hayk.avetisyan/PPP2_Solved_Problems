//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool); 
//////////////////////////////////////////////////////////////////////////////////////

/// Drill 4
void print_array10(ostream& os, int* pToInts)
{
    for (int index = 0; index < 10; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

/// Drill 7
void print_array(ostream& os, const int* pToInts, const int n)
{
    for (int index = 0; index < n; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

int main()
{
    try {
        /// TRY THIS page: 591
        char ch; 
        int i; 
        double d; 
        bool b; 
        char* pChar; 
        int* pInt; 
        double* pDouble;
        bool* pBool;
        sizes(ch, i, d, b, pChar, pInt, pDouble, pBool);

        //////////////////////////// PART 1 ////////////////////////////////////////
        cout << endl << "/////////////////// PART 1  /////////////////////" << endl;
        /// Drill 1
        int* pToInts = new int[10];
        cout << endl;
        cout << "pToInts = " << pToInts << "\n\n";

        /// Drill 2
        for (int index = 0; index < 10; ++index) {
            cout << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
                 << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
        }

        /// Drill 3
        delete [] pToInts;

        /// Drill 5
        int* pToInts10 = new int[10] {100,101,102,103,104,105,106,107,108,109};
        cout << endl;
        print_array10(cout, pToInts10);
        delete [] pToInts10;
        
        /// Drill 6
        int* pToInts11 = new int[11] {100,101,102,103,104,105,106,107,108,109,110};
        cout << endl;
        print_array10(cout, pToInts11);
        delete [] pToInts11;

        /// Drill 8
        int* pToInts20 = new int[20] {100,101,102,103,104,105,106,107,108,109,
                                      110,111,112,113,114,115,116,117,118,119};
        cout << endl;
        print_array(cout, pToInts20, 20);
        delete [] pToInts20;

        /// Drill 10
        /// 5, 6, and 8 using an array instead of vector, and print_array10
        /// and print_array using array instead of vector


        //////////////////////////// PART 2 ////////////////////////////////////////
        cout << endl << "/////////////////// PART 2  /////////////////////" << endl;
        /// Drill 1
        int* p1 = new int{7};

        /// Drill 2
        cout << endl << "p1 = " << p1 << ", *p1 = " << *p1 << endl;

        /// Drill 3
        int* p2 = new int[7] {1, 2, 4, 8, 16, 32, 64};

        /// Drill 4
        cout << endl;
        print_array(cout, p2, 7);

        /// Drill 5
        int* p3 = p2;

        /// Drill 6
        p2 = p1;

        /// Drill 7
        p2 = p3;

        /// Drill 8
        cout << "p1 = " << p1 << ", *p1 = " << *p1 << endl;
        cout << "p2 = " << p2 << ", *p2 = " << *p2 << endl;

        /// Drill 9
        delete [] p2;
        delete p1;

        /// Drill 10
        p1 = new int[10] {1, 2, 4, 8, 16, 32, 64, 128, 256, 512};

        /// Drill 11
        p2 = new int[10];

        /// Drill 12
        for (int index = 0; index <= 10; ++index) {
            p2[index] = p1[index];
        }

        /// Drill 13
        vector<int> v1(10);
        vector<int> v2 {1, 2, 4, 8, 16, 32, 64, 128, 256, 512};
        cout << endl;
        for (int index = 0; index <= 10; ++index) {
            v1[index] = v2[index];
            int* pv1 = &v1[index];
            cout << "pv1[" << index << "] = " << pv1 << endl;
        }

        cout << endl << "incrementing pointers\n";
        int* pv1 = &v1[0];
        for (int index = 0; index <= 10; ++index) {
            cout << "pv1[" << index << "] = " << &v1[index] << endl;
            ++pv1;
        }

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool) 
{
    cout << "the size of char is " << sizeof(char) << ' ' << sizeof (ch) << '\n';
    cout << "the size of int is " << sizeof(int) << ' ' << sizeof (i) << '\n';
    cout << "the size of double is " << sizeof(double) << ' ' << sizeof (d) << '\n';
    cout << "the size of bool is " << sizeof(bool) << ' ' << sizeof (b) << '\n';
    cout << "the size of char* is " << sizeof(char*) << ' ' << sizeof (pChar) << '\n';
    cout << "the size of int* is " << sizeof(int*) << ' ' << sizeof (pInt) << '\n';
    cout << "the size of double* is " << sizeof(double*) << ' ' << sizeof (pDouble) << '\n';
    cout << "the size of bool* is " << sizeof(bool*) << ' ' << sizeof (pBool) << '\n';
}    

