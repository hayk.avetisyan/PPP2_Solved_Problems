//////// SOLUTION of exercise_17_02 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
template<class T> int size(T);	// for every type T
template<class T> int size();	// for every type T
//////////////////////////////////////////////////////////////////////////////////////

struct X { int a, b, c; };

struct V {
    int a, b, c;
    virtual void f() {}
};

int main()
{
    try {
        double doubleNumber = 1.2;
        X x1Struct;
        V v1Struct;
        cout << "doubleNumber = " << doubleNumber << endl;
        cout << "size(doubleNumber) = " << size(doubleNumber) << endl;
        cout << "size<double>() = " << size<double>() << endl;
        cout << "size(x1Struct) = " << size(x1Struct) << endl;
        cout << "size(v1Struct) = " << size(v1Struct) << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
template<class T> 
int size(T)	// for every type T
{
    T* p = new T[2];
    int s = reinterpret_cast<char*>(&p[1])-reinterpret_cast<char*>(&p[0]);
    //cout << "&p[1] = " << &p[1] << endl;
    //cout << "&p[0] = " << &p[0] << endl;
    //cout << "reinterpret_cast<char*>(&p[1]) = " << reinterpret_cast<char*>(&p[1]) << end;
    //cout << "reinterpret_cast<char*>(&p[0]) = " << reinterpret_cast<char*>(&p[0]) << end;
    delete[] p;
    return s;
}

template<class T>
int size()	// for every type T
{
    T* p = new T[2];
    int s = reinterpret_cast<char*>(&p[1])-reinterpret_cast<char*>(&p[0]);
    delete[] p;
    return s;
}

