//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
char *increased_Array(const char *str);
char *get_String(void);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char *str = get_String();
        cout << "str = " << str << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
char *increased_Array(const char *str)
{
    size_t size = 1;
    while (str[size - 1]) {
        ++size;
    }

    char *strIncreased = new char[size + 1];
    for (size_t index = 0; str[index]; ++index) {
        strIncreased[index] = str[index];
    }

    delete[] str;
    return strIncreased;
}

char *get_String(void)
{
    char *str = new char[1];
    char ch;
    int index = 0;
    while (cin >> ch && ch != '!') {
        str[index] = ch;
        str = increased_Array(str);
        ++index;
    }

    str[index] = '\0';
    return str;
}






