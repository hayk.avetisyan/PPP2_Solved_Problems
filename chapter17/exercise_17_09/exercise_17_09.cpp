//////// SOLUTION of exercise_17_09 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h" /// all include stuff

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointers);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char ch1 = 'A';  
        char ch2 = 'B';
        char *pCh1 = &ch1; /// stack ptrs. to ch1
        char *pCh2 = &ch2; /// and ch2
        char *pCh1_new = new char {ch1}; /// heap ptrs. to ch1
        char *pCh2_new = new char {ch2}; /// and ch2

        /// diff between stack pointers to char
        int stackCharDiff = pCh2 - pCh1; 
        print_Pointer_Diff("stackCharDiff", stackCharDiff); /// calling print_Pointer_Diff 

        /// diff between heap pointers to char
        int heapCharDiff = pCh2_new - pCh1_new;
        print_Pointer_Diff("heapCharDiff", heapCharDiff); /// calling print_Pointer_Diff  
        delete pCh1_new;  /// delete from heap pCh1_new
        delete pCh2_new;  /// and pCh2_new

        int intNumber1 = 65;
        int intNumber2 = 66;
        int *pIntNumber1 = &intNumber1; /// stack ptrs. to intNumber1
        int *pIntNumber2 = &intNumber2; /// and intNumber2 
        int *pIntNumber1_new = new int {intNumber1};      /// heap ptrs. to intNumber1
        int *pIntNumber2_new = new int {intNumber2};      /// and intNumber2

        /// diff between stack pointers to int
        int stackIntDiff = pIntNumber2 - pIntNumber1; 
        print_Pointer_Diff("stackIntDiff", stackIntDiff); /// calling print_Pointer_Diff 

        /// diff between heap pointers to int
        int heapIntDiff = pIntNumber2_new - pIntNumber1_new;
        print_Pointer_Diff("heapIntDiff", heapIntDiff); /// calling print_Pointer_Diff 
        delete pIntNumber1_new;  /// delete from heap pIntNumber1_new
        delete pIntNumber2_new;  /// and pIntNumber2_new

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointer)
{
    cout << str_diffOfPointers << " = " << diffOfPointer << endl; 

    /// print info about "more or less than 0"
    if (diffOfPointer > 0) {
        cout << str_diffOfPointers << " > 0" << endl;
    } else {
        cout << str_diffOfPointers << " < 0" << endl;
    }

    cout << endl; /// new line
}

