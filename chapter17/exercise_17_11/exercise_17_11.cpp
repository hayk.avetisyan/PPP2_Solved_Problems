//////// SOLUTION of exercise_17_11 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h" /// all include stuff

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointers);
//////////////////////////////////////////////////////////////////////////////////////

class Link {
public:
    string value;
    Link(const string& valueInit, Link *prevInit = nullptr, Link *succInit = nullptr)
        :value{valueInit}, prev{prevInit}, succ{succInit} { }

    Link* insert(Link *linkNew); // insert n before this object
    Link* add(Link *linkPrev); // insert n after this object
    Link* erase(); // remove this object from list  
    Link* find(const string& str); // find s in list
    const Link* find(const string& str) const; // find s in const list (see §18.5.1)
    Link* advance(int n); // move n positions in list
    Link* next() const { return succ; }
    Link* previous() const { return prev; }
private:
    Link *prev;
    Link *succ;
};

Link *Link::insert(Link *linkNew)
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;

    linkNew->succ = this;
    if(this->prev) this->prev->succ = linkNew;
    linkNew->prev = this->prev;
    this->prev = linkNew;
    return linkNew;
}

Link *Link::add(Link *linkNew) 
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;
    linkNew->succ = this->succ;
    linkNew->prev = this;
    if (this->succ) this->succ->prev = linkNew;
    this->succ = linkNew;
    return linkNew;
}


Link *Link::erase()
{
    if (nullptr == this) return nullptr;
    if (this->succ) this->succ->prev = this->prev;
    if (this->prev) this->prev->succ = this->succ;
    return this->succ;
}

Link *Link::find(const string& str) // find s in list
{
    Link *pToCurrent = this;
    while(pToCurrent) {
        if (str == pToCurrent->value) return pToCurrent;
        pToCurrent = pToCurrent->succ;
    }
    
    return nullptr;
}

Link *Link::advance(int n) // move n positions in list
{
    if (nullptr == this) return nullptr;

    Link *pToCurrent = this;
    if (n > 0) {
        while (n--) {
            if (nullptr == pToCurrent->succ) return nullptr;
            pToCurrent = pToCurrent->succ;
        }
    } else if (n < 0) {
        while (n++) {
            if (nullptr == pToCurrent->prev) return nullptr;
            pToCurrent = pToCurrent->prev;
        }
    }

    return this;
}

void print_all(Link *pLink)
{
    cout << "{";
    while (pLink) {
        cout << pLink->value;
        if (pLink = pLink->next()) {
            cout << ", ";
        }
    }

    cout << "}";
}

int main()
{
    try {
        Link *norse_gods = new Link{"Odin", nullptr, nullptr};
        norse_gods = norse_gods->insert(new Link{"Frigg"});
        norse_gods = norse_gods->insert(new Link{"Freya"});
        norse_gods = norse_gods->insert(new Link{"Thor"});
        norse_gods = norse_gods->insert(new Link{"Loki"});
        norse_gods = norse_gods->insert(new Link{"Tyr"});
        norse_gods = norse_gods->insert(new Link{"Vidar"});
        print_all(norse_gods);

        Link *pLink_Odin = norse_gods->find("Thor");
        cout << endl;
        cout << "pLink_Odin -> value = " << pLink_Odin -> value << endl;
        norse_gods->add(new Link{"Zeus"});
        print_all(norse_gods);

        Link *pLink_Zeus = norse_gods->find("Zeus");

        if (pLink_Zeus) {
            if (pLink_Zeus == norse_gods) norse_gods = norse_gods->next();
            pLink_Zeus->erase();
        }

        cout << endl;
        print_all(norse_gods);


        Link *greek_gods = new Link{"Mars", nullptr, nullptr};
        greek_gods = greek_gods->insert(new Link{"Poseidon"});
        greek_gods = greek_gods->insert(new Link{"Ares"});
        greek_gods = greek_gods->insert(new Link{"Athena"});
        cout << endl;
        print_all(greek_gods);

        Link *pLink_Mars = greek_gods->find("Mars");
        pLink_Mars->add(pLink_Zeus);
        cout << endl;
        print_all(greek_gods);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointer)
{
    cout << str_diffOfPointers << " = " << diffOfPointer << endl; 

    /// print info about "more or less than 0"
    if (diffOfPointer > 0) {
        cout << str_diffOfPointers << " > 0" << endl;
    } else {
        cout << str_diffOfPointers << " < 0" << endl;
    }

    cout << endl; /// new line
}

