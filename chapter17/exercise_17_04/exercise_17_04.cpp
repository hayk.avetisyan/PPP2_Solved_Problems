//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void to_lower(char* s);
char* strdup(const char* s);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char s[] = "Hello, World!";
        cout << "s = " << s << endl;
        char* sNew = strdup(s);
        cout << "\nPrinting the string copied on the free store:\n";
        cout << "sNew = " << sNew << endl;
        cout << "sNew[0] = " << sNew[0] << endl;
        cout << "sNew[1] = " << sNew[1] << endl;
        cout << "..." << endl;

        delete [] sNew;
        cout << "\nPrinting the string copied on the free store after deallocating:\n";
        cout << "sNew = " << sNew << endl;
        cout << "sNew[0] = " << sNew[0] << endl;
        cout << "sNew[1] = " << sNew[1] << endl;
        cout << "..." << endl;

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
void to_lower(char* s)
{
    while (*s != '\0') {
        if ('A' <= *s && *s <= 'Z') {
            *s += 32;
        }

        ++s;
    }
}

char* strdup(const char* s)
{
    int size = 0;
    while (*s != '\0') {
        ++s;
        ++size;
    }
    s -= size;

    ++size;
    cout << "size = " << size << endl;
    char* sNew = new char[size];
    int loopLimit = size - 2;
    for (int index = 0; index <= loopLimit; ++index) {
        sNew[index] = s[index];
        cout << sNew[index];

    }

    sNew[size - 1] = '\0';
    cout << sNew[size - 1] << endl;

    return sNew;
}




