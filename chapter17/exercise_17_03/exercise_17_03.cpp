//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool); 
void print_array10(ostream& os, int* pToInts);
void print_array(ostream& os, const int* pToInts, const int n);
void to_lower(char* s);
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        char s[] = "Hello, World!";
        cout << "s = " << s << endl;
        to_lower(s);
        cout << "\nAfter to_lower(char* ) function:\n";
        cout << "s = " << s << endl;




    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints sizes of the argument list-types
void sizes(char ch, int i, double d, bool b, char* pChar, int* pInt, double* pDouble, bool* pBool) 
{
    cout << "the size of char is " << sizeof(char) << ' ' << sizeof (ch) << '\n';
    cout << "the size of int is " << sizeof(int) << ' ' << sizeof (i) << '\n';
    cout << "the size of double is " << sizeof(double) << ' ' << sizeof (d) << '\n';
    cout << "the size of bool is " << sizeof(bool) << ' ' << sizeof (b) << '\n';
    cout << "the size of char* is " << sizeof(char*) << ' ' << sizeof (pChar) << '\n';
    cout << "the size of int* is " << sizeof(int*) << ' ' << sizeof (pInt) << '\n';
    cout << "the size of double* is " << sizeof(double*) << ' ' << sizeof (pDouble) << '\n';
    cout << "the size of bool* is " << sizeof(bool*) << ' ' << sizeof (pBool) << '\n';
}    

/// Drill 4
void print_array10(ostream& os, int* pToInts)
{
    for (int index = 0; index < 10; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

/// Drill 7
void print_array(ostream& os, const int* pToInts, const int n)
{
    for (int index = 0; index < n; ++index) {
        os << "pToInts[" << index << "] = " << pToInts[index] << ",  " 
           << "&pToInts[" << index << "] = " << &pToInts[index] << "\n";
    }
}

void to_lower(char* s)
{
    while (*s != '\0') {
        if ('A' <= *s && *s <= 'Z') {
            *s += 32;
        }

        ++s;
    }
}



