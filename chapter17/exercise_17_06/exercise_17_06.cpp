//////// SOLUTION of the Drill ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h"
#include <iomanip>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
void alloc_All_Heap(void);
template<class T> double size(T);	// for every type T
template<class T> double size();	// for every type T
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        alloc_All_Heap();
 
    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
template<class T> 
double size(T)	// for every type T
{
    T* p = new T[2];
    int s = reinterpret_cast<char*>(&p[1])-reinterpret_cast<char*>(&p[0]);
    //cout << "&p[1] = " << &p[1] << endl;
    //cout << "&p[0] = " << &p[0] << endl;
    //cout << "reinterpret_cast<char*>(&p[1]) = " << reinterpret_cast<char*>(&p[1]) << end;
    //cout << "reinterpret_cast<char*>(&p[0]) = " << reinterpret_cast<char*>(&p[0]) << end;
    delete[] p;
    return s;
}

template<class T>
double size()	// for every type T
{
    T* p = new T[2];
    int s = reinterpret_cast<char*>(&p[1])-reinterpret_cast<char*>(&p[0]);
    delete[] p;
    return s;
}

class SmallSizeType {
    double d1 = 1;
    double d2 = 2;
    double d3 = 3;
    double d4 = 4;
    double d5 = 5;
    double d6 = 6;
    double d7 = 7;
    double d8 = 8;
};

class MiddleSizeType {
    SmallSizeType sm1;
    SmallSizeType sm2;
    SmallSizeType sm3;
    SmallSizeType sm4;
    SmallSizeType sm5;
    SmallSizeType sm6;
    SmallSizeType sm7;
    SmallSizeType sm8;
};

class BigSizeType {
    MiddleSizeType md1;
    MiddleSizeType md2;
    MiddleSizeType md3;
    MiddleSizeType md4;
    MiddleSizeType md5;
    MiddleSizeType md6;
    MiddleSizeType md7;
    MiddleSizeType md8;
};

void alloc_All_Heap(void)
{
    double memUsedSize = 0;
    while(true) {
        BigSizeType *pBigSizeType = new BigSizeType;
        memUsedSize += size<BigSizeType>();
        cout << setprecision(1) << fixed << memUsedSize / 1000000000 << endl;
    }

}
        

