//////// SOLUTION of exercise_17_13 ////////////////////////////////////
#include "../../std_lib_facilities_haykav.h" /// all include stuff

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointers);
//////////////////////////////////////////////////////////////////////////////////////

struct god {
    string name = "";
    string mythology = "";
    string vehicle = "";
    string weapon = "";
    god(const string& nameInit, const string& mythInit, const string& weapInit, const string& vehicleInit)
        : name{nameInit}, mythology{mythInit}, weapon{weapInit}, vehicle{vehicleInit} { }
    god(const string& nameInit, const string& mythInit,  const string& weapInit)
        : name{nameInit}, mythology{mythInit}, weapon{weapInit} { }
    god(const string& nameInit, const string& mythInit)
        : name{nameInit}, mythology{mythInit} { }
};

class Link {
public:
    string value;
    god godLink;

    Link(const string& nameInit, const string& mythInit, const string& weapInit, const string& vehicleInit,
         Link *prevInit = nullptr, Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit, weapInit, vehicleInit}), prev{prevInit}, succ{succInit} { }

    Link(const string& nameInit, const string& mythInit, const string& weapInit,
         Link *prevInit = nullptr, Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit, weapInit}), prev{prevInit}, succ{succInit} { }

    Link(const string& nameInit, const string& mythInit, Link *prevInit = nullptr, Link *succInit = nullptr)
        :godLink(god{nameInit, mythInit}), prev{prevInit}, succ{succInit} { }

    Link* insert(Link *linkNew); // insert n before this object
    Link* add(Link *linkPrev); // insert n after this object
    void erase(); // remove this object from list  
    Link* find(const string& str); // find s in list
    const Link* find(const string& str) const; // find s in const list (see §18.5.1)
    Link* advance(int n); // move n positions in list
    Link* next() const { return succ; }
    Link* previous() const { return prev; }

    Link* add_ordered(Link *linkNew);
private:
    Link *prev;
    Link *succ;
};

Link *Link::insert(Link *linkNew)
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;

    linkNew->succ = this;
    if(this->prev) this->prev->succ = linkNew;
    linkNew->prev = this->prev;
    this->prev = linkNew;
    return linkNew;
}

Link *Link::add(Link *linkNew) 
{
    if (nullptr == linkNew) return this;
    if (nullptr == this) return linkNew;
    linkNew->succ = this->succ;
    linkNew->prev = this;
    if (this->succ) this->succ->prev = linkNew;
    this->succ = linkNew;
    return linkNew;
}

void Link::erase()
{
    if (nullptr == this) return;
    if (this->succ) this->succ->prev = this->prev;
    if (this->prev) this->prev->succ = this->succ;
    this->prev = nullptr;
    this->succ = nullptr;
}

Link *Link::find(const string& str) // find s in list
{
    Link *pToCurrent = this;
    while(pToCurrent) {
        if (str == pToCurrent->godLink.name) return pToCurrent;
        pToCurrent = pToCurrent->succ;
    }
    
    return nullptr;
}

Link *Link::advance(int n) // move n positions in list
{
    if (nullptr == this) return nullptr;

    Link *pToCurrent = this;
    if (n > 0) {
        while (n--) {
            if (nullptr == pToCurrent->succ) return nullptr;
            pToCurrent = pToCurrent->succ;
        }
    } else if (n < 0) {
        while (n++) {
            if (nullptr == pToCurrent->prev) return nullptr;
            pToCurrent = pToCurrent->prev;
        }
    }

    return this;
}

Link *Link::add_ordered(Link *linkNew)
{
    if (0 == linkNew) return this;
    if (0 == this) {
        linkNew->succ = 0;
        linkNew->prev = 0;
        return linkNew;
    }

    if (linkNew->godLink.name <= godLink.name) {
        Link *pToCurrent = insert(linkNew);
        return pToCurrent;
    }

    Link *pToCurrent = this;
    while(linkNew->godLink.name > pToCurrent->godLink.name) {
        if (pToCurrent->succ) {
            pToCurrent = pToCurrent->succ;
        } else {
            pToCurrent->add(linkNew);
            return this;
        }
    }

    pToCurrent = pToCurrent->prev;
    pToCurrent->add(linkNew);
    return this;

}

Link *extract(Link **ppLink, string godName)
{
    Link *tempLink = (*ppLink)->find(godName);
    if (nullptr == tempLink) return nullptr;
    if (tempLink == *ppLink) *(ppLink) = (*ppLink)->next();

    tempLink->erase();
    return tempLink;
} 

void print_all(Link *pLink)
{
    cout << "{\n";
    while (pLink) {
        cout << "\t" << pLink->godLink.name << ", " << pLink->godLink.mythology;
        
        string tempString = pLink->godLink.weapon;
        if ("" != tempString) {
            cout << ", " << tempString;
        }

        tempString = pLink->godLink.vehicle;
        if ("" != tempString) {
            cout << ", " << tempString;
        }

        pLink = pLink->next();
        cout << endl;
    }

    cout << "}";
}

int main()
{
    try {
        Link *gods = new Link{"Odin", "Norse", "Spear called Gungnir", "Eight-legged flying horse called Sleipner", nullptr, nullptr};
        gods = gods->insert(new Link{"Thor", "Norse", "Hammer"});
        gods = gods->insert(new Link{"Loki", "Norse"});
        gods = gods->insert(new Link{"Balder", "Norse"});
        gods = gods->insert(new Link{"Frigg", "Norse"});
        gods = gods->insert(new Link{"Tyr", "Norse", "Spear and shield"});
        gods = gods->insert(new Link{"Mars", "Roman", "Spear and shield"});
        gods = gods->insert(new Link{"Saturn", "Roman"});
        gods = gods->insert(new Link{"Mercury", "Roman"});
        gods = gods->insert(new Link{"Apollo", "Roman"});
        gods = gods->insert(new Link{"Vulcan", "Roman"});
        gods = gods->insert(new Link{"Neptune", "Roman", "Trident", "Sea horse"});
        gods = gods->insert(new Link{"Posidon", "Greek", "Trident"});
        gods = gods->insert(new Link{"Hera", "Greek"});
        gods = gods->insert(new Link{"Hades", "Greek", "Helmet of darkness"});
        gods = gods->insert(new Link{"Hermes", "Greek", "Caduceus"});
        gods = gods->insert(new Link{"Zeus", "Greek", "lightning"});
        gods = gods->insert(new Link{"Dionysus", "Greek"});
        gods = gods->insert(new Link{"Athena", "Greek"});
        gods = gods->insert(new Link{"Ares", "Greek", "Spear and shield"});
        cout << "various gods:\n";
        print_all(gods);

        Link *norse_gods = extract(&gods, "Odin");
        norse_gods = norse_gods->add_ordered(extract(&gods, "Freya"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Thor"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Loki"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Balder"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Frigg"));
        norse_gods = norse_gods->add_ordered(extract(&gods, "Tyr"));
        newLine(3);
        cout << "norse_gods after extraction:\n";
        print_all(norse_gods);

        Link *roman_gods = extract(&gods, "Mars");
        roman_gods = roman_gods->add_ordered(extract(&gods, "Saturn"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Mercury"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Apollo"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Vulcan"));
        roman_gods = roman_gods->add_ordered(extract(&gods, "Neptune"));
        newLine(3);
        cout << "roman_gods after extraction:\n";
        print_all(roman_gods);

        Link *greek_gods = extract(&gods, "Posidon");
        greek_gods = greek_gods->add_ordered(extract(&gods, "Hera"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Hades"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Zeus"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Dionysus"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Athena"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Ares"));
        greek_gods = greek_gods->add_ordered(extract(&gods, "Hermes"));
        newLine(3);
        cout << "greek_gods after extraction:\n";
        print_all(greek_gods);

        newLine(3);
        cout << "various gods after extraction:\n";
        print_all(gods);
    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
/// prints diffOfPointer's value and either more or less than 0
void print_Pointer_Diff(const string& str_diffOfPointers, const int diffOfPointer)
{
    cout << str_diffOfPointers << " = " << diffOfPointer << endl; 

    /// print info about "more or less than 0"
    if (diffOfPointer > 0) {
        cout << str_diffOfPointers << " > 0" << endl;
    } else {
        cout << str_diffOfPointers << " < 0" << endl;
    }

    cout << endl; /// new line
}

